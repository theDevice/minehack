#ifndef POSITION_H_INCLUDED
#define POSITION_H_INCLUDED

#include <ostream>

#include "config.h"

using namespace std;


class position{



    public:

        position();
        ~position();

        friend position operator+(position In_LeftHandSide, position In_RightHandSide);
        friend position operator-(position In_LeftHandSide, position In_RightHandSide);
        friend position operator*( position In_LeftHandSide, int In_Skalar );
        friend bool operator< ( const position& In_LeftHandSide, position& In_RightHandSide );
        friend bool operator== ( const position& In_LeftHandSide, position& In_RightHandSide );
        friend string operator+ ( const string& In_LeftHandSide, position& In_RightHandSide );
        friend ostream& operator<< ( ostream &out, position &In_Position );
        //bool operator= ( const position& In_LeftHandSide, const position& In_RightHandSide );
        friend bool operator!= (const position& In_LeftHandSide, const position& In_RightHandSide);

        int PosY = -1;
        int PosX = -1;
        int PosZ = -1;

    private:

};
















#endif // POSITION_H_INCLUDED
