#ifndef ITEM_H_INCLUDED
#define ITEM_H_INCLUDED


#include <string>

#include "config.h"
#include "cube.h"

using namespace std;


class item : public cube{

    public:
        item();
        ~item();

        string getName();
        int getEnergy();

    protected:
        int Size;               // some number from 1 to 100. every inventory will have be enough space for 100 units. so for example 20 items of size 5 or 1 of size 100. or 100 of size 1
        int Energy = 100;       //can be damaged
        string Name  = "none";
        int Level = 0;          //minimum level of character to use this item
        void destroyThisItem();

    private:

};

#endif // ITEM_H_INCLUDED
