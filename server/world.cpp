#include <stdio.h>      // printf, NULL
#include <stdlib.h>     // srand, rand
#include <time.h>       // time
#include <iostream>
#include <fstream>
#include <sys/time.h>           //needed for timestamp

using namespace std;

#include "world.h"


world::world(){


}



world::world( database* In_Database ){     //dummy constructor

    Database = In_Database;

}

world::~world(void){


}


void world::createNew( bool In_MakeFlatWorld ){

    cout << getTimestampInMiliseconds() << " entering world::createNew()" << endl;


    //MaxLevelOverZero = MAX_LEVEL_OVER_ZERO;

    WorldSize.Height = HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING;
    WorldSize.Width = HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING;
    WorldSize.MaxLevelOverZero = MAX_LEVEL_OVER_ZERO;

    //!That is just creating a flat world with a solid surface
    int Temp_SizeOfGermWorld = HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE;
    for(int64_t Y = 0; Y < (Temp_SizeOfGermWorld) ; Y++ ){
        vector < vector < head > > XVector;
        for( int64_t X = 0; X < (Temp_SizeOfGermWorld) ; X++ ){
            vector < head > ZVector;
            for( int64_t Z = 0; Z < MAX_LEVEL_OVER_ZERO /*AVERAGE_LEVEL_OF_GROUND*/ ; Z++){
                head Temp_Head;

                cube* Temp_Cube = new cube();

                if( Z < AVERAGE_LEVEL_OF_GROUND ){
                    Temp_Cube->setTypeOfCube( solid );           //making a solid ground
                }else{
                    Temp_Cube->setTypeOfCube(none);            //or thin air
                }
                Temp_Head.CubeStack.push_back( Temp_Cube );
                ZVector.push_back( Temp_Head );
            }
            XVector.push_back( ZVector );
        }
        GermWorld.push_back(XVector);
    }

    if(In_MakeFlatWorld == false){
        //!that is setting Germs for Mountains and Valleys into the World
        setRandomGermsForMountainsAndValleysIntoGermWorld();
    }

    if(In_MakeFlatWorld == false){
        makeMountainOrValley();
    }

        copyGermWorldInNormalWorld();

    if(In_MakeFlatWorld == false){
        placeClusterOfCubeOnSurface( solid );
        placeClusterOfCubeOnSurface( solid);     //two times, because it makes the structure even more interesting, because it can double
        fillWorldWithWaterToLevel();
        placeClusterOfCubeOnSurface(grass);
        placeClusterOfCubeOnSurface(sand);
        placeClusterOfCubeOnSurface(gravel);
    }

    if(In_MakeFlatWorld == false){
        //!making trees
        for (int I = 0; I < NUMBER_OF_TREES; I++){
            bool TreePlaced = false;
            int NumberOfTryes = 0;
            while(TreePlaced == false && NumberOfTryes < 100){
                TreePlaced = makeTree( ( rand()%(HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING-2) )+2, ( rand()%(HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING-2) )+2);
                if(TreePlaced == false){
                    cout << getTimestampInMiliseconds() << " couldn't place tree!!!!! trying once again" << endl;
                }
                NumberOfTryes++;
            }
            cout << "tryed to place Tree " << NumberOfTryes << " times" << endl;
        }
    }

    writeWorldInFile("WorldVector");

    cout << getTimestampInMiliseconds() << " leaving world::createNew()" << endl;

}



void world::writeWorldInFile(string In_WhichWorld){

    cout << getTimestampInMiliseconds() << " entering world::writeWorldInFile()" << endl;

    system("rm world[0].archive");
    system("rm world[1].archive");
    system("rm world[2].archive");
    system("rm world[3].archive");
    system("rm world[4].archive");
    system("rm world[5].archive");
    system("rm world[6].archive");
    system("rm world[7].archive");
    system("rm world[8].archive");
    system("rm world[9].archive");
    system("rm world[10].archive");
    system("rm world[11].archive");
    system("rm world[12].archive");
    system("rm world[13].archive");
    system("rm world[14].archive");
    system("rm world[15].archive");

    vector < vector < vector < head >>> Temp_World;
    if(In_WhichWorld == "GermWorld"){
        Temp_World = GermWorld;
    }else if(In_WhichWorld == "WorldVector"){
        Temp_World = WorldVector;
    }else{
        cout << getTimestampInMiliseconds() << " don't understand which world was meant" << endl;
        system("sleep(5)");
        exit(0);
    }

    for ( int64_t Z = 0; Z < MAX_LEVEL_OVER_ZERO; Z++ ){
        #if DEBUG_WRITE_WORLD_IN_FILE
            cout << getTimestampInMiliseconds() << " Z: " << Z << endl;
        #endif // DEBUG_WRITE_WORLD_IN_FILE
        for ( size_t Y = 0; Y < Temp_World.size(); Y++){
            for (size_t X = 0; X < Temp_World[Y].size(); X++){
                string Symbol;
                if( Temp_World[Y][X][Z].CubeStack[0]->getTypeOfCube() == solid ){



                    if( Temp_World[Y][X][Z].CubeStack[0]->getTypeOfCube() == solid ){
                        Symbol = "S";
                    }else if( Temp_World[Y][X][Z].CubeStack[0]->isGermForMountainOrValley() ){
                        Symbol = "X";
                    }else{
                        #if DEBUG_WRITE_WORLD_IN_FILE
                            cout << getTimestampInMiliseconds() << " unknown cube in GermForMountainOrValley at position: " << Y << ";" << X << ";" << Z  << endl;
                        #endif // DEBUG_WRITE_WORLD_IN_FILE
                    }


                }else if(Temp_World[Y][X][Z].CubeStack[0]->getTypeOfCube( ) == none){  //thin air
                    Symbol = " ";
                }else if( Temp_World[Y][X][Z].CubeStack[0]->getTypeOfCube( ) == trunk ){
                    Symbol = "o";
                }else if( Temp_World[Y][X][Z].CubeStack[0]->getTypeOfCube( ) == branch ){
                    Symbol = "|";
                }else if( Temp_World[Y][X][Z].CubeStack[0]->getTypeOfCube( ) == leafsAndTwigs ){
                    Symbol = "b";
                }else if( Temp_World[Y][X][Z].CubeStack[0]->getTypeOfCube(  ) == water ){
                    Symbol = "~";
                }else if(Temp_World[Y][X][Z].CubeStack[0]->getTypeOfCube( ) == grass){
                    Symbol = ",";
                }else if(Temp_World[Y][X][Z].CubeStack[0]->getTypeOfCube( ) == sand){
                    Symbol = ":";
                }else if(Temp_World[Y][X][Z].CubeStack[0]->getTypeOfCube() == gravel){
                    Symbol = "*";
                }else{
                    #if DEBUG_WRITE_WORLD_IN_FILE
                        cout << getTimestampInMiliseconds() << " not solid ground or NONE" << endl;
                    #endif // DEBUG_WRITE_WORLD_IN_FILE()
                }

                if( Temp_World[Y][X][Z].CubeStack[0]->isGermForMountainOrValley() == true ){
                    Symbol = "X";
                }

                ofstream file;
                file.open ("world[" + to_string(Z) + "].archive", ios::out | ios::app);
                file << Symbol;
                file.close();

                #if DEBUG_WRITE_WORLD_IN_FILE
                    cout << getTimestampInMiliseconds() << " wrote an " << Symbol << " in file: " << "world[" + to_string(Z) + "].archive ; " << "[Position: Y:" << Y << " X:" << X << " Z:" << Z << "]"<< endl;
                #endif // DEBUG_WRITE_WORLD_IN_FILE()
            }
            ofstream file;
            file.open ("world[" + to_string(Z) + "].archive", ios::out | ios::app);
            file << "\n";
            file.close();
        }
    }
    cout << getTimestampInMiliseconds() << " leaving world::writeWorldInFile()" << endl;
}


void world::makeMountainOrValley(){

    cout << getTimestampInMiliseconds() << " entering makeMountainOrValley (that can take a while)" << endl;

    bool GermFound = false;
    bool LeaveLoop = false;

    while(LeaveLoop == false){
        GermFound = false;

        //searching for a germ
        position OriginalPosition;
        for( int Z = 0; Z < MAX_LEVEL_OVER_ZERO && GermFound == false; Z++ ){
            for( int Y = 0; Y < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING && GermFound == false; Y++){
                for( int X = 0; X < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING && GermFound == false; X++){
                    if( GermWorld[Y][X][Z].CubeStack[0]->isGermForMountainOrValley() ){
                        OriginalPosition.PosY = Y;
                        OriginalPosition.PosX = X;
                        OriginalPosition.PosZ = Z;
                        GermFound = true;

                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " found germ at position: \t" << OriginalPosition.PosY << "\t" << OriginalPosition.PosX << "\t" << OriginalPosition.PosZ << endl;
                        #endif // DEBUG_OUTPUT
                    }
                }
            }
        }

        if( GermFound == false ){       //no germ found
            LeaveLoop = true;
        }


        if( OriginalPosition.PosZ < AVERAGE_LEVEL_OF_GROUND && GermFound == true ){     //!valley
            #if DEBUG_OUTPUT
                cout << getTimestampInMiliseconds() << " digging a valley: Y: " << OriginalPosition.PosY << " X: " << OriginalPosition.PosX << " Z: " << OriginalPosition.PosZ << endl;
            #endif // DEBUG_OUTPUT

            position NewPosition;                   //we just want to make changes one level above the current level
            NewPosition.PosZ = OriginalPosition.PosZ + 1;

            for (int I = 0; I < 8; I++){
                bool DoIt = true;

                if( I == 0 ){   //position variation
                    if( OriginalPosition.PosX > 0 ){
                        NewPosition.PosX = OriginalPosition.PosX - 1;
                    }else{
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                        DoIt = false;
                    }
                    if( OriginalPosition.PosY > 0 ){
                        NewPosition.PosY = OriginalPosition.PosY - 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                }else if( I == 1 ){
                    NewPosition.PosX = OriginalPosition.PosX;
                    if( OriginalPosition.PosY > 0 ){
                        NewPosition.PosY = OriginalPosition.PosY - 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                }else if( I == 2 ){
                    if( OriginalPosition.PosX < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE -1){
                        NewPosition.PosX = OriginalPosition.PosX + 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                    if( OriginalPosition.PosY > 0 ){
                        NewPosition.PosY = OriginalPosition.PosY - 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                }else if( I == 3 ){
                    if(OriginalPosition.PosX < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1){
                        NewPosition.PosX = OriginalPosition.PosX + 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                    if(OriginalPosition.PosY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1){
                        NewPosition.PosY = OriginalPosition.PosY;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                }else if( I == 4 ){
                    if(OriginalPosition.PosX < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1){
                        NewPosition.PosX = OriginalPosition.PosX + 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                    if( OriginalPosition.PosY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1 ){
                        NewPosition.PosY = OriginalPosition.PosY + 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                }else if( I == 5 ){
                    if(OriginalPosition.PosX < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1){
                        NewPosition.PosX = OriginalPosition.PosX;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                    if(OriginalPosition.PosY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1){
                        NewPosition.PosY = OriginalPosition.PosY + 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                }else if( I == 6 ){
                    if(OriginalPosition.PosX > 0){
                        NewPosition.PosX = OriginalPosition.PosX - 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                    if(OriginalPosition.PosY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1){
                        NewPosition.PosY = OriginalPosition.PosY + 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                }else if( I == 7 ){
                    if(OriginalPosition.PosX > 0){
                        NewPosition.PosX = OriginalPosition.PosX - 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                    if(OriginalPosition.PosY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1){
                        NewPosition.PosY = OriginalPosition.PosY;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                }

                if(DoIt){
                    if( GermWorld[NewPosition.PosY][NewPosition.PosX][NewPosition.PosZ].CubeStack[0]->getTypeOfCube() == solid ){

                        GermWorld[NewPosition.PosY][NewPosition.PosX][NewPosition.PosZ].CubeStack[0]->setIsGermForMountainOrValley(true);

                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " setIsGermForMountainOrValley done. Position: " << NewPosition.PosY << " ; " << NewPosition.PosX << " ; " << NewPosition.PosZ << endl;
                        #endif // DEBUG_OUTPUT

                        for( int I = NewPosition.PosZ + 1; GermWorld[NewPosition.PosY][NewPosition.PosX][I].CubeStack[0]->getTypeOfCube() == solid; I++ ){        //deleting all the stuff above
                            GermWorld[NewPosition.PosY][NewPosition.PosX][I].CubeStack[0]->setTypeOfCube(none);
                            #if DEBUG_OUTPUT
                                cout << getTimestampInMiliseconds() << " set cube to 'none' at: \t" << NewPosition.PosY << "\t" << NewPosition.PosX << "\t" << I << endl;
                            #endif // DEBUG_OUTPUT
                        }
                    }
                }
            }

        }else if ( OriginalPosition.PosZ > AVERAGE_LEVEL_OF_GROUND && GermFound == true){      //!mountain
            #if DEBUG_OUTPUT
                cout <<  "\n" << getTimestampInMiliseconds() << " building a mountain. Position: Y: " << OriginalPosition.PosY << " X: " << OriginalPosition.PosX << " Z: " << OriginalPosition.PosZ << endl;
            #endif // DEBUG_OUTPUT
            position NewPosition;   //we just want to make changes under the germ-level
            //NewPosition = OriginalPosition;
            NewPosition.PosZ = OriginalPosition.PosZ - 1;

            for(int I = 0; I < 8; I++){
                bool DoIt = true;
                #if DEBUG_OUTPUT
                    int Debug_I = I;
                    cout << getTimestampInMiliseconds() << " Debug_I: " << Debug_I << endl;
                #endif // DEBUG_OUTPUT()

                if( I == 0 ){   //position variation
                    if(OriginalPosition.PosX > 0){
                        NewPosition.PosX = OriginalPosition.PosX - 1;
                    }else{
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                        DoIt = false;
                    }
                    if(OriginalPosition.PosY > 0){
                        NewPosition.PosY = OriginalPosition.PosY - 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                }
                else if( I == 1 ){
                    NewPosition.PosX = OriginalPosition.PosX;
                    if( OriginalPosition.PosY > 0 ){
                        NewPosition.PosY = OriginalPosition.PosY - 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                }
                else if( I == 2 ){
                    if( OriginalPosition.PosX < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE -1){
                        NewPosition.PosX = OriginalPosition.PosX + 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                    if( OriginalPosition.PosY > 0 ){
                        NewPosition.PosY = OriginalPosition.PosY - 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                }
                else if( I == 3 ){
                    if(OriginalPosition.PosX < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1){
                        NewPosition.PosX = OriginalPosition.PosX + 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                    if(OriginalPosition.PosY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1){
                        NewPosition.PosY = OriginalPosition.PosY;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }

                }
                else if( I == 4 ){
                    if(OriginalPosition.PosX < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1){
                        NewPosition.PosX = OriginalPosition.PosX + 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                    if( OriginalPosition.PosY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1 ){
                        NewPosition.PosY = OriginalPosition.PosY + 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }

                }
                else if( I == 5 ){
                    if(OriginalPosition.PosX < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1){
                        NewPosition.PosX = OriginalPosition.PosX;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                    if(OriginalPosition.PosY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1){
                        NewPosition.PosY = OriginalPosition.PosY + 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }

                }
                else if( I == 6 ){
                    if(OriginalPosition.PosX > 0){
                        NewPosition.PosX = OriginalPosition.PosX - 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                    if(OriginalPosition.PosY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1){
                        NewPosition.PosY = OriginalPosition.PosY + 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }

                }
                else if( I == 7 ){
                    if(OriginalPosition.PosX > 0){
                        NewPosition.PosX = OriginalPosition.PosX - 1;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }
                    if(OriginalPosition.PosY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING + HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE-1){
                        NewPosition.PosY = OriginalPosition.PosY;
                    }else{
                        DoIt = false;
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " set DoIt to false at I: " << I << endl;
                        #endif // DEBUG_OUTPUT
                    }

                }
                #if DEBUG_OUTPUT
                    cout << getTimestampInMiliseconds() << " new NewPosition:\t" << NewPosition.PosY << "\t" << NewPosition.PosX << "\t" << NewPosition.PosZ << "\tDoIt: " << DoIt <<  endl;
                #endif // DEBUG_OUTPUT

                if(DoIt){

                    //standardCube Debug_CubeType = GermWorld[NewPosition.PosY][NewPosition.PosX][NewPosition.PosZ][0].getTypeOfCube();

                    if( GermWorld[NewPosition.PosY][NewPosition.PosX][NewPosition.PosZ].CubeStack[0]->getTypeOfCube() == none  ){
                        GermWorld[NewPosition.PosY][NewPosition.PosX][NewPosition.PosZ].CubeStack[0]->setIsGermForMountainOrValley(true);

                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " setIsGermForMountainOrValley done. Position: " << NewPosition.PosY << " ; " << NewPosition.PosX << " ; " << NewPosition.PosZ << endl;
                        #endif // DEBUG_OUTPUT

                        for( int I = NewPosition.PosZ; GermWorld[NewPosition.PosY][NewPosition.PosX][I].CubeStack[0]->getTypeOfCube() == none; I--){
                            GermWorld[NewPosition.PosY][NewPosition.PosX][I].CubeStack[0]->setTypeOfCube(solid);
                            #if DEBUG_OUTPUT
                                cout << getTimestampInMiliseconds() << " set cube to 'solid' at: \t" << NewPosition.PosY << "\t" << NewPosition.PosX << "\t" << I << endl;
                            #endif // DEBUG_OUTPUT
                        }
                    }
                }
            }
        }else if(GermFound == true){                                                          //nothing at all
            #if DEBUG_OUTPUT
                cout << getTimestampInMiliseconds() << " exactly on ground level; PosY: " << OriginalPosition.PosY << " ; PosX: " << OriginalPosition.PosX << " ; PosZ: " << OriginalPosition.PosZ << endl;
            #endif // DEBUG_OUTPUT
        }else if(GermFound == false){
            #if DEBUG_OUTPUT
                cout << getTimestampInMiliseconds() << " no more germ" << endl;
            #endif // DEBUG_OUTPUT
        }else{
            cout << getTimestampInMiliseconds() << " !!!!something else happend!!! error error error " << endl;
        }

        #if DEBUG_OUTPUT
            cout << getTimestampInMiliseconds() << " Germ Found: " << GermFound << " [bool] ; onPosition :\t" << OriginalPosition.PosY << "\t" << OriginalPosition.PosX << "\t" << OriginalPosition.PosZ << endl;
        #endif // DEBUG_OUTPUT

        if(GermFound){
            #if DEBUG_OUTPUT
                cout << getTimestampInMiliseconds() << " trying to setIsGermForMountainOrValley to false for cube with position:\t" << OriginalPosition.PosY << "\t" << OriginalPosition.PosX << "\t" << OriginalPosition.PosZ << endl;
            #endif // DEBUG_OUTPUT

            GermWorld[OriginalPosition.PosY][OriginalPosition.PosX][OriginalPosition.PosZ].CubeStack[0]->setIsGermForMountainOrValley(false);       //important, otherwise the loop will handle the same germ all over again
        }

    }
}






const vector <cube*> world::getCubesOnPositionFromGermWorld(position In_Position){
    return GermWorld[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack;
}


const vector<cube*> world::getCubesOnPositionFromNormalWorld(position In_Position){
    if( In_Position.PosZ >= MAX_LEVEL_OVER_ZERO || In_Position.PosZ < 0 || In_Position.PosY < 0 || In_Position.PosX < 0 ){
        cout << "trying to get Cubes from invalid area: " << In_Position <<" exit now!" << endl;
        system("sleep(5)");
        exit(0);
    }
    return WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack;
}


const vector <cube*>* world::getPointerToCubeStackOnPositionFromNormalWorld(position In_Position){
    cout << getTimestampInMiliseconds() << " entering world::getPointerToCubeStackOnPositionFromNormalWorld() ; In_Position: " << In_Position << endl;
    if( In_Position.PosZ >= MAX_LEVEL_OVER_ZERO || In_Position.PosZ < 0 || In_Position.PosY < 0 || In_Position.PosX < 0 ){
        return NULL;
    }
    return &(WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack);
}

const head* world::getPointerToHeadOnPositionFromNormalWorld( position In_Position ){
    if( In_Position.PosZ >= MAX_LEVEL_OVER_ZERO || In_Position.PosZ < 0 || In_Position.PosY < 0 || In_Position.PosX < 0 ){
        return NULL;
    }

    return &(WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ]);
}



bool world::makeTree(int In_PosY, int In_PosX){
    int TrunkHeight = -1;
    while( TrunkHeight <= (MIN_TRUNK_HEIGHT - 1) ){
        TrunkHeight= rand() % MAX_TRUNK_HEIGHT + 1;
    }
    int NumberOfTreeBranches = -1;
    while (NumberOfTreeBranches <= (MINIMUM_NUMBER_OF_TREEBRANCHES-1) ){
        NumberOfTreeBranches = rand() % MAXIMUM_NUMBERS_OF_TREEBRANCHES + 1;
    }
    cout << getTimestampInMiliseconds() << " random number of branches: " << NumberOfTreeBranches << " ; TrunkHeight: " << TrunkHeight << endl;
    #if DEBUG_OUTPUT
        cout << "\n" << getTimestampInMiliseconds() << " world::makeTree(): trying to place tree at position: " << In_PosY << ";" << In_PosX << "  ; with height: " << TrunkHeight << endl;
    #endif // DEBUG_OUTPUT
    int Z_Index = MAX_LEVEL_OVER_ZERO-1;
    while( WorldVector[In_PosY][In_PosX][Z_Index].CubeStack[0]->getTypeOfCube() != solid ){    //searching for the solid ground
        Z_Index--;
    }

    if( In_PosX < 3 || In_PosY < 3 || In_PosX >= (HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING-3) || In_PosY >= (HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING-3) ){
        #if DEBUG_OUTPUT
            cout << getTimestampInMiliseconds() << " to near on the edge. returning false" << endl;
        #endif
        //system("sleep 4");
        return false;
    }

    if(     (
                WorldVector[In_PosY+1][In_PosX][Z_Index+1].CubeStack[0]->getTypeOfCube() == solid &&
                WorldVector[In_PosY-1][In_PosX][Z_Index+1].CubeStack[0]->getTypeOfCube() == solid &&
                WorldVector[In_PosY][In_PosX+1][Z_Index+1].CubeStack[0]->getTypeOfCube() == solid &&
                WorldVector[In_PosY][In_PosX-1][Z_Index+1].CubeStack[0]->getTypeOfCube() == solid
            ) ||
            In_PosX < 2 ||
            In_PosY < 2
        ){        //tree is on bottom of a valley or on the edge of the world
        return false;
    }

    if( WorldVector[In_PosY][In_PosX][Z_Index+1].CubeStack[0]->getTypeOfCube() != none ){
        #if DEBUG_OUTPUT
            cout << getTimestampInMiliseconds() << " trying to place the tree on a spot with non-solid ground. returning false!" << endl;
        #endif // DEBUG_OUTPUT
        return false;
    }

    int PositionOfFirstTrunk = 0;
    if( Z_Index < ( MAX_LEVEL_OVER_ZERO - MAX_TRUNK_HEIGHT ) - 2){   //preventing growing trees over max. world-level
                                                        //!attention! when adding a trees and branches, this condition must be reset
        Z_Index++;

        PositionOfFirstTrunk = Z_Index;

        for( ; (Z_Index - PositionOfFirstTrunk) < TrunkHeight; Z_Index++){
            WorldVector[In_PosY][In_PosX][Z_Index].CubeStack[0]->setTypeOfCube(trunk);
        }
    }else{
        #if DEBUG_OUTPUT
            cout << getTimestampInMiliseconds() << " I < ( MAX_LEVEL_OVER_ZERO - MAX_TRUNK_HEIGHT ) - 2  nicht erf?llt " << endl;
        #endif // DEBUG_OUTPUT
        return false;
    }
    //so, now we have a trunk. now we need branches

    for(int BranchCounter = 0; BranchCounter < NumberOfTreeBranches; BranchCounter++){
        position Temp_PositionOfBranch;
        Temp_PositionOfBranch.PosX = In_PosX;
        Temp_PositionOfBranch.PosY = In_PosY;
        Temp_PositionOfBranch.PosZ = Z_Index;

        int XY_PositionOfBranch = rand() % 8;           //random position
        if( XY_PositionOfBranch == 0 ){
            Temp_PositionOfBranch.PosX--;
            Temp_PositionOfBranch.PosY--;
        }else if(XY_PositionOfBranch == 1){
            //Temp_PositionOfBranch.PosX
            Temp_PositionOfBranch.PosY--;
        }else if(XY_PositionOfBranch == 2){
            Temp_PositionOfBranch.PosX++;
            Temp_PositionOfBranch.PosY--;
        }else if(XY_PositionOfBranch == 3){
            Temp_PositionOfBranch.PosX++;
            //Temp_PositionOfBranch.PosY
        }else if(XY_PositionOfBranch == 4){
            Temp_PositionOfBranch.PosX++;
            Temp_PositionOfBranch.PosY++;
        }else if(XY_PositionOfBranch == 5){
            //Temp_PositionOfBranch.PosX
            Temp_PositionOfBranch.PosY++;
        }else if(XY_PositionOfBranch == 6){
            Temp_PositionOfBranch.PosX--;
            Temp_PositionOfBranch.PosY++;
        }else if(XY_PositionOfBranch == 7){
            Temp_PositionOfBranch.PosX--;
            //Temp_PositionOfBranch.PosY
        }

        //and now a rendom ZX position
        int Z_PositionOfBranch = rand() % 3;           //random position
        if( Z_PositionOfBranch == 0 ){
            Temp_PositionOfBranch.PosZ--;
        }else if(Z_PositionOfBranch == 1){
            //Temp_PositionOfBranch.PosZ                //leave it like that
        }else if(Z_PositionOfBranch == 2){
            Temp_PositionOfBranch.PosZ++;
        }

        if(Temp_PositionOfBranch.PosZ < MAX_LEVEL_OVER_ZERO && Temp_PositionOfBranch.PosX >= 0 && Temp_PositionOfBranch.PosY >= 0 && Temp_PositionOfBranch.PosX < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING && Temp_PositionOfBranch.PosY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING){
            #if DEBUG_OUTPUT
                cout << getTimestampInMiliseconds() << " trying to make a branch on position: " << Temp_PositionOfBranch.PosY << " ; " << Temp_PositionOfBranch.PosX << " ; " << Temp_PositionOfBranch.PosZ << endl;
            #endif // DEBUG_OUTPUT
            if( WorldVector[Temp_PositionOfBranch.PosY][Temp_PositionOfBranch.PosX][Temp_PositionOfBranch.PosZ].CubeStack[0]->getTypeOfCube() == solid ){
                #if DEBUG_OUTPUT
                    cout << getTimestampInMiliseconds() << " fail!! trying to make solid into a branche. decreasing I and trying again!" << endl;
                #endif // DEBUG_OUTPUT
                Z_Index--;
            }else{
                WorldVector[Temp_PositionOfBranch.PosY][Temp_PositionOfBranch.PosX][Temp_PositionOfBranch.PosZ].CubeStack[0]->setTypeOfCube(branch);
                if( Temp_PositionOfBranch.PosZ - 1 > PositionOfFirstTrunk ){
                    WorldVector[In_PosY][In_PosX][Temp_PositionOfBranch.PosZ-1].CubeStack[0]->setTypeOfCube(trunk);        //otherwise, branches could just hang around in thin air
                }else{
                    #if DEBUG_OUTPUT
                        cout << getTimestampInMiliseconds() << " Temp_PositionOfBranch.PosZ -1 <= PositionOfTrunk" << endl;
                    #endif // DEBUG_OUTPUT
                }
            }
        }else{
            #if DEBUG_OUTPUT
                cout << getTimestampInMiliseconds() << " condition not fullfiled: Temp_PositionOfBranch.PosZ < MAX_LEVEL_OVER_ZERO && Temp_PositionOfBranch.PosX >= 0 && Temp_PositionOfBranch.PosY >= 0 && Temp_PositionOfBranch.PosX < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING && Temp_PositionOfBranch.PosY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING" << endl;
            #endif // DEBUG_OUTPUT
        }
    }

    //alrighty, now we have branches. now we need leafs and twigs (zweige, alta!)
    cout << getTimestampInMiliseconds() << " adding leafsAndTwigs now" << endl;
    for(int Z = PositionOfFirstTrunk; Z < (PositionOfFirstTrunk + TrunkHeight + 3) ;Z++ ){ //3 sould be enought to cover all branches
        for( int X = In_PosX - 3; X < In_PosX + 3; X++){            //same                         //should also be enought
            for ( int Y = In_PosY - 3; Y < In_PosY + 3; Y++ ){      //same
                if( X > 0 && Y > 0 && Z < MAX_LEVEL_OVER_ZERO /*- 1*/ && X < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING && Y < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING){                               //enought distance to the edge?
                    if( WorldVector[Y][X][Z].CubeStack[0]->getTypeOfCube() ==  branch){

                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " branch detected at: " << Y << " ; " << X << " ; " << Z << endl;
                        #endif // DEBUG_OUTPUT
                        //branch detected

                        for( int Temp_PosZ = (Z - 1); Temp_PosZ < (Z + 2) && Temp_PosZ < (MAX_LEVEL_OVER_ZERO/* - 1*/); Temp_PosZ++ ){
                            for( int Temp_PosX = (X - 1); Temp_PosX < (X + 2) && X < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING; Temp_PosX++ ){
                                for( int Temp_PosY = (Y - 1); Temp_PosY < (Y + 2) && Y < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING; Temp_PosY++ ){
                                    #if DEBUG_OUTPUT
                                        cout << getTimestampInMiliseconds() << " Temp_PosZ : " << Temp_PosZ << " ; Temp_PosX: " << Temp_PosX << " ; Temp_PosY: " << Temp_PosY << endl;
                                    #endif // DEBUG_OUTPUT
                                    if( WorldVector[Temp_PosY][Temp_PosX][Temp_PosZ].CubeStack[0]->getTypeOfCube() == none ){
                                        #if DEBUG_OUTPUT
                                            cout << getTimestampInMiliseconds() << " trying to make leafAndTwigs on Position: " << Y << " ; " << X << " ; " << Z << endl;
                                        #endif // DEBUG_OUTPUT
                                        if( (rand()%PROBABILITY_OF_GETTING_LEAFS_AND_TWIGS + 1) == 1 ){                                         //get random leafsAndTwigs
                                            if( Temp_PosX > 0 && Temp_PosY > 0 && Temp_PosZ > 0 && Temp_PosZ < (MAX_LEVEL_OVER_ZERO /*-1*/ ) ){
                                                WorldVector[Temp_PosY][Temp_PosX][Temp_PosZ].CubeStack[0]->setTypeOfCube(leafsAndTwigs);
                                            }else{
                                                #if DEBUG_OUTPUT
                                                    cout << getTimestampInMiliseconds() << " !!!!!trying to add leafAndTwigs out of range on Position: " << Temp_PosY << " ; " << Temp_PosX << " ; " << Temp_PosZ << endl;
                                                #endif // DEBUG_OUTPUT
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    #if DEBUG_OUTPUT
                        cout << getTimestampInMiliseconds() << " trying to make leafes to close to the edge: " <<  Y << " ; " << X << " ; " << Z << endl;
                    #endif // DEBUG_OUTPUT
                }
            }
        }
    }
    #if DEBUG_OUTPUT
        cout << getTimestampInMiliseconds() << " end of world::makeTree()\n" << endl;
    #endif // DEBUG_OUTPUT()
    return true;
}


void world::copyGermWorldInNormalWorld(){

    cout << getTimestampInMiliseconds() << " entering world::copyGermWorldInNormalWorld()" << endl;
    //cout << "WorldVector.size(): " << WorldVector.size() << endl;

    //!at first its necessary to make the vector big enought to copy the important parts of the GermWorld in

    cout << "calling generateEmptyVectorFramesWithoutHeads, with WorldSize: Height: " << WorldSize.Height << " Width: " << WorldSize.Width << " MaxLevelOverZero: " << WorldSize.MaxLevelOverZero << endl;
    generateEmptyVectorFramesWithoutHeads(&WorldVector, WorldSize);

    //cout << "WorldVector.size(): " << WorldVector.size() << endl;
    for(size_t Y = 0; Y < WorldVector.size(); Y++){
        //cout << "Y: " << Y << endl;
        //cout << "WorldVector[" << Y <<"].size(): " << WorldVector[Y].size() << endl;
        for( size_t X = 0; X < WorldVector[Y].size(); X++ ){
            //cout << "X: " << X << endl;
            //cout << "WorldVector[" << Y <<"][" << X << "].size(): " << WorldVector[Y][X].size() << endl;
            for( size_t Z = 0; Z < WorldVector[Y][X].size(); Z++){
                //cout << "Z: " << Z << endl;
                //WorldVector[Y][X][Z].CubeStack[0] = GermWorld[Y][X][Z].CubeStack[0];
                WorldVector[Y][X][Z] = GermWorld[Y][X][Z];  //copying the had, not only the cubestack

            }
        }
    }


    cout << getTimestampInMiliseconds() << " leaving world::copyGermWorldInNormalWorld()" << endl;
}

void world::fillWorldWithWaterToLevel(){
    for( size_t Y = 0; Y < WorldVector.size(); Y++ ){
        for( size_t X = 0; X < WorldVector[Y].size(); X++ ){
            for( size_t Z = 0; Z < LEVEL_OF_WATER; Z++ ){
                if(WorldVector[Y][X][Z].CubeStack[0]->getTypeOfCube() == none){
                    WorldVector[Y][X][Z].CubeStack[0]->setTypeOfCube(water);
                }
            }
        }
    }
}



void world::setRandomGermsForMountainsAndValleysIntoGermWorld(){
    #if DEBUG_OUTPUT
        cout << "\n" << getTimestampInMiliseconds() << " entering world::setRandomGermsForMountainsAndValleysIntoGermWorld()" << endl;
    #endif // DEBUG_OUTPUT
    //!that is setting Germs for Mountains and Valleys into the World
    bool MountainsAreDone = false;
    for(int J = 0; J < 2; J++){                                         //two times. for mountains and for valleys

        int NumberOfLoops = PROBABILITY_OF_GETTING_MOUNTAIN;
        if( J == 1){                                            //after one (outer-)loop, mountains are done
            MountainsAreDone = true;                            //and this is needed to make valleys in a few lines
            NumberOfLoops = PROBABILITY_OF_GETTING_VALLEY;
        }

        for(int64_t I = 0; I < NumberOfLoops; I++){   //!first, mountains first
            uint64_t PosX = rand() % HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING;                      //something in range of 0 and sizeOfGermworld - 1
            uint64_t PosY = rand() % HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING;

            int PosZ = -1;

            if(MountainsAreDone == false){  //mountains are over ground. WHHHAAATTTT??
                PosZ = ( (rand() % (MAX_HEIGHT_OF_MOUNTAINS - AVERAGE_LEVEL_OF_GROUND) )) + AVERAGE_LEVEL_OF_GROUND;

            }else{                          //valleys
                PosZ = ( rand() % MAX_HEIGHT_OF_VALLEYS );
            }

            #if DEBUG_OUTPUT
                cout << "\n" << getTimestampInMiliseconds() << " random position for mountain/valley-peak: " << "\tPosY " << PosY << "\tPosX " << PosX << "\tPosZ " << PosZ << endl;
                //int DEBUG_OUTPUT_PosY_PosX_size = GermWorld[PosY][PosX].size();
            #endif // DEBUG_IS_GERM_FOR_MOUNTAIN_OR_VALLEY

            if ( AVERAGE_LEVEL_OF_GROUND <= PosZ  ){                              //if the ground is to low, i need to fill it up with solid ground

                for( int Temp_Z = AVERAGE_LEVEL_OF_GROUND; Temp_Z < PosZ; Temp_Z++ ){
                    GermWorld[PosY][PosX][Temp_Z].CubeStack[0]->setTypeOfCube(solid);
                }
            }

            if( PosZ < AVERAGE_LEVEL_OF_GROUND ){
                for( int Temp_Z = AVERAGE_LEVEL_OF_GROUND; Temp_Z >/*=*/ PosZ; Temp_Z-- ){   //was the '>=' causing the errors, at generating the world? answer: seems so!
                    GermWorld[PosY][PosX][Temp_Z].CubeStack[0]->setTypeOfCube(none);
                }
            }

            GermWorld[PosY][PosX][PosZ].CubeStack[0]->setTypeOfCube(solid);
            GermWorld[PosY][PosX][PosZ].CubeStack[0]->setIsGermForMountainOrValley(true);

            #if DEBUG_OUTPUT
                cout << getTimestampInMiliseconds() << " trying to push position of germ to Vec_GermPosition" << endl;
            #endif // DEBUG_OUTPUT
            position Temp_PositonOfGerm;
            Temp_PositonOfGerm.PosY = PosY;
            Temp_PositonOfGerm.PosX = PosX;
            Temp_PositonOfGerm.PosZ = PosZ;
            vector <position> Temp_Vector;
            Vec_GermPositions.push_back(Temp_Vector);

            #if DEBUG_OUTPUT
                //position Debug_Position = GermWorld[PosY][PosX][PosZ][0].getPosition();
                cout << getTimestampInMiliseconds() << " Cube set as GermForMountainOrValley on: \tPosY " << PosY << "\tPosX " << PosX << "\tPosZ " << PosZ << endl << endl;
            #endif // DEBUG_OUTPUT

            //!now i try to make little randam-shaped clusters out of the germs, to get a more natural look
            makeClustersOfGerms(PosY, PosX, PosZ);

            #if MAKE_LESS_STEEPER_MOUNTAINS
                if( rand()%PROBABILITY_OF_GETTING_A_LESS_STEEPER_MOUNTAIN == 0 ){
                    cout << getTimestampInMiliseconds() << " trying to generate a less steeper Mountain" << endl;
                    makeLastGermedMountainLessSteep();
                }
            #endif // MAKE_LESS_STEEPER_MOUNTAINS
        }
    }
}


void world::makeClustersOfGerms(int In_PosY, int In_PosX, int In_PosZ){   //call from: world::setRandomGermsForMountainsAndValleysIntoGermWorld()
    cout << "entering  world::makeClustersOfGerms()" << endl;

    //!now i try to make little randam-shaped clusters out of the germs, to get a more natural look
    cout << getTimestampInMiliseconds() << " starting to make a clusterz" << endl;
    int Temp_SizeOfCluster = -1;
    int Temp_ProbabilityOfPositivOrNegative = rand()%2; //change the size of the new cluster slightly
    if( Temp_ProbabilityOfPositivOrNegative == 0 ){     //Positiv deltag
        Temp_SizeOfCluster = (AVERAGE_CLUSTER_SIZE_FOR_GERMS - ( rand() % DELTA_CLUSTER_SIZE_FOR_GERMS)); //depending on how big we set the value in the define.
    }else if( Temp_ProbabilityOfPositivOrNegative == 1 ){
        Temp_SizeOfCluster = ( AVERAGE_CLUSTER_SIZE_FOR_GERMS - ( rand() % DELTA_CLUSTER_SIZE_FOR_GERMS ) );
    }
    //now we know how big the cluster should be lets rock!
    position AimedCube;
    int Temp_Direction = 0;
    for (/*Temp_SizeOfCluster*/; Temp_SizeOfCluster > 0; Temp_SizeOfCluster--){             //do something for all the size of the cluster
                                                                                        //random direction the cluster should grow
                                                                                                //  0 1 2
                                                                                                //  7   3
                                                                                                //  6 5 4
        AimedCube.PosX = In_PosX;              //The Position we are aiming at
        AimedCube.PosY = In_PosY;
        AimedCube.PosZ = In_PosZ;

        bool AimingAtAGerm = true;
        //cout << getTimestampInMiliseconds() << " entering while loop" << endl;
        while(                                                                          //do suff as long as
                (AimingAtAGerm == true) &&                       // we are aiming at something that is the same as our selected object
                (AimedCube.PosY >= 0) &&                                                  //but we shouldn't get out of our valid area
                (AimedCube.PosY <= HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING) &&
                (AimedCube.PosX >= 0) &&
                (AimedCube.PosX <= HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING) &&
                (AimedCube.PosZ <= MAX_LEVEL_OVER_ZERO - 1) &&
                (AimedCube.PosZ >= 0)
                                                ) {

            Temp_Direction = rand() % 8;
            //cout << "Temp_Direction: " << Temp_Direction << endl;
            if( Temp_Direction == 0 ){                                   //this adjusts the aiming
                AimedCube.PosX--;
                AimedCube.PosY--;
            }else if( Temp_Direction == 1 ){
                AimedCube.PosY--;
            }else if( Temp_Direction == 2 ){
                AimedCube.PosX++;
                AimedCube.PosY--;
            }else if( Temp_Direction == 3 ){
                AimedCube.PosX++;
            }else if( Temp_Direction == 4 ){
                AimedCube.PosX++;
                AimedCube.PosY++;
            }else if( Temp_Direction == 5 ){
                AimedCube.PosY++;
            }else if( Temp_Direction == 6 ){
                AimedCube.PosX--;
                AimedCube.PosY++;
            }else if( Temp_Direction == 7 ){
                AimedCube.PosX--;
            }

            if( (AimedCube.PosY < 0) || (AimedCube.PosX < 0) ){          //but if we left the valid area with that

                #if DEBUG_OUTPUT
                    cout<< "left valid area. reseting positions" << endl;
                #endif // DEBUG_OUTPUT
                AimedCube.PosX = In_PosX;                                                           //we set the aim back to where we started
                AimedCube.PosY = In_PosY;
                AimedCube.PosZ = In_PosZ;

            }

            AimingAtAGerm = GermWorld[AimedCube.PosY][AimedCube.PosX][AimedCube.PosZ].CubeStack[0]->isGermForMountainOrValley();   //in any case we check what we found at the aimed position

            position Temp_PositionOfGerm;
            int Temp_SizeOfVec_GermPositions = Vec_GermPositions.size();
            //cout << getTimestampInMiliseconds() << " trying to push position of AimedCube back on Vec_GermPositions[" << Temp_SizeOfCluster << "]" << endl;
            Vec_GermPositions[Temp_SizeOfVec_GermPositions - 1].push_back(AimedCube);
        }//and go back to the loop. we will do this as long as we get to something that is not a germ

        GermWorld[AimedCube.PosY][AimedCube.PosX][AimedCube.PosZ].CubeStack[0]->setTypeOfCube(solid);
        GermWorld[AimedCube.PosY][AimedCube.PosX][AimedCube.PosZ].CubeStack[0]->setIsGermForMountainOrValley(true );                //then we place the object
        #if DEBUG_OUTPUT
            cout << getTimestampInMiliseconds() << " placed new solid Germ on Y: " << AimedCube.PosY << " X: " << AimedCube.PosX << " Z: " << AimedCube.PosZ << endl;
        #endif // DEBUG_OUTPUT
    }

    cout << "leaving  world::makeClustersOfGerms()" << endl;
}


void world::makeLastGermedMountainLessSteep(){  //called from: setRandomGermsForMountainsAndValleysIntoGermWorld
    //!this function was written very quickly and i thought, it will cause a lot of trouble. but it seems to work out of the box ;-) . it just takes a bit time. but i leave it like that..

    cout << getTimestampInMiliseconds() << " entering world::makeLastGermedMountainLessSteep()" << endl;

    int Temp_SizeOfVec_GermPositions = Vec_GermPositions.size();
    while(Vec_GermPositions[Temp_SizeOfVec_GermPositions - 1].size() > 0){
        position Temp_PositionOfCurrentGerm = Vec_GermPositions[Temp_SizeOfVec_GermPositions - 1][ Vec_GermPositions[Temp_SizeOfVec_GermPositions - 1].size() - 1 ];
        Vec_GermPositions[Temp_SizeOfVec_GermPositions - 1].pop_back();

        position Temp_PositionOfNewSetGerm;
        Temp_PositionOfNewSetGerm = Temp_PositionOfCurrentGerm;

        Temp_PositionOfNewSetGerm.PosZ--;

        if(Temp_PositionOfCurrentGerm.PosZ > AVERAGE_LEVEL_OF_GROUND){
            for( int J = 0; J < 25; J++ ){

                   /*!

                   9  10 11 12  13

                   24  1  2  3  14

                   23  8  0  4  15

                   22  7  6  5  16

                   21 20  19 18 17

                   */

                if( J == 0 ){
                    //just on under the original
                }else if( J == 1 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY - 1;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX - 1;
                }else if( J == 2 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY - 1;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX;
                }else if( J == 3 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY - 1;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX + 1;
                }else if( J == 4 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX + 1;
                }else if( J == 5 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY + 1;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX + 1;
                }else if( J == 6 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY + 1;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX;
                }else if( J == 7 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY + 1;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX - 1;
                }else if( J == 8 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX - 1;
                }else if( J == 9 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY - 2;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX - 2;
                }else if( J == 10 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY - 2;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX - 1;
                }else if( J == 11 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY - 2;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX;
                }else if( J == 12 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY - 2;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX + 1;
                }else if( J == 13 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY - 2;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX + 2;
                }else if( J == 14 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY - 1;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX + 2;
                }else if( J == 15 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX + 2;
                }else if( J == 16 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY + 1;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX + 2;
                }else if( J == 17 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY + 2;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX + 2;
                }else if( J == 18 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY + 2;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX + 1;
                }else if( J == 19 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY + 2;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX;
                }else if( J == 20 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY + 2;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX - 1;
                }else if( J == 21 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY + 2;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX - 2;
                }else if( J == 22 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY + 1;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX - 2;
                }else if( J == 23 ){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX - 2;
                }else if( J == 24){
                    Temp_PositionOfNewSetGerm.PosY = Temp_PositionOfCurrentGerm.PosY - 1;
                    Temp_PositionOfNewSetGerm.PosX = Temp_PositionOfCurrentGerm.PosX - 2;
                }

            //setting the cubes as germs
            //checking valid positions
                if(Temp_PositionOfNewSetGerm.PosX > 0 && Temp_PositionOfNewSetGerm.PosY > 0 && Temp_PositionOfNewSetGerm.PosX < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING && Temp_PositionOfNewSetGerm.PosY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING){
                    #if DEBUG_OUTPUT
                        cout << getTimestampInMiliseconds() << " trying to set Temp_PositionOfNewSetGerm solid and as germ: " << Temp_PositionOfNewSetGerm.PosY << " ; " << Temp_PositionOfNewSetGerm.PosX << " ; " << Temp_PositionOfNewSetGerm.PosZ << endl;
                    #endif // DEBUG_OUTPUT
                    GermWorld[Temp_PositionOfNewSetGerm.PosY][Temp_PositionOfNewSetGerm.PosX][Temp_PositionOfNewSetGerm.PosZ].CubeStack[0]->setTypeOfCube(solid);
                    GermWorld[Temp_PositionOfNewSetGerm.PosY][Temp_PositionOfNewSetGerm.PosX][Temp_PositionOfNewSetGerm.PosZ].CubeStack[0]->setIsGermForMountainOrValley(true);

                    #if DEBUG_OUTPUT
                        cout << getTimestampInMiliseconds() << " trying to push new positon of generated germ on Vec_GermPositions[" << Temp_SizeOfVec_GermPositions - 1 << "]" << endl;
                    #endif // DEBUG_OUTPUT
                    Vec_GermPositions[Temp_SizeOfVec_GermPositions - 1].push_back(Temp_PositionOfNewSetGerm);
                }else{
                    #if DEBUG_OUTPUT
                        cout << getTimestampInMiliseconds() << " Temp_PositionOfNewSetGerm.PosX > 0 && Temp_PositionOfNewSetGerm.PosY > 0 && Temp_PositionOfNewSetGerm.PosX < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING && Temp_PositionOfNewSetGerm.PosY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING nicht erf?llt" << endl;
                    #endif // DEBUG_OUTPUT
                }
            }

        }else{
            #if DEBUG_OUTPUT
                cout << getTimestampInMiliseconds() << " AVERAGE_LEVEL_OF_GROUND is reached" << endl;
            #endif // DEBUG_OUTPUT
        }

    }

    cout << getTimestampInMiliseconds() << " leaving world::makeLastGermedMountainLessStepp()" << endl;

}


void world::placeClusterOfCubeOnSurface(standardCube In_TypeOfCubes){

    cout << getTimestampInMiliseconds() << " entering world::placeClusterOfCubeOnSurface(), sleeping for 3 seconds" << endl;

    //!lets make clusters out of the cubes
    #if DEBUG_OUTPUT
        cout << "\n" << getTimestampInMiliseconds() << " entering world::placeClusterOfCubeOnSurface(), sleeping for 3 seconds" << endl;
        system("sleep 3");
    #endif // DEBUG_OUTPUT

    //OriginalWorldQuadrant1 = WorldQuadrant1;        //this is needed because otherwise we get a mess. we just take the information of the OriginalWorldQuadrant and make cluster out of it to insert those in the "real" WorldQuadratn1. if we would do the same form the same world as source and aim, that wouldnt work and i cant explain because i'm drunk!
    vector < vector < cube >> Temp_2DWorld;         //will be copied to the real world later

    //int Debug_WorldSize = World.size();
    for(size_t Y = 0; Y < WorldVector.size(); Y++){
        vector < cube > Temp_XVector;
        for(size_t X = 0; X < WorldVector[Y].size(); X++){
            cube Temp_Cube;
            Temp_XVector.push_back(Temp_Cube);
        }
        Temp_2DWorld.push_back(Temp_XVector);
    }

    int Temp_ProbabilityOfRequestedCube = 0;
    if(In_TypeOfCubes == grass){
        Temp_ProbabilityOfRequestedCube = PROBABILITY_OF_GETTING_GRASS;
    }else if(In_TypeOfCubes == sand){
        Temp_ProbabilityOfRequestedCube = PROBABILITY_OF_GETTING_SAND;
    }else if( In_TypeOfCubes == solid ){
        Temp_ProbabilityOfRequestedCube = PROBABILITY_OF_GETTING_SOLID;
    }else if( In_TypeOfCubes == gravel ){
        Temp_ProbabilityOfRequestedCube = PROBABILITY_OF_GETTING_GRAVEL;
    }else{
        cout << getTimestampInMiliseconds() << " requested Cube-Type is not implemented (yet). exit now" << endl;
        system("sleep(5)");
        exit(0);
    }

    for(int I = 0; I < Temp_ProbabilityOfRequestedCube; I++){
        int Temp_PosX = rand()%HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING;
        int Temp_PosY = rand()%HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING;

        Temp_2DWorld[Temp_PosY][Temp_PosX].setTypeOfCube(In_TypeOfCubes);
    }

    //!so now we have "Germs" for the requested Cube aswell... lets make clusters

    vector < vector < cube >> Backup_2DWorld = Temp_2DWorld; //conatins just the germs

    for(int Y = 0; Y < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING; Y++){
        for(int X = 0; X < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING; X++){
            #if DEBUG_OUTPUT
                cout << getTimestampInMiliseconds() << " checking 2DWorld, if != none: Y: " << Y << " X: " << X << endl;
            #endif // DEBUG_OUTPUT
            //standardCube Debug_TypeOfCube = Backup_2DWorld[Y][X].getTypeOfCube();
            if(Backup_2DWorld[Y][X].getTypeOfCube() != none){             //get from the original generated world the object which are not blank
                standardCube Temp_CubeType;
                Temp_CubeType = Backup_2DWorld[Y][X].getTypeOfCube();        //and safe the type of this object for later

                int Temp_SizeOfCluster = 0;

                if( Temp_CubeType == grass ){      //lets make a cluster of grass
                    int Temp_Probability_of_Positiv_or_Negativ = rand()%2;
                    if( Temp_Probability_of_Positiv_or_Negativ == 0) {          //Positiv delta
                        Temp_SizeOfCluster = ( AVERAGE_CLUSTER_SIZE_FOR_GRASS + ( rand() % DELTA_CLUSTER_SIZE_FOR_GRASS ) );
                    }else if( Temp_Probability_of_Positiv_or_Negativ == 1 ){    //Negativ delta
                        Temp_SizeOfCluster = ( AVERAGE_CLUSTER_SIZE_FOR_GRASS - ( rand() % DELTA_CLUSTER_SIZE_FOR_GRASS ) );
                    }
                }else if( Temp_CubeType == sand ){  //sand
                    int Temp_Probability_of_Positiv_or_Negativ = rand()%2;
                    if( Temp_Probability_of_Positiv_or_Negativ == 0) {          //Positiv delta
                        Temp_SizeOfCluster = ( AVERAGE_CLUSTER_SIZE_FOR_SAND + ( rand() % DELTA_CLUSTER_SIZE_FOR_SAND ) );
                    }else if( Temp_Probability_of_Positiv_or_Negativ == 1 ){    //Negativ delta
                        Temp_SizeOfCluster = ( AVERAGE_CLUSTER_SIZE_FOR_SAND - ( rand() % DELTA_CLUSTER_SIZE_FOR_SAND ) );
                    }
                }else if( Temp_CubeType == solid ){

                    int Temp_Probability_of_Positiv_or_Negativ = rand()%2;
                    if( Temp_Probability_of_Positiv_or_Negativ == 0) {          //Positiv delta
                        Temp_SizeOfCluster = ( AVERAGE_CLUSTER_SIZE_FOR_SOLID + ( rand() % DELTA_CLUSTER_SIZE_FOR_SOLID ) );
                    }else if( Temp_Probability_of_Positiv_or_Negativ == 1 ){    //Negativ delta
                        Temp_SizeOfCluster = ( AVERAGE_CLUSTER_SIZE_FOR_SOLID - ( rand() % DELTA_CLUSTER_SIZE_FOR_SOLID ) );
                    }
                }else if( Temp_CubeType == gravel ){
                    int Temp_Probability_of_Positiv_or_Negativ = rand()%2;
                    if( Temp_Probability_of_Positiv_or_Negativ == 0) {          //Positiv delta
                        Temp_SizeOfCluster = ( AVERAGE_CLUSTER_SIZE_FOR_GRAVEL + ( rand() % DELTA_CLUSTER_SIZE_FOR_GRAVEL ) );
                    }else if( Temp_Probability_of_Positiv_or_Negativ == 1 ){    //Negativ delta
                        Temp_SizeOfCluster = ( AVERAGE_CLUSTER_SIZE_FOR_GRAVEL - ( rand() % DELTA_CLUSTER_SIZE_FOR_GRAVEL ) );
                    }

                }else{
                    cout << "requestet Temp_CubeType is not implemented. exit now!" << endl;
                    system("sleep(5)");
                    exit(0);
                }

                //!now we know how big the cluster should be, and the type of objects. lets rock!

                int AimedObjectX = X;
                int AimedObjectY = Y;
                standardCube Temp_TypeOfCubeWeAreAimingAt;
                int Temp_Direction = 0;

                #if DEBUG_OUTPUT
                    cout << getTimestampInMiliseconds() << " starting for loop. Temp_SizeOfCluster = " << Temp_SizeOfCluster << endl;
                #endif // DEBUG_OUTPUT
                //system("sleep 3");
                for (int I = 0; I < Temp_SizeOfCluster; I++){             //do something for all the size of the cluster
                                                                         //random direction the cluster should grow
                                                                                                    //  0 1 2
                                                                                                    //  7   3
                                                                                                    //  6 5 4


                    AimedObjectX = X;                                                               //The Position we are aiming at
                    AimedObjectY = Y;

                    Temp_TypeOfCubeWeAreAimingAt = Temp_CubeType;                //start with the same object as the while-loop wouldn't work otherwise

                    int Counter = 0;

                    //!this while loop is searching for a cube, which is not already from the type we put in here
                    while(                                                                        //do suff as long as
                            (Temp_TypeOfCubeWeAreAimingAt == Temp_CubeType) &&                       // we are aiming at something that is the same as our selected object
                            (AimedObjectY >= 0) &&                                                  //but we shouldn't get out of our valid area
                            (AimedObjectY < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING) &&
                            (AimedObjectX >= 0) &&
                            (AimedObjectX < HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING) &&
                            Counter < 3000                                                          //it might be already full. so this is the solution to not get a endless loop
                          ) {

                        Temp_Direction = rand() % 8;

                        if( Temp_Direction == 0 ){                                   //this adjusts the aiming
                            AimedObjectX--;
                            AimedObjectY--;
                        }else if( Temp_Direction == 1 ){
                        //    AimedObjectX--;
                            AimedObjectY--;
                        }else if( Temp_Direction == 2 ){
                            AimedObjectX++;
                            AimedObjectY--;
                        }else if( Temp_Direction == 3 ){
                            AimedObjectX++;
                        //    AimedObjectY--;
                        }else if( Temp_Direction == 4 ){
                            AimedObjectX++;
                            AimedObjectY++;
                        }else if( Temp_Direction == 5 ){
                            AimedObjectY++;
                        }else if( Temp_Direction == 6 ){
                            AimedObjectX--;
                            AimedObjectY++;
                        }else if( Temp_Direction == 7 ){
                            AimedObjectX--;
                        }


                        if(     (AimedObjectY < 0) ||                                                   //but if we left the valid area with that
                                (AimedObjectY >= HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING) ||
                                (AimedObjectX < 0)   ||
                                (AimedObjectX >= HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING)
                                                                                                                    ){

                            AimedObjectX = X;                                                           //we set the aim back to where we started
                            AimedObjectY = Y;
                        }

                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " I : " << I << " ; while loop counter: " << Counter << " ; trying to get type of object on Y: " << AimedObjectY << " X: " << AimedObjectX << endl;
                        #endif // DEBUG_OUTPUT
                        Temp_TypeOfCubeWeAreAimingAt = Temp_2DWorld[AimedObjectY][AimedObjectX].getTypeOfCube(); //in any case we check what we found at the aimed position
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " cube is: " << Temp_TypeOfCubeWeAreAimingAt << endl;
                        #endif // DEBUG_OUTPUT

                        Counter++;
                    }                               //and go back to the loop. we will do this as long as we get to something that is from another type than our source-object


                  //      cout << getTimestampInMiliseconds() << " try to placed new object on Y: \t" << AimedObjectY << " X: \t" << AimedObjectX << endl;
                    Temp_2DWorld[AimedObjectY][AimedObjectX].setTypeOfCube(Temp_CubeType);                //then we place the object

                    #if DEBUG_OUTPUT
                        cout << getTimestampInMiliseconds() << " placed new object on Y: \t" << AimedObjectY << " X: \t" << AimedObjectX  <<  "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
                    #endif // DEBUG_OUTPUT

                }       //and do the same thing again as long as the cluster is big enough
            }
        }
    }

    //!now we have the clusters in a 2D world. now we need them to be copied on the surface of the real world:
    #if DEBUG_OUTPUT
        cout << getTimestampInMiliseconds() << " copying 2D World down to real world" << endl;
    #endif // DEBUG_OUTPUT
    for(size_t Y = 0; Y < WorldVector.size(); Y++){
        #if DEBUG_OUTPUT
            cout << getTimestampInMiliseconds() << " Y: " << Y << endl;
        #endif // DEBUG_OUTPUT
        for(size_t X = 0; X < WorldVector[Y].size(); X++){
            #if DEBUG_OUTPUT
                cout << getTimestampInMiliseconds() << "  X: " << X << endl;
            #endif // DEBUG_OUTPUT
            if( Temp_2DWorld[Y][X].getTypeOfCube() != none ){       //if the 2D-world is not a "none"-cube
                bool Found = false;
                int Temp_Z = -1;
                for(int I = MAX_LEVEL_OVER_ZERO - 2; Found == false && I >= 0;I--){
                    #if DEBUG_OUTPUT
                        cout << getTimestampInMiliseconds() << " trying to get Type Of Cube on Position: " << Y << " ; " << X  << " ; " << I << endl;
                    #endif // DEBUG_OUTPUT
                    if( WorldVector[Y][X][I].CubeStack[0]->getTypeOfCube() == solid &&
                        WorldVector[Y][X][I+1].CubeStack[0]->getTypeOfCube() == none
                            ){
                        #if DEBUG_OUTPUT
                            cout << getTimestampInMiliseconds() << " is solid" << endl;
                        #endif // DEBUG_OUTPUT
                        Found = true;
                        Temp_Z = I;
                    }
                }
                if(Found && (Temp_Z + 1) >= 0 && (Temp_Z + 1) < MAX_LEVEL_OVER_ZERO){      //if solid ground has been found, copy cube
                    WorldVector[Y][X][Temp_Z + 1].CubeStack[0]->setTypeOfCube(In_TypeOfCubes);
                }else{
                    #if DEBUG_OUTPUT
                        cout << getTimestampInMiliseconds() << " Bedinung: Found && (Temp_Z + 1) >= 0 && (Temp_Z + 1) < MAX_LEVEL_OVER_ZERO nicht erf?llt!" << endl;
                    #endif // DEBUG_OUTPUT
                }
            }
        }
    }

      cout << getTimestampInMiliseconds() << " leaving world::placeClusterOfCubeOnSurface(), sleeping for 3 seconds" << endl;
}



/*!
void world::addCharacterToCubeStack(position In_Position, int In_UserID ){

    cout << getTimestampInMiliseconds() << " world::addCreatureToCubeStack() : In_Position " << In_Position.PosY << ";" << In_Position.PosX << ";" << In_Position.PosZ << " In_UserID: " << In_UserID <<  endl;

    if( WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack[0]->getTypeOfCube() == none ){

        //! if the cube on cubestack at this position is "none" (that can only be the case if there is no other cube on that stack)
        //! then we just change that cube to a characterCube

        WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack[0]->setTypeOfCube( characterCube, In_UserID );

        //!#ifdef USE_COMPLICATED_VOODOO
        markHeadAsChanged( In_Position );
        //!#endif // USE_COMPLICATED_VOODOO

        //! now we need to make that changes also in the database
        Database->replaceCubeWithCharacterCube( WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack[0], In_Position );


    }else{  // anything but 'none'

        //! in the case, the first cube on the stack is not a "none"-cube, than we just add a cube with type CharacterCube

        cube* Temp_CreatureCube = new cube(characterCube);
        Temp_CreatureCube->setIDofAccountSittingAtThisCube(In_UserID);
        WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack.push_back(Temp_CreatureCube);

        int Index_WhereItHasBeenPushedTo = ( WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ] ).CubeStack.size() - 1;

        //!aaaannnndddd... keep the database syncron...
        Database->addCharacterCubeToCubestack( WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack[Index_WhereItHasBeenPushedTo], In_Position );
    }

    cout << getTimestampInMiliseconds() << " end of world::addCharacterToCubeStack()" << endl;
}
*/

/*!
void world::removeCharacterCubeFromPosition(int In_AccountID, position In_Position){

    bool found = false;
    for (size_t I = 0; I < WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack.size() && found == false; I++){
        standardCube Temp_CubeTypeOnStack = WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack[I]->getTypeOfCube();
        if( Temp_CubeTypeOnStack == characterCube && WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack[I]->getIDofAccountSittingAtThisCube() == In_AccountID ){
            if( I  == 0 ){
                                                          //if cube is the first on the vector, we need to turn it into none

                WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack[0]->setIDofAccountSittingAtThisCube( -1 );   //just for safety reasons
                WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack[0]->setTypeOfCube( none );

            }else{                                                                                  //otherwise we just turn it to none

                WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack.erase(WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack.begin() + I);

            }
        }

    }
    //!#ifdef USE_COMPLICATED_VOODOO
    markHeadAsChanged(In_Position);
    //!#endif // USE_COMPLICATED_VOODOO


    //! and again, we need to keep the database in sync
    Database->removeCharacterCubeFromCubestack( In_Position );


}
*/



vector <int> world::getIDsofAccountsWhoCanSeePosition(position In_Position){
    //vector <int> Output;

    return WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].AccountsCurrentlySeeThisPosition;

}

string world::getTimestampInMiliseconds(){

    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;

    string Output = to_string(ms);
    Output.erase( 0, 4 );

    return Output;
}


void world::extendTheWorldWithFlatLand( int In_NumberOfCubesToAdd, direction In_DirectionWhereTheWorldShouldBeExtanden ){

    //!debug:
    string Direction;
    if( In_DirectionWhereTheWorldShouldBeExtanden == d_right ){
        Direction = "right";
    }else if( In_DirectionWhereTheWorldShouldBeExtanden == d_down ){
        Direction = "down";
    }else{
        Direction = "wrong direction parameter";
    }



    cout << getTimestampInMiliseconds() << " entering world::exextendTheWorldWithFlatLand(): In_NumberOfCubesToAdd: " << In_NumberOfCubesToAdd << " In_DirectionWhereTheWorldShouldBeExtanden: " << Direction << endl;


    if( In_DirectionWhereTheWorldShouldBeExtanden == d_right ){




        for(int Y = 0; Y < WorldSize.Height; Y++){


            for( int X = WorldSize.Width; X < (WorldSize.Width + In_NumberOfCubesToAdd); X++ ){
                vector < head > ZVector;

                for( int Z = 0; Z < MAX_LEVEL_OVER_ZERO; Z++ ){
                    head Temp_Head;
                    position Position;
                    Position.PosY = Y;
                    Position.PosX = X;
                    Position.PosZ = Z;
                    cube* Temp_Cube = new cube;
                    if( Z < AVERAGE_LEVEL_OF_GROUND ){
                        Temp_Cube->setTypeOfCube(solid);           //making a solid ground
                    }else{
                        Temp_Cube->setTypeOfCube(none);            //or thin air
                    }

                    Temp_Head.CubeStack.push_back(Temp_Cube);
                    ZVector.push_back(Temp_Head);
                }
                WorldVector[Y].push_back(ZVector);


            }

        }

        WorldSize.Width += In_NumberOfCubesToAdd;

    }else if(In_DirectionWhereTheWorldShouldBeExtanden == d_down){



        for(int Y = WorldSize.Height; Y < (WorldSize.Height + In_NumberOfCubesToAdd); Y++){
            vector < vector < head >>XVector;
            for(int X = 0; X < WorldSize.Width; X++){
                vector < head > ZVector;
                for( int Z = 0; Z < MAX_LEVEL_OVER_ZERO; Z++ ){
                    head Temp_Head;
                    position Position;
                    Position.PosY = Y;
                    Position.PosX = X;
                    Position.PosZ = Z;
                    cube* Temp_Cube = new cube/*( Position )*/;
                    if( Z < AVERAGE_LEVEL_OF_GROUND ){
                        Temp_Cube->setTypeOfCube(solid);           //making a solid ground
                    }else{
                        Temp_Cube->setTypeOfCube(none);            //or thin air
                    }
                    Temp_Head.CubeStack.push_back(Temp_Cube);
                    ZVector.push_back(Temp_Head);
                }
                XVector.push_back(ZVector);
            }
            WorldVector.push_back(XVector);
        }

        WorldSize.Height += In_NumberOfCubesToAdd;


    }else{
        cout << "world::extendTheWorldWithFlatLand():  given direction is not 'down' or 'right'. exit now!" << endl;
        system("sleep(5)");
        exit(0);
    }


    cout << getTimestampInMiliseconds() << " end of world::extendTheWorldWithFlatLand(). sleeping for 2 seconds" << endl;
    system("sleep 2");


}



worldSize world::getWorldSize(){

    return WorldSize;
}

/*!
int world::getMaxLevelOverZero(){

    return WorldSize.MaxLevelOverZero;
}
*/



/*!

bool world::setSolidCubeOnPosition( position In_Position ){
    bool Output = false;

    vector <cube*> Temp_Cubes = getCubesOnPositionFromNormalWorld(In_Position);
    if (Temp_Cubes.size() == 1){
        if ( ( *(Temp_Cubes[0]) ).getTypeOfCube() == characterCube ){    // the only cube will be the character-cube because the character still sits on this position
            Output = true;                                      //only possible, if the cube-type is 'none'

            (*(Temp_Cubes[0])).setTypeOfCube(solid);

        }else{
            Output = false;                                     //not possible, because requestet cube-type is not 'none'!
        }


    }else{
        Output = false;         //not possible, because, there is more the one cube on the requestet position
    }


    return Output;
}
*/

bool world::positionIsValid( position In_Position ){
    bool Output = false;

    if(
        In_Position.PosY >= 0 &&
        In_Position.PosY < WorldSize.Height &&
        In_Position.PosX >= 0 &&
        In_Position.PosX < WorldSize.Width &&
        In_Position.PosZ >= 0 &&
        In_Position.PosZ < MAX_LEVEL_OVER_ZERO
                                                        ){

        Output = true;
    }else{
        //cout << "world::positionIsValid(): debug: In_Position: " << In_Position << " is not valid" << endl;
    }

    return Output;
}





//! PLEASE!!! only use this function with valid positions. you can use world::positionIsValid() to check this.
//! if you don't, the server will crash!
void world::addIDofAAccountWhoCanSeePosition(int In_AccountIDtoAdd, position In_ValidPosition){

    //cout << "entering world::addIDofAAccountWhoCanSeePosition(): In_ValidPosition: " << In_ValidPosition << endl;

    //check if the information is already on the position. that might be the reason, why it slowed down after a while
    bool NeedsToBeAdded = true;
    for( size_t Index = 0; Index < WorldVector[In_ValidPosition.PosY][In_ValidPosition.PosX][In_ValidPosition.PosZ].AccountsCurrentlySeeThisPosition.size(); Index++ ){
        if( WorldVector[In_ValidPosition.PosY][In_ValidPosition.PosX][In_ValidPosition.PosZ].AccountsCurrentlySeeThisPosition[Index] == In_AccountIDtoAdd ){
            NeedsToBeAdded = false;
        }
    }

    // if not already there, it needs to be added
    if( NeedsToBeAdded == true ){
        WorldVector[In_ValidPosition.PosY][In_ValidPosition.PosX][In_ValidPosition.PosZ].AccountsCurrentlySeeThisPosition.push_back(In_AccountIDtoAdd);
    }



    //    cout << "world::addIDofAAccountWhoCanSeePosition(): position validation failed!" << endl;



}



//! PLEASE!!! only use this function with valid positions. you can use world::positionIsValid() to check this.
//! if you don't, the server will crash!
void world::removeIDofAAccountWhoCanSeePosition(int In_AccountIDtoRemove, position In_ValidPosition/*, int* In_Counter*/){

    //cout << "entering world::removeIDofAAccountWhoCanSeePosition(): In_Position: " << In_ValidPosition << endl;

    bool found = false;
    for( size_t Index = 0; Index < WorldVector[In_ValidPosition.PosY][In_ValidPosition.PosX][In_ValidPosition.PosZ].AccountsCurrentlySeeThisPosition.size() && found == false; Index++ ){
        //cout << "world::removeIDofAAccountWhoCanSeePosition(): debug: World[" << In_ValidPosition.PosY << "][" << In_ValidPosition.PosX << "][" << In_ValidPosition.PosZ << "]: I: " << Index << endl;
        if( WorldVector[In_ValidPosition.PosY][In_ValidPosition.PosX][In_ValidPosition.PosZ].AccountsCurrentlySeeThisPosition[Index] == In_AccountIDtoRemove ){
            found = true;

            //!WorldVector[In_ValidPosition.PosY][In_ValidPosition.PosX][In_ValidPosition.PosZ].first.erase(World[In_ValidPosition.PosY][In_ValidPosition.PosX][In_ValidPosition.PosZ].first.begin(),World[In_Position.PosY][In_Position.PosX][In_Position.PosZ].first.begin()+I);
            //!is that cool like ^that?
            //!isn't it better like that:
            WorldVector[In_ValidPosition.PosY][In_ValidPosition.PosX][In_ValidPosition.PosZ].AccountsCurrentlySeeThisPosition.erase( WorldVector[In_ValidPosition.PosY][In_ValidPosition.PosX][In_ValidPosition.PosZ].AccountsCurrentlySeeThisPosition.begin()+Index );
            //!yep, seems to be better like that
        }
    }



    //    cout << "world::removeIDofAAccountWhoCanSeePosition(): position validation failed!" << endl;

}

//!#ifdef USE_COMPLICATED_VOODOO
head* world::getHeadPointerFromPosition( position In_Position ){
    return &WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ];
}

void world::addIDto_ClientsSawThisPositionInThisSession( int In_ClientID, position In_Position ){
    cout << "entering world::addIDto_AccountsSawThisPositionInThisSession() In_Position: " << In_Position << endl;

    //cout << "worldVector.size(): " << WorldVector.size() << endl;
    //cout << "WorldVector[" << In_Position.PosY << "].size(): " << WorldVector[In_Position.PosY].size() << endl;
    //cout << "WorldVector[" << In_Position.PosY << "][" << In_Position.PosX << "].size(): " << WorldVector[In_Position.PosY][In_Position.PosX].size() << endl;
    //cout << "WorldVector[" << In_Position.PosY << "][" << In_Position.PosX << "][" << In_Position.PosZ << "].size(): " << WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].size() << endl;
    if( WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].ClientsSawThisPositionInThisSession.count(In_ClientID) == false ){  //if not already in the list
        //cout << "trying to add ID to map" << endl;
        (WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ]).ClientsSawThisPositionInThisSession[In_ClientID] = false;   //'false' indicates, that the cube hasn't changed since the account saw it the last time
        //cout << "done" << endl;
    }
    //cout << "leaving world::addIDto_AccountsSawThisPositionInThisSession()" << endl;
}

map<int, bool>* world::getPointerTo_ClientsSawThisPositionInThisSession( position In_Position ){
    return &(WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].ClientsSawThisPositionInThisSession);
}

void world::markHeadAsChanged( position In_Position ){
    map<int,bool>* Map_ClientsSawThisPositionInThisSession = &(WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].ClientsSawThisPositionInThisSession);
    //!debug world::markHeadAsChanged
    //if( In_Position.PosY == 10 && In_Position.PosX == 10 && In_Position.PosZ == 7 ){
    //    cout << "break" << endl;
    //}
    //!
    for( map<int,bool>::iterator Iterator = Map_ClientsSawThisPositionInThisSession->begin(); Iterator!=Map_ClientsSawThisPositionInThisSession->end(); ++Iterator ){
        Iterator->second = true;
    }
    //cout << "leaving world::markHeadAsChanged" << endl;
}
//!#endif // USE_COMPLICATED_VOODOO





vector< vector <vector <head> > >* world::getPointerToWorldVector(){

    return &WorldVector;

}


/*
void world::setWorldSize( worldSize In_WorldSize ){
    WorldSize = In_WorldSize;
}

*/

void world::loadWorldFromDatabase(){

    cout << getTimestampInMiliseconds() <<  " entering loadWorldFromDatbase()" << endl;
    Database->loadWorldSizeInformation(&WorldSize);
    generateEmptyVectorFramesWithoutHeads( &WorldVector, WorldSize );
    Database->getWorld( &WorldVector, WorldSize );
    cout << getTimestampInMiliseconds() <<  " leaving loadWorldFromDatbase()" << endl;

}


void world::writeWorldInDatabase(){
    cout << getTimestampInMiliseconds() << " entering world::writeWorldInDatabase()" << endl;
    Database->writeWorld( &WorldVector, WorldSize );
    cout << getTimestampInMiliseconds() << " leaving world::writeWorldInDatabase()" << endl;
}




void world::generateEmptyVectorFramesWithoutHeads( vector<vector<vector<head>>>* In_EmptyVector, const worldSize In_Wordsize ){

    cout << getTimestampInMiliseconds() << " entering world::generateEmptyVectorFramesWithoutHeads(). In_WorldSize: Height: " << In_Wordsize.Height << " ; Width: " << In_Wordsize.Width << " ; MaxLevelOverZero: " << In_Wordsize.MaxLevelOverZero << endl;
    //system("sleep 3");

    for( size_t Dim_Y = 0; Dim_Y < In_Wordsize.Height; Dim_Y++ ){
        //cout << "Dim_Y: " << Dim_Y << endl;
        vector< vector< head > > Temp_Vectors_X;


        for( size_t Dim_X = 0; Dim_X < In_Wordsize.Height; Dim_X++ ){
            //cout << "Dim_X: " << Dim_X << endl;
            vector< head > Temp_Vectors_Z;

            for( size_t Dim_Z = 0; Dim_Z < In_Wordsize.MaxLevelOverZero; Dim_Z++ ){
                //cout << "Dim_Z: " << Dim_Z << endl;
                head Temp_Head;
                Temp_Vectors_Z.push_back( Temp_Head );
            }
            Temp_Vectors_X.push_back( Temp_Vectors_Z );

        }
        In_EmptyVector->push_back( Temp_Vectors_X );
    }

    cout << getTimestampInMiliseconds() << " leaving world::generateEmptyVectorFramesWithoutHeads()" << endl;

}


bool world::isSomeCharacterSittingOnPosition( position In_Position ){
    bool Output = false;
    const vector<cube*>* CubestackOnThatPosition = getPointerToCubeStackOnPositionFromNormalWorld(In_Position);

    for( size_t Index = 0; Index < CubestackOnThatPosition->size(); Index++ ){
        if( (*CubestackOnThatPosition)[Index]->getTypeOfCube() == characterCube ){
            Output = true;
        }
    }
    return Output;
}



bool world::addCubeToCubestack( position In_Position, cube* In_CubeToAdd ){
    cout << getTimestampInMiliseconds() << " entering world::addCubeToCubestack(), In_Position: " << In_Position << endl;
    bool Output = false;

    //!checking now if there is no other massive cube at cubestack, if the input-cube is massive
    //! that of course doesn't matter if in_cube is not massive.
    //! although that check might be redundant, i do it here!
    const vector<cube*>* CubeStackOnThatPosition = getPointerToCubeStackOnPositionFromNormalWorld(In_Position);
    bool InputCubeIsMassive = In_CubeToAdd->isMassive();
    bool OtherCubeOnStackIsAlreadyMassive = false;
    if( InputCubeIsMassive ){
        cout << "InputCubeIsMassive == true" << endl;
        for( size_t Index = 0; Index < CubeStackOnThatPosition->size() && (OtherCubeOnStackIsAlreadyMassive == false); Index++ ){
            OtherCubeOnStackIsAlreadyMassive = (*CubeStackOnThatPosition)[Index]->isMassive();
        }
        if( OtherCubeOnStackIsAlreadyMassive ){
            cout << "OtherCubeOnStackIsAlreadyMassive == true" << endl;
            return false;
        }else{
            cout << "OtherCubeOnStackIsAlreadyMassive == false" << endl;
            Output = true;
        }
    }else{
        Output = true;
    }

    cout << "InputCubeIsMassive == false" << endl;


    head* HeadOnPosition = &(WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ]);
    HeadOnPosition->CubeStack.push_back(In_CubeToAdd);



    Database->sync( In_Position, getPointerToHeadOnPositionFromNormalWorld(In_Position) );

    cout << getTimestampInMiliseconds() << " leaving world::addCubeToCubestack()" << endl;
    return Output;
}


void world::removeCubeFromCubestack( position In_Position, cube* In_CubeToRemove ){
    cout << getTimestampInMiliseconds() << " entering world::removeCubeToCubestack()" << endl;

    //!missing: remove cube from cubestack
    const vector<cube*>* Cubestack = getPointerToCubeStackOnPositionFromNormalWorld(In_Position);
    bool found = false;
    for( size_t Index = 0; Index < Cubestack->size() && found == false; Index++ ){
        if( (*Cubestack)[Index] == In_CubeToRemove ){

            WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack.erase( WorldVector[In_Position.PosY][In_Position.PosX][In_Position.PosZ].CubeStack.begin() + Index );

            found = true;
        }

    }
    if( found == false ){
        cout << "to remove cube is not on cubestack. exit now!" << endl;
        exit(1);
    }


    Database->sync( In_Position, getPointerToHeadOnPositionFromNormalWorld(In_Position) );
    cout << getTimestampInMiliseconds() << " leaving world::removeCubeToCubestack()" << endl;
}


position world::getResultingPositionAfterTryingToMoveToNewPosition( position In_NewPositionOfCharacter ){

    cout << getTimestampInMiliseconds() << " entering world::getResultingPositionAfterTryingToMoveToNewPosition(). In_NewPositionOfCharacter: " << In_NewPositionOfCharacter << endl;

    position Output;

    position PositionBelow = In_NewPositionOfCharacter;
    PositionBelow.PosZ--;

    //! checking requested position if is solid
    bool RequestedPositionIsMassive = positionIsMassive( In_NewPositionOfCharacter );

    //!checking position below, if is solid
    bool PositionBelowIsMassive = positionIsMassive( PositionBelow );

    if( RequestedPositionIsMassive == false ){
        cout << "RequestedPositionIsMassive == false" << endl;
        if( PositionBelowIsMassive == true ){
            cout << "PositionBelowIsMassive == true" << endl;
            Output = In_NewPositionOfCharacter;

        }else{  //position below is not massive

            // rekursion...
            cout << "re-enter getResultingPositionAfterTryingToMoveToNewPosition() with PositionBelow: " << PositionBelow << endl;
            Output = getResultingPositionAfterTryingToMoveToNewPosition(PositionBelow);

        }
    }

    if( RequestedPositionIsMassive ){
        cout << "RequestedPositionIsMassive == true" << endl;

        //! ok, on step up
        cout << "increasing PosZ" << endl;
        In_NewPositionOfCharacter.PosZ++;

        //! no rekursion here. we only want to make sure, character can step one cube up
        if( positionIsMassive(In_NewPositionOfCharacter) == false ){
            cout << "positionIsMassive(In_NewPositionOfCharacter) == flase" << endl;
            Output = In_NewPositionOfCharacter;
        }

    }


    cout << getTimestampInMiliseconds() << " leaving world::getResultingPositionAfterTryingToMoveToNewPosition(). Output: " << Output << endl;
    return Output;
}


bool world::positionIsMassive( position In_ToCheckPosition ){
    bool Output = false;

    vector < cube* > Temp_CubesOnThisPosition = getCubesOnPositionFromNormalWorld(In_ToCheckPosition);
    for( size_t I = 0; I < Temp_CubesOnThisPosition.size(); I++ ){
        if( Temp_CubesOnThisPosition[I]->isMassive() == true ){
            Output = true;
        }
    }
    return Output;
}
