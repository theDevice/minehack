#ifndef ACTION_H
#define ACTION_H

#include <iostream>

#include "config.h"

using namespace std;

class action{

    public:
        action();
        ~action();
        //action(const action& other);
        action& operator=(const action& other);

    protected:
        string Name = "none";

    private:
};

#endif // ACTION_H
