#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED


//!used to be in sController.cpp
//#define HEARTBLEED false              changed that to be a commandline-option or/and a config-file option
#define DISPLAY_WORLD true
#define PORT_NUMBER 51717
//#define PORT_NUMBER 50002
#define UNLOCKWRITINGMUTEX false
//#define DEBUG_GENERAL_INFORMATION false
//#define DEBUG_CALCULATE_SIGHT_FIELD false
//#define CALCULATE_SIGHT_FIELD_WITHOUT_NONE_CUBES true
#define HOW_MUCH_TO_EXTEND_THE_WORLD_WHEN_CHARACTER_COMES_TO_THE_EDGE 100
#define TRASHOLD_TO_EXTEND_WORLD_WHEN_CHARACTER_COMES_MIN_POSITIONS_TO_EDGE 50
#define DEFAULT_SIGHT_RADIUS 7
#define MAX_SIGHT_RADIUS 8
#define NUMBER_OF_CLIENTS 42        //offline and online!!
#define SPLIT_SERIALIZING_INTO_THREADS 2 // valid = '2', '4', '1'. if '1' there will be only the main thread


//! in sController.h
//#define RATIO_Z_RADIUS_TO_SIGHT_RADIUS 0.3
#define RATIO_Z_RADIUS_TO_SIGHT_RADIUS 1


//! in world.cpp
//world parameters
#define HOW_FAR_DO_WE_INIT_THE_WORLD_IN_THE_BEGINNING 30   // 150 for profiling
#define HOW_MUCH_BIGGER_SHOULD_THE_GERM_WORLD_BE 5
#define MAX_LEVEL_OVER_ZERO 14
#define AVERAGE_LEVEL_OF_GROUND 7
#define AVERAGE_CLUSTER_SIZE_FOR_GERMS 10
#define DELTA_CLUSTER_SIZE_FOR_GERMS 5
#define LEVEL_OF_WATER 6
#define MAX_HEIGHT_OF_MOUNTAINS 11  //should be above AVERAGE_LEVEL_OF_GROUND
#define MAX_HEIGHT_OF_VALLEYS 3
#define PROBABILITY_OF_GETTING_MOUNTAIN 7
#define PROBABILITY_OF_GETTING_VALLEY 10
#define PROBABILITY_OF_GETTING_A_LESS_STEEPER_MOUNTAIN 2 //3 means 3 : 1
#define MAKE_LESS_STEEPER_MOUNTAINS true

//tree parameters
#define NUMBER_OF_TREES 25
#define MAX_TRUNK_HEIGHT 5
#define MIN_TRUNK_HEIGHT 3
#define MINIMUM_NUMBER_OF_TREEBRANCHES 5
#define MAXIMUM_NUMBERS_OF_TREEBRANCHES 9
#define PROBABILITY_OF_GETTING_LEAFS_AND_TWIGS 2 //3 means 3 to 1 ; but 3 doesn't look good. 2 is better


//the DELTA defines how much the cluster-size changes. but on the long run they will have the AVERAGE-size
//#define AVERAGE_CLUSTER_SIZE_FOR_STONE 15
//#define DELTA_CLUSTER_SIZE_FOR_STONE 5
#define AVERAGE_CLUSTER_SIZE_FOR_GRASS 45
#define DELTA_CLUSTER_SIZE_FOR_GRASS 20
#define AVERAGE_CLUSTER_SIZE_FOR_SAND 45
#define DELTA_CLUSTER_SIZE_FOR_SAND 20
#define AVERAGE_CLUSTER_SIZE_FOR_SOLID 1000
#define DELTA_CLUSTER_SIZE_FOR_SOLID 5
#define AVERAGE_CLUSTER_SIZE_FOR_GRAVEL 10
#define DELTA_CLUSTER_SIZE_FOR_GRAVEL 5

//#define PROBABILITY_OF_GETTING_STONE 5
#define PROBABILITY_OF_GETTING_GRASS 40
#define PROBABILITY_OF_GETTING_SAND 40
#define PROBABILITY_OF_GETTING_SOLID 4
#define PROBABILITY_OF_GETTING_GRAVEL 30

//#define DEBUG_GENERAL_INFORMATION false
//#define DEBUG_WRITE_WORLD_IN_FILE false
//#define DEBUG_OUTPUT false

//#define JUST_MAKE_FLAT_WORLD true       //thats handy for profiling, because it will be the same in every version
                                            //i changed that to be a commandline option (in main.cpp)

//! in cube.cpp
//#define DEBUG_IS_GERM_FOR_MOUNTAIN_OR_VALLEY false
//#define DEBUG_GERMWORLD false
//#define DEBUG_GENERAL_INFORMATION false

//! in character.cpp
#define NUMBER_OF_SHELLS 1

//! in connectedClient.cpp
#define UNLOCKWRITINGMUTEX false        //set true to force server to send a unlock-command to client


//! new
//!#define USE_COMPLICATED_VOODOO         // hard to explain...
                                        // you might know, i filtered none-cubes while serializing to reduce
                                        // traffic and speed (at least i thought, it could be a speed improvement).
                                        // because of the new DiffSightField architecture, which is very efficient but
                                        // also complicated, i then had the problem, that a change of a cube wouldn't
                                        // be displayed at client side, as long as the cube changed from 'something' to 'none'
                                        // (because it then get filtered). but that change used to happen a lot especially after
                                        // each movement of a character, because that cube than changed from a 'character-cube'
                                        // to (in most cases) a 'none'-cube. one solution was to turn off the 'none'-filtering,
                                        // which still works fine and you can turn this on by un-defining the USE_COMPLICATED_VOODOO-variable.
                                        // that of courses increases the traffic.
                                        // by defining this variable,
                                        // there will be a list (actually a map) in the head-struct (the head-struct is a collection of stuff i need at every position in the world),
                                        // called AccountsSawThisPositionInThisSession. in this map
                                        // i save as a key value the client-ID of all clients, which saw
                                        // this position in their current session. the value of this map is
                                        // a bool set to 'false' by default. that means, that the cube-stack
                                        // on this position hasn't changed since the client got it last sent.
                                        // every time, a cube on this position changes, the whole lot of values
                                        // in this map need to be set "true" -> indicating that the client hasn't
                                        // seen it since it has last changed.
                                        // when serializing, i check all those bools, and if the bool (for this client)
                                        // is true, i serialize the cube on this position in any case, even if it is a 'none'-cube,
                                        // which would be filtered otherwise. by that, the client gets a "none"-cube where he used to print something
                                        // because it is stored in a curses-pad. this gets overwritten by " " (a space symbol), and everything is fine.
                                        //! after profiling a little bit (with about 15 down-right movements with server on raspberry pi, a flat world and a sight-radius of 6, and 2 threads for serializing)
                                        //! it doesn't seem to be significant much difference between both methods.
                                        //! but since complicated-voodoo needs less bandwith, i will use it.
                                        //! and there still seems to be a bug (i saw it only once), that must be found





#define VALIDATE_DATABASE_BEFORE_OVERWRITING_CUBE true  //! if true: the database well be checked at some points (for example replaceCubeWithCharacterCube() )
                                                        //! which shouldn't be necessary if database is always syncron with in-memory-world




#endif // CONFIG_H_INCLUDED
