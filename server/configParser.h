#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H

#include <string>

#include "structs.h"

using namespace std;

class configParser{

    public:
        configParser();
        ~configParser();
        void parse( parameters* In_Parameters, vector<string> In_Lines );



    protected:

    private:


};

#endif // CONFIGPARSER_H
