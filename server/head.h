#ifndef HEAD_H_INCLUDED
#define HEAD_H_INCLUDED

#include <vector>


#include "cube.h"
#include "config.h"

using namespace std;


class head{

    private:


    public:
        head();
        ~head();

        vector <int> AccountsCurrentlySeeThisPosition;
        vector<cube*> CubeStack;                                        //it is possible to have more then one cube in the same place

        //!#ifdef USE_COMPLICATED_VOODOO
        map < int,bool > ClientsSawThisPositionInThisSession;   //int=ID of client, bool = the cubestack has changed since the account saw this position last time
        //!#endif // USE_COMPLICATED_VOODOO


};















#endif // HEAD_H_INCLUDED
