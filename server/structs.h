#ifndef STRUCTS_H_INCLUDED
#define STRUCTS_H_INCLUDED


#include "cube.h"
#include "config.h"

#include <vector>

using namespace std;



struct worldSize {
                        int Height = -1;
                        int Width = -1;
                        int MaxLevelOverZero = -1;
                                            };



typedef enum{d_left, d_right, d_up, d_down, d_downleft, d_downright, d_upleft, d_upright, d_non, d_upZ, d_downZ}direction;        //i need to add the "d_" because it seems, that in namespace std there is something equal defined




typedef enum{ Enum_Password, Enum_Accountname, Enum_Charname}PasswordOrAccountnameOrCharname;



typedef struct { string MessageType; string MessageContent; string EndSymbol;}struct_message;




typedef struct{
    string Password = "";
    bool MakeFlatWorld = false;
    bool Heartbleed = false;
    bool MakeNew = false;       //create new world-database at start of application
}parameters;






#endif // STRUCTS_H_INCLUDED
