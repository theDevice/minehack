#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <iostream>
#include <map>

#include "cube.h"
//#include "config.h"

using namespace std;



cube::cube(){

}

cube::cube( int In_Energy, bool In_IsMassive/*!, bool In_CanBlockSight,*/, standardCube In_KindOfCube/*!, int In_IDofAccountSittingAtThisCube*/ ){
    Energy = In_Energy;
    IsMassive = In_IsMassive;
    //CanBlockSight = In_CanBlockSight;
    KindOfCube = In_KindOfCube;
    //!IDofAccountSittingAtThisCube = In_IDofAccountSittingAtThisCube;
}

cube::cube(standardCube In_StandardCube){
    KindOfCube = In_StandardCube;
}


cube::~cube(){

}



standardCube cube::getTypeOfCube(){
    #if DEBUG_GERMWORLD
        cout << "cube::getTypeOfCube() ; In_CalledFrom: " << In_CalledFrom << endl;
    #endif // DEBUG_GERMWORLD()
    return KindOfCube;
}



void cube::setIsGermForMountainOrValley(bool In_IsGermForMountainOrValley){     //called from world::world()
    #if DEBUG_IS_GERM_FOR_MOUNTAIN_OR_VALLEY
        string Debug_CalledFrom = In_CalledFrom;
        //bool Debug_IsGermForMountainOrValley = IsGermForMountainOrValley;
        cout << "IsGermForMountainOrValley: " << IsGermForMountainOrValley << " ; In_IsGermForMountainOrValley: " << In_IsGermForMountainOrValley << endl;
        cout << "cube::setIsGermForMountainOrValley(): Position: PosX: " << PositionOfCube.PosX << " PosY: " << PositionOfCube.PosY << " PosZ: " << PositionOfCube.PosZ << endl;
    #endif // DEBUG_IS_GERM_FOR_MOUNTAIN_OR_VALLEY

    IsGermForMountainOrValley = In_IsGermForMountainOrValley;

}

bool cube::isGermForMountainOrValley(){
    return IsGermForMountainOrValley;
}

void cube::setTypeOfCube( standardCube In_NewTypeOfCube, int In_UserIDofCharacter ){

    if(In_NewTypeOfCube == solid){
        IsMassive =        true;
        //!CanBlockSight = true;
    }else if( In_NewTypeOfCube == trunk ){
        IsMassive =        true;
        //!CanBlockSight = true;
    }else if( In_NewTypeOfCube == none ){
        IsMassive =        false;
        //!CanBlockSight = false;
    }else if( In_NewTypeOfCube == grass ){
        IsMassive =        false;
        //!CanBlockSight = false;
    }else if( In_NewTypeOfCube == sand ){
        IsMassive =        true;
        //!CanBlockSight = false;
    }else if( In_NewTypeOfCube == water ){
        IsMassive =        true;
        //!CanBlockSight = true;
    }else if( In_NewTypeOfCube == branch ){
        IsMassive =        true;
        //!CanBlockSight = false;
    }else if( In_NewTypeOfCube == leafsAndTwigs ){
        IsMassive =        false;
        //!CanBlockSight = false;
    }else if( In_NewTypeOfCube == characterCube ){
        IsMassive =        true;
        //!CanBlockSight = true;
    }else if( In_NewTypeOfCube == gravel ){
        IsMassive =        false;
        //!CanBlockSight = false;
    }else{
        cout << "cube::setTypeOfCube() ; missing 'isFlat' for requestet cube-type! exit now!" << endl;
        system("sleep(5)");
        exit(0);
    }

    //!IDofAccountSittingAtThisCube = In_UserIDofCharacter;





    KindOfCube = In_NewTypeOfCube;
}

int cube::getEnergy(){
    return Energy;
}

bool cube::isMassive(){
    return IsMassive;
}



string cube::getStringOfCubeType(){
    string Output = "";

    if( KindOfCube == none ){
        Output = "none";
    }else if( KindOfCube == stone ){
        Output = "stone";
    }else if( KindOfCube == trunk ){
        Output = "trunk";
    }else if( KindOfCube == water ){
        Output = "water";
    }else if( KindOfCube == grass ){
        Output = "grass";
    }else if( KindOfCube == solid ){
        Output = "solid";
    }else if( KindOfCube == branch ){
        Output = "branch";
    }else if( KindOfCube == leafsAndTwigs ){
        Output = "leafsAndTwigs";
    }else if( KindOfCube == sand ){
        Output = "sand";
    }else if( KindOfCube == characterCube ){
        Output = "characterCube";

    }else if( KindOfCube == gravel ){
        Output = "gravel";
    }else{
        Output = "cube::getStringOfCubeType(): CubeType not defined!";
        cout << "cube::getStringOfCubeType(): CubeType not defined " << endl;
        system("sleep(5)");
        exit(0);
    }

    return Output;
}


/*!
void cube::setIDofAccountSittingAtThisCube(int In_AccountID){
    IDofAccountSittingAtThisCube = In_AccountID;
}
*/

/*!
int cube::getIDofAccountSittingAtThisCube(){
    return IDofAccountSittingAtThisCube;
}
*/



bool operator==( const cube& In_Cube1, const cube& In_Cube2 ){
    //bool Output = true;

    StructWithVariables Cube1Variables = In_Cube1.getVariables();
    StructWithVariables Cube2Variables = In_Cube2.getVariables();

    if( Cube1Variables.Energy != Cube2Variables.Energy){
        return false;
    }
    if( Cube1Variables.IDofAccountSittingAtThisCube != Cube2Variables.IDofAccountSittingAtThisCube ){
        return false;
    }
    if( Cube1Variables.IsMassive != Cube2Variables.IsMassive ){
        return false;
    }
    if( Cube1Variables.IsGermForMountainOrValley != Cube2Variables.IsGermForMountainOrValley ){
        return false;
    }

    if( Cube1Variables.KindOfCube != Cube2Variables.KindOfCube ){
        return false;
    }



    return true;

}



StructWithVariables cube::getVariables () const{
    StructWithVariables Output;

    Output.Energy = Energy;
    //!Output.IDofAccountSittingAtThisCube = IDofAccountSittingAtThisCube;
    Output.IsMassive = IsMassive;
    Output.IsGermForMountainOrValley = IsGermForMountainOrValley;
    Output.KindOfCube = KindOfCube;


    return Output;

}

/*!
bool cube::canBlockSight(){
    return CanBlockSight;
}
*/
