#ifndef ATTACK_H_INCLUDED
#define ATTACK_H_INCLUDED


using namespace std;

#include <string>

#include "weapon.h"
#include "action.h"

class attack: public action{

    public:
        attack();
        attack(string In_Name, float In_Range);
        ~attack();

        int getMultiplicator();

       void setUsedWeaponForThatAttack( weapon * In_PointerToWeapon );
       weapon* getPointerToWeapon();


    protected:
        float Range = 0;       //has to be float. because, if a weapon got the smallest range (for example, bare fists), then you want to be able to hit all cubes surrounding you. so the distance between a cube to you can be more then 1 but not more then 2. so in that case, i would choose a range-value a bit bigger then square-root of 2.

        int Multiplicator = 1;

        weapon* UsedWeapon = NULL;



};






#endif // ATTACK_H_INCLUDED
