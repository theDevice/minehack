#ifndef USERACCOUNT_H
#define USERACCOUNT_H

#include "character.h"
#include "config.h"
#include "structs.h"

#include <string>
#include <vector>


using namespace std;

class UserAccount{

    public:
        UserAccount(int In_DefaultSightRadiusForCharacter, int In_AccountID);
        UserAccount( const UserAccount& In_UserAccount );        //Copy-constructor
        ~UserAccount();
        void setUserNameAndPasswordAndCharacterName(string Password, string AccountName, string CharacterName);
        string getUsername();
        string getPassword();
        bool IsAlreadyAttachedToAClient();
        void setIDofClientAttachedWithThisAccount( int64_t In_IDofAttachedClient);
        int64_t getIDofClientAttachedWithThisAccount();
        void unsetIDofClientAttacedWithThisAccount();
        string getTimestampInMiliseconds();
        void characterIsDead();
        character* getPointerToCharacter();

    private:
        int AccountID = -1;
        string UserName;
        string Password;
        bool UserNameIsLocked = false;
        character* CurrentCharacter;
        int64_t IDofClientAttachedWithThisAccount = -1; //is -1 when not attached.

};







#endif // USERACCOUNT_H
