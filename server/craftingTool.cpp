#include <string>

#include "craftingTool.h"

using namespace std;

craftingTool::craftingTool(string In_Name, standardCube In_CanBeUsedWithCubeType){
    Name = In_Name;
    CanBeUsedWithCubeType = In_CanBeUsedWithCubeType;
}

craftingTool::~craftingTool(){
    //dtor
}

/*!
craftingTool::craftingTool(const craftingTool& other){
    //copy ctor
}
*/

craftingTool& craftingTool::operator=(const craftingTool& rhs){
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}

/*
void craftingTool::setCraftingTool( craftingTool* In_PointerToTool ){


}
*/
