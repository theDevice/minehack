#ifndef CLIENT_H_INCLUDED
#define CLIENT_H_INCLUDED

#include <string>
#include <poll.h>           //needed for recv(). to check if the client is still connected

#include "UserAccount.h"
#include "head.h"
#include "config.h"

#include <../include/cereal/archives/json.hpp>
#include <../include/cereal/types/vector.hpp>      //stuff for json. we might not need all of them.

using namespace std;


class client{


    public:
        client(int64_t In_NewSocketFileDescriptor, int64_t In_ID);
        ~client();

        string waitForMessageFromClient( UserAccount *In_PointerUserAccount );
        void sendMessageToClient(string In_Msg);
        void clearLatestReceivedMessage();
        int64_t getIDOfClient();
        void aNewChatMessageArrived(string In_NewMsg);
        bool getIsClientStillConnected();
        void askForHeartbleed();
        void sendOrderToTurnHeartIntoBlue();
        bool tryToMove(direction In_Direction);     //just returns a bool if move was successfull
        void updatePosition(position In_NewPosition);
        void sendOrderToUnlockMutexForWritingSocket();
        void sendInformationOfUnsuccessfullLogin( int64_t In_IndexOfTheRequestedAccountInVector );
        void confirmAccountCreationProcess();
        void tellClientUserAccountNameIsAlreadyInUse();
        void markClientAsDissconnected();
        int64_t getAccountID();
        void setAccountID(int64_t In_NewAccountID);
        void unsetAccountID();
        void ClientAnsweredHeartbleed();
        string getTimestampInMiliseconds();
        bool return_ClientIsConnected();
        void setHeartbleedIsAlreadyStarted();
        bool getHeartbleedIsAlreadyStarted();
        //!#ifdef USE_COMPLICATED_VOODOO
        void addIDto_ListOfPositionsThisAccountSawInThisSession( head* In_Head );
        //!#endif // USE_COMPLICATED_VOODOO

    private:
        //!#ifdef USE_COMPLICATED_VOODOO
        vector <head*> ListOfPositionsThisAccountSawInThisSession;  //this will be filled up with pointers to heads (which is a struct on every position in the world). after a account got logged of or disconnected, it is important to iterate over this list and delete the information in the heads that this account saw this position in this session
        //!#endif // USE_COMPLICATED_VOODOO
        int64_t AccountID = -1;
        int64_t ClientID;
        int64_t SocketFileDescriptor;
        bool ClientIsConnected;             //if the client disconnects, this will be set "false" in waitForMsg()

        struct pollfd pfd;

        bool WaitingForHeartbleed = false;
        bool HeartbleedIsAlreadyStarted = false;


};







#endif // CLIENT_H_INCLUDED
