/*

linker options:
-pthread

//-fsanitize=thread -fPIE -pie

compiler options:
//-std=gnu++11            //that doesn't seem to be needed any more!

-I include              //needed for cereal stuff

//-fsanitize=thread      //ThreadSanitizer for finding bugs and data-race-issues



//to run in valgrind, use: valgrind --log-file="valgrind.log" ./server

//to create analysis-file, use: gprof server gmon.out > analysis

*/


#include "config.h"
#include "controller.h"
#include "structs.h"
#include "configParser.h"

#include <sys/timeb.h>          //needed for timestap in log-file
#include <signal.h>
#include <fstream>
#include <dirent.h>
#include <sstream>

using namespace std;

int main( int argc, char* argv[] ){

    system("clear");
    parameters Parameters;

    //cout << "argc: " << argc << endl;
    if( argc == 0 ){
        Parameters.Password =   "password--------";
        cout << "Password: " << Parameters.Password << endl;
    }else{
        for( size_t index = 1; index < argc; index++ ){
            //cout << "argc[" << index << "]: " << argv[index] << endl;
            string Temp_Argument = argv[index];



            if( Temp_Argument == "-h" || Temp_Argument == "--help" ){
                cout << "printing help..." << endl;
                cout << "hello, this is the server for minehack. minehack is an mmorpg in traditional rogue-like flavour." << endl;

                cout << "options:" << endl << endl;
                cout << "\e[1m" << "-h, --help" << "\e[0m" << endl;
                cout << "\tdisplay this information!" << endl;



                cout << endl << "\e[1m" << "-p, --password" << "\e[0m" << endl;
                cout << "\tset password. must me less then 16 characters!" << endl;

                cout << endl << "\e[1m" << "-mn, --makeNew" << "\e[0m" << endl;
                cout << "\toverwrite current database." << endl;

                cout << endl << "\e[1m" << "-f, --flat" << "\e[0m" << endl;
                cout << "\tcreate a flat world. that is quicker and enough for most test-cases." << endl;

                cout << endl << "\e[1m" << "-h, --heartbleed" << "\e[0m" << endl;
                cout << "\tsending a heartbeed to client." << endl;

                cout << endl << "\e[1m" << "-dc, --defaultconfig" << "\e[0m" << endl;
                cout << "\toverwrites any existing config-file with default values" << endl;

                cout << endl << "\e[1m" << "-c, --config" << "\e[0m" << endl;
                cout << "\tuse config-file. must be in same folder as this binary. other options can still be used!" << endl;
                cout << "\tvalid options an a config file are: " << endl;
                cout << "\t\t password <somePassword>" <<endl;
                cout << "\t\t flatWorld true/false" <<endl;
                cout << "\t\t heartbleed true/false" <<endl;



                cout << endl << "exit now!" << endl;
                exit(0);


            //!this must stay at the beginning, because it must be able to be overwritten by other options!
            }else if( Temp_Argument == "-c" || Temp_Argument == "--config" ){
                //!checking if there is a "config"-file

                vector<string> FilesInThisFolder;
                DIR *dp;
                struct dirent *dirp;
                string ThisFolder = ".";
                if((dp  = opendir( ThisFolder.c_str() )) == NULL) {
                    cout << "Error(" << errno << ") opening " << "." << endl;
                    return errno;
                }

                while ((dirp = readdir(dp)) != NULL) {
                    (FilesInThisFolder).push_back(string(dirp->d_name));
                }
                closedir(dp);

                bool ConfigFound = false;
                for( size_t index = 0; index < FilesInThisFolder.size(); index++ ){
                    if( FilesInThisFolder[index] == "config" ){
                        ConfigFound = true;
                    }
                }

                vector<string> Lines;
                if( ConfigFound == false ){
                    cout << "no config-file found. exit now!" << endl;
                    exit(1);
                }else{
                    string Line = "";
                    ifstream ConfigFile("config");
                    while( getline(ConfigFile, Line) ){
                        Lines.push_back(Line);
                    }
                }
                configParser Parser;
                Parser.parse(&Parameters, Lines);

            }else if( Temp_Argument == "-dc" || Temp_Argument == "--defaultconfig" ){
                cout << "do you really want to overwrite any existing cofnig file? (y/n)" << endl;
                char Answer = getchar();
                if( Answer == 'y' || Answer == 'Y' ){
                    system("rm -r config"); //well, it might be a folder...
                    system("touch config");
                    system("echo 'password password' >> config");
                    system("echo 'makeNew true/false' >> config");
                    system("echo 'flatWorld false' >> config");
                    system("echo 'heartbleed false' >> config");
                    system("echo '#some comment' >> config");
                }else if( Answer == 'n' || Answer == 'N' ){
                    cout << "you typed '" << to_string(Answer) << "'. exit now!" <<endl;
                    exit(1);
                }else{
                    cout << "invalid answer. exit now!" << endl;
                    exit(1);
                }

            }else if( Temp_Argument == "-f" || Temp_Argument == "--flat" ){
                cout << "making a flat world" << endl;
                Parameters.MakeFlatWorld = true;
            }else if( Temp_Argument == "-h" || Temp_Argument == "--heartbleed" ){
                cout << "turning heartbleed on" << endl;
                Parameters.Heartbleed = true;
            }else if( Temp_Argument == "-p" || Temp_Argument == "--password" ){
                if( index+1 == argc ){
                    cout << "wrong usage. after '-p' or '--password' you must specify a password" << endl;
                }else{
                    string NextArgument = argv[index+1];
                    char FirstLetter = NextArgument.at(0);
                    if( FirstLetter == '-' ){
                        cout << "error. after '-p' or '--password' you must specify a password. exit now!" << endl;
                        exit(1);
                    }
                    Parameters.Password = argv[index+1];
                    if( Parameters.Password.size() > 16 ){
                        cout << "choosen password is to long. cant be more then 16 characters. exit now! " << endl;
                        exit(1);
                    }

                    for ( int I = Parameters.Password.size() ; Parameters.Password.size() < 16 ; I++ ){
                        Parameters.Password += "-";
                    }
                    cout << "set password to: " << Parameters.Password << endl;
                    index++;
                }


            }else if( Temp_Argument == "-mn" || Temp_Argument == "--makeNew" ){
                cout << "are you sure you want to delete the current database? (y/n)" << endl;
                char Answer = getchar();
                if( Answer == 121 || Answer == 89 ){        // ascii y or Y
                    Parameters.MakeNew = true;
                }else if( Answer == 110 || Answer == 78 ){  //asscii n or N
                    cout << "you decided not to delete the database. exit now" << endl;
                    exit(0);
                }else{
                    cout << "wrong letter. exit now" << endl;
                    exit(1);
                }




            }else{
                cout << "unknown option '" << argv[index] << "'. exit now!" << endl;
                exit(1);
            }






        }
    }
    cout << "end parsing argumens" << endl;


    if( Parameters.MakeNew ){
        cout << "deleting database now" << endl;
        system("rm -r database");
        cout << "done" << endl;
    }





    //!debugging
    //!Parameters.MakeNew = true;


  //  signal(SIGPIPE, handler);
 //   signal(SIGPIPE,SIG_IGN);                            // https://www.linuxquestions.org/questions/programming-9/how-to-handle-a-broken-pipe-exception-sigpipe-in-fifo-pipe-866132/









    system("> sendMessageToClient.log");       //clearing log-file
    system("> interpretReceivedMessage.log");
    system("> waitForMessageFromClient.log");
    system("> debug.log");





    //system("sleep 1");

    srand(time(0));                     //init random



    controller Controller( Parameters );

    Controller.initYourself();      //checks out ports and sockets and stuff

    Controller.manageYourself();








  //!  world CurrentWorld;                         //creates a new world
                                                //!what's missing here is the option to load an existing world!

  //!  saveWorld(CurrentWorld);

  //  CurrentWorld.displayWorld(1);                           //test display the first-quadrant of the world


  //  printf("server = on\n");






    Controller.closeSocketFileDescriptor();

    return 0; // we never get here




}








