#ifndef WORLD_H_INCLUDED
#define WORLD_H_INCLUDED

#include <vector>

#include "cube.h"
#include "config.h"
#include "structs.h"
#include "position.h"
#include "database.h"
//#include "head.h"

//!will be needed later for saving stuff
//!#include <cereal/archives/json.hpp>
//!#include <cereal/types/vector.hpp>      //stuff for json. we might not need all of them.





class world{


    private:
        //int64_t NumberOfDifferentObjects;
        //int64_t SizeOfGermWorld = -1;

        worldSize WorldSize;

        //int MaxLevelOverZero = -1;




        //!vector< vector< vector< tuple<   vector< int >, vector< cube* >,    vector <    pair<   int,    bool> > > > > > GermWorld;
        //this 4-D-vector will be the 1st world, which will be consist of Germs for calculate the finished world. late this world can be used to extend the "normal" world
    //! ^ Y
    //!         ^ X
    //!                 ^ Z
    //!                                                 ^cube-stack
    //!                                                         ^ pointer to cubes
    //!                                         ^account-IDs which can see this position
    //!                                                                     ^ this vector organizes the IDs ob Accounts, which once saw this position in the current session
    //!                                                                                         ^these are the IDs,
    //!                                                                                                 ^ this is 'false' if the cube-stack on this position hasn't changed since last seen by account/character. per default it will be set to 'false' and only set true at the moment of change
    //! ^[Y]     ^[X]     ^[Z]             ^first       ^second

        //vector< vector <vector <vector < int64_t > > > > WhichCharacterSeesWhichPositions;
        //i will save here later the information which client sees which positions. i need it in 4-D because there can be a stack of connectedClients see the same positions
        //! well, that is a bad idea... i used to store that information directly in the cubes.
        //! but that was a bad idea, because cubes can move... so i need to store that information directly in the world,
        //! because that information belongs to the world (means: the position) not the cube


        // fuck all that shit. i put it all in a struct and call it 'head'
        vector< vector< vector< head > > > GermWorld;






        vector< vector< vector < head > > > WorldVector;    //this is now the actual world
    //      Y       X       Z
    //




        vector < vector <position> > Vec_GermPositions;
        //this vector will consist of the germs for Mountains and valleys. its just handy to have them here aswell.
        //and i try to make it possible to have less steep.
        //!maybe its better to make this not a member variable but a argument for makeLastGermedMountainLessSteep()



        database* Database;



        //!funktions
        cube convertIntToObject(int64_t In_Int);
      //  void displayWorld(/*int WhichQuadrant*/);
        void writeWorldInFile(string In_WichWorld);
        void makeMountainOrValley();                                            //makes Mountains and / or Valleys in GermWorld
        const vector <cube*> getCubesOnPositionFromGermWorld(position In_Position);
        const head* getPointerToHeadOnPositionFromNormalWorld( position In_Position );
        bool makeTree( int PosY, int PosX);        //in normal world
                                                                                //PosZ will be calculated automatically... first "solid"-cube from up to down
                                                                                //returns false, if the tree couldn't be placed there
        void copyGermWorldInNormalWorld();
        void fillWorldWithWaterToLevel();
        void setRandomGermsForMountainsAndValleysIntoGermWorld();
        void makeClustersOfGerms(int PosY, int PosX, int PosZ);
        void makeLastGermedMountainLessSteep();
        void placeClusterOfCubeOnSurface(standardCube In_TypeOfCubes);
        //void addIDofAAccountWhoCanSeeCubeOnPosition(int In_AccountIDtoAdd, position In_CubeOnPosition);
        //!void removeIDofAAccountWhoHasBeenSeenCubesOnPosition( int In_AccountIDtoRemove, position In_CubeOnPosition );


        //!void addCharacterToCubeStack(position In_Position, int In_UserID );
        string getTimestampInMiliseconds();

        void debug(string In_Put);






        //!#ifdef USE_COMPLICATED_VOODOO
        void markHeadAsChanged( position In_Position );
        //!#endif // USE_COMPLICATED_VOODOO


        vector< vector <vector <head> > >* getPointerToWorldVector();


        /*
        //!lets see if this is also working as a private funktion... NO!
        template <class Archive>                                                         //!serialize it!
        void serialize(Archive & archive){
            archive( CEREAL_NVP(NumberOfDifferentObjects), CEREAL_NVP(World) ); // serialize things by passing them to the archive
        }
        */


        //void setWorldSize( worldSize In_WorldSize );
        void generateEmptyVectorFramesWithoutHeads( vector<vector<vector<head>>>* In_EmptyVector, const worldSize In_Wordsize );




    public:
        world();              //dummy constructor
        world( database* In_Database);
        ~world();

        const vector <cube*>* getPointerToCubeStackOnPositionFromNormalWorld( position In_Position );
        void createNew( bool In_MakeFlatWorld );
        void writeWorldInDatabase();
        void loadWorldFromDatabase();
        worldSize getWorldSize();
        //!int getMaxLevelOverZero();
        vector <int> getIDsofAccountsWhoCanSeePosition(position In_Position);
        //!void removeCharacterCubeFromPosition(int In_AccountID, position In_Position);
        bool isSomeCharacterSittingOnPosition( position In_Position );
        //!bool setSolidCubeOnPosition( position In_Position );//returns false if not possible


        bool addCubeToCubestack( position In_Position, cube* In_CubeToAdd );
        void removeCubeFromCubestack( position In_Position, cube* In_CubeToRemove );

        bool positionIsValid( position In_Position );

        //! PLEASE!!! only use this two function with valid positions. you can use world::positionIsValid() to check this.
        //! if you don't, the server will crash!
        void addIDofAAccountWhoCanSeePosition(int In_AccountIDtoAdd, position In_ValidPosition);
        void removeIDofAAccountWhoCanSeePosition(int In_AccountIDtoRemove, position In_ValidPosition);
        void addIDto_ClientsSawThisPositionInThisSession( int In_ClientID, position In_Position );
        head* getHeadPointerFromPosition( position In_Position );

        map< int, bool >* getPointerTo_ClientsSawThisPositionInThisSession( position In_Position );
        const vector <cube*> getCubesOnPositionFromNormalWorld(position In_Position);

        void extendTheWorldWithFlatLand( int In_NumberOfCubesToAdd, direction In_DirectionWhereTheWorldShouldBeExtanden );   //the direction must be 'right' or 'down'

        position getResultingPositionAfterTryingToMoveToNewPosition( position In_NewPositionOfCharacter );

        bool positionIsMassive( position In_ToCheckPosition );
};

















#endif // WORLD_H_INCLUDED
