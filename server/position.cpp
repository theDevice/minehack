#include "position.h"


using namespace std;

position::position(){

}




position::~position(){

}


position operator+(position In_LeftHandSide, position In_RightHandSide){



    In_LeftHandSide.PosY += In_RightHandSide.PosY;
    In_LeftHandSide.PosX += In_RightHandSide.PosX;
    In_LeftHandSide.PosZ += In_RightHandSide.PosZ;


    return In_LeftHandSide;

}


position operator-(position In_LeftHandSide, position In_RightHandSide){



    In_LeftHandSide.PosY -= In_RightHandSide.PosY;
    In_LeftHandSide.PosX -= In_RightHandSide.PosX;
    In_LeftHandSide.PosZ -= In_RightHandSide.PosZ;


    return In_LeftHandSide;

}

position operator*( position In_LeftHandSide, int In_Skalar ){

    In_LeftHandSide.PosY = In_LeftHandSide.PosY * In_Skalar;
    In_LeftHandSide.PosX = In_LeftHandSide.PosX * In_Skalar;
    In_LeftHandSide.PosZ = In_LeftHandSide.PosZ * In_Skalar;

    return In_LeftHandSide;
}


bool operator< ( const position& In_LeftHandSide, position& In_RightHandSide ){


    if( In_LeftHandSide.PosY < In_RightHandSide.PosY ){
        return true;
    }else if( In_LeftHandSide.PosY > In_RightHandSide.PosY ){
        return false;
    }else if( In_LeftHandSide.PosX < In_RightHandSide.PosX ){
        return true;
    }else if( In_LeftHandSide.PosX > In_RightHandSide.PosX ){
        return false;
    }else if( In_LeftHandSide.PosZ < In_RightHandSide.PosZ ){
        return true;
    }else if( In_LeftHandSide.PosZ > In_RightHandSide.PosZ ){
        return false;
    }else{
        //!if the two sides are equal in all three dimensions, it just doesn't matter which one will be sorted first
        //I try i with returning false now, but it might be faster with true
        //!so i should benchmark that later!!!
        // changing it to "true" makes segmentation-faults
        return false;
    }

}


bool operator== ( const position& In_LeftHandSide, position& In_RightHandSide ){
    bool Output = false;

    if( In_LeftHandSide.PosY == In_RightHandSide.PosY && In_LeftHandSide.PosX == In_RightHandSide.PosX && In_LeftHandSide.PosZ == In_RightHandSide.PosZ ){
        Output = true;
    }


    return Output;


}

string operator+ ( const string& In_LeftHandSide, position& In_RightHandSide ){

    string Output = "";

    Output = In_LeftHandSide + to_string(In_RightHandSide.PosY) + ";" + to_string(In_RightHandSide.PosX) + ";" + to_string(In_RightHandSide.PosZ);

    return Output;

}


ostream& operator<< ( ostream &out, position &In_Position ){
    //! http://www.learncpp.com/cpp-tutorial/93-overloading-the-io-operators/

    // Since operator<< is a friend of the Point class, we can access
    // Point's members directly.
    out << In_Position.PosY << ";" << In_Position.PosX << ";" << In_Position.PosZ;
    return out;

}


/*
void operator= ( const position& In_LeftHandSide, const position& In_RightHandSide ){



}

*/



bool operator!= (const position& In_LeftHandSide, const position& In_RightHandSide){
    bool Output = false;
    if( (In_LeftHandSide.PosY != In_RightHandSide.PosY) || (In_LeftHandSide.PosX != In_RightHandSide.PosX) || (In_LeftHandSide.PosZ != In_RightHandSide.PosZ) ){
        Output = true;
    }

    return Output;
}
