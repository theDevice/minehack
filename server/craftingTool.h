#ifndef CRAFTINGTOOL_H
#define CRAFTINGTOOL_H

#include "item.h"
#include "cube.h"
#include "config.h"

#include <string>


using namespace std;

class craftingTool : public item{

    public:
        craftingTool(string In_Name, standardCube In_CanBeUsedWithCubeType);
        ~craftingTool();
        //craftingTool(const craftingTool& other);
        craftingTool& operator=(const craftingTool& other);
        //void setCraftingTool( craftingTool* In_PointerToTool );

    protected:
        standardCube CanBeUsedWithCubeType;

    private:


};

#endif // CRAFTINGTOOL_H
