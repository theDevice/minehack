#ifndef SIGHTLIBRARY_INCLUDED
#define SIGHTLIBRARY_INCLUDED

#include <map>
#include <vector>

#include "position.h"

class sightLibrary{


    private:

        float RatioZ_RadiusToSightRadius;

//! this is a solution only working with a simple-sightField withoud shadow-effects
        map < int, vector < vector < position > > > SightFieldMap;
//!                 ^   again, this vector represents the 'shells' of an sightField. since i need to sort
//!                     the positions somehow (to process them later in threads) i sort them by biggest dimension
//!                     for example: The position -3,5,6 will be in shell 6. the zeros shell doesn't contain anything
//!                     and though represents the character itself.
//!                            ^  this vector then is a list with positions in the same shell.
//!     ^ and i organize the sightFields in map, where
//!            ^ the key is representing the sightRadius



//! this will contain the information about the position which must be added to the sightField after a move
        map < int, map < int, map < int, map < int, vector< position > > > > > PositionsToAddAfterMovement;
//! it is organized in a three-dimensional map, where
//!                     ^ this indexes the Y-dimension
//!                                 ^ this the X-dimension
//!                                             ^ and this of course the Z-dimension
//! means: you find in this Variable a vector
//!                                         (         ^ here        )
//! with all positions
//!                                                 (        ^ here      )
//! needed to be add to your sightField. all you need to do is pick the right vector (in terms of array)
//! by putting in the vector (in terms of mathematical vector) of movement
//!                 ^ here,   ^ here,   ^ and here
//!            ^ and the whole thing is sorted by sight-radius



//! it is important to understand, that both, the vectors PositionsToAddAfterMovement and PositionsToRemovedAfterMovement
//! contain the relative positions that need to be added to the absolute position of the character AFTER movement, means with it's
//! new position

    //! and the same thing with positions to be removed after a movement:
        map <int, map < int, map < int, map < int, vector< position > > > > > PositionsToRemovedAfterMovement;
        vector < position > MovementDirections;


    public:

        sightLibrary( float In_RatioZ_RadiusToSightRadius);
        ~sightLibrary();

        //map < int, vector < vector < pair < position, vector< position > > > > > SightFieldMap;


        void calculateSightFieldMapWithRadius(int In_Radius);
        vector < vector < position > >* returnPointerToSightFieldBlueprint(int In_Radius);
        string getTimestampInMiliseconds();
        void calculatePositionsToAddAfterMovement( int In_Radius );
        void calculatePositionsToRemoveAfterMovement( int In_Radius );
        vector < position >* getPointerToPositionsToBeAddedToSightField( int In_SightRadius, position In_MovementVector);

/*
        vector < vector < pair< position, vector< position > > > > calculateHowEachPositionEffectsTheShadows( vector < vector < pair < position, vector < position > > > >  In_BlankVectorOfPositions, int In_Radius );
             //!    ^that is vector of all positions.
             //!                ^that positions. it just got calculated by calculateSightFieldMapWithRadius()
             //!                            ^that vector contains all positions can not be seen any more when
             //!                ^that positions is blocking sight.
             //!                                   ^so, once again. these are the positions i need to remove later, when
             //!                        ^this position is blocking sight. and i say it again.
//! uh. and ^that vector organizes the positions in "shells". the shell-number is same as the largest dimension of a position
//! for example: The position -3,5,6 will be in shell 6, which is equal to calculateHowEachPositionEffectsTheShadows[6]
*/




};
















#endif // SIGHTLIBRARY_INCLUDED
