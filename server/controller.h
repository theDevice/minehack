#ifndef SCONTROLLER_H_INCLUDED
#define SCONTROLLER_H_INCLUDED

#include <sys/types.h>      //
#include <sys/socket.h>     //        = needed for the socket and server stuff
#include <netinet/in.h>     //
#include <vector>
#include <thread>
#include <mutex>

#include "client.h"
#include "world.h"
#include "sightLibrary.h"



using namespace std;


class controller{

    public:
        controller( parameters In_Parameters );
        ~controller();

        void initYourself();
        void waitForMoreClients();       //returns ID of connected client
        void closeSocketFileDescriptor();
        void manageYourself();
        void startThreadForWaitingForMessages(int64_t In_ID_OfToHandleClient);
        void startThreadForCatchingClients();
        void interpretReceivedMessage(string In_ReceivedMessage, int64_t In_IDofClient);
        void startThreadForHeartbleed( int64_t In_ID_OfToHandleClient);
        void sendHeartbleed(/*int64_t In_Heartbleed*/);
        direction convertStringToDirection(string In_Direction);
        bool areEqual(char In_Char1[], char In_Char2[]);
        int64_t tryToLoginWith(string In_Password, string In_AccountName);       //returns -1 if the account is not in the list. returns the index of the vector, wherer the account is saved, when the requested login-account is avaible
        void confirmLoginProcess( int64_t In_IDofClient, int In_AccountID );
        string extract(PasswordOrAccountnameOrCharname In_PasswordOrAccountnameOrCharname , string In_PasswordAndUsernameAndCharname);
        bool nameAlreadyInUse( string In_Username  );
        string getTimestampInHoursAndMinutes();

        void receivedChatMessage( string In_ChatMessage, int64_t In_IDofClientSendingThisMessage );
        void receivedHeartbleedFromClient(int64_t In_ClientID);
      //  void incommingSightArray();
        bool receivedRequestToMove(string In_Direction, int64_t In_ClientID);
        void receivedCreateNewAccount( string In_PasswordAndAccountname, int64_t In_ClientID );
        void receivedLoginRequest( string In_PasswordAndUsername, int64_t In_ClientID );
        void receivedClientWantsToCloseConnection( int64_t In_ClientID );
        void receivedPrimaryAttack( int In_IDofToAttackAccount, int In_ClientID);
        void receivedSecondaryAttack( int In_IDofToAttackAccount, int In_ClientID);
        void receivedAdminCommand( string In_Command, int In_ClientID);

        struct_message extractMessage(string In_OriginalMessage);
        vector<string> splitString(string In_Seperator, string In_OriginalMessage);
        //void calculateSightForClient(int In_ClientID, bool In_AddInformationThatItCanBeSeenToCubes);        //writes the current SightField in UserAccount/Character of the Client with In_ClientID
        //!vector < vector < cube >> goThroughThisNodeAndReturnWhatTheCharacterCanSee( sightNode* In_SightNode, position In_PositionOfCharacter/*, position In_PositionOfNodePointingAtTheToHandleNode*/);
        //string getSerializedSightField( int In_AccountID );
        void serializeSightField_subFunction(
                                                //!#ifdef USE_COMPLICATED_VOODOO
                                                int In_ClientID,
                                                //!#endif // USE_COMPLICATED_VOODOO
                                                character* In_CharacterPointer, string* In_Output, vector< pair< position, cube*>>* In_SightField, size_t In_DoItFrom, size_t In_Until,  bool In_Filter_NoneCubes );
        string serializeSightField( int ClientID, int In_AccountID, vector< pair< position, cube*>>* In_DifferentialSightField, bool In_Filter_NoneCubes );
        void calculateDiffSightFieldAndSendToClientAndUpdatePosition( int In_ClientID, position VectorFromOldPositionToNewPosition, position In_OldAbsolutePositionOfCharacter );
        void buildSightFieldFromScratchAndUpdatePosition( int In_ClientID );
        position calculateNewPosition(position In_OldPosition, direction In_Direction);     //if the requested Movement is allowed, this funktion returns the new position (can also be a in an other z-level); if the movement is not allowed (for example because there is a wall) this funktion returns the old position
        string getTimestampInMiliseconds();
        void extendTheWorld( direction In_ExtendInDirection );
        void broadcast( string In_ToBroadcastMessage );
        void debug(string In_Put);


    private:
        mutex Serialize_Mutex;
        string ServerPassword;

        vector<UserAccount> AllUserAccounts;

        //!client** AllClients;
        vector<client*> AllClients;

        uint64_t IdentifierForClients;


        int64_t SocketFileDescriptor;                   //file descriptors, i.e. array subscripts into the file descriptor table . These two variables store the values returned by the socket system call and the accept system call.
        int64_t PortNumber;
        bool ServerIsRunning;
        int64_t newSocketFileDescriptor;
        struct sockaddr_in serv_addr, cli_addr;     //A sockaddr_in is a structure containing an internet address. This structure is defined in netinet/in.h.
                                                    //      struct sockaddr_in{
                                                    //       short   sin_family; // must be AF_INET
                                                    //       u_short sin_port;
                                                    //       struct  in_addr sin_addr;
                                                    //          char    sin_zero[8]; // Not used, must be zero
                                                    //      };
                                                    //An in_addr structure, defined in the same header file, contains only one field, a unsigned long called s_addr.
                                                    //The variable serv_addr will contain the address of the server, and cli_addr will contain the address of the client which connects to the server.

        socklen_t SizeOfAdressOfClient;
        mutex MutexForOutputControll;
        mutex MutexForInterpretMessage;






        float Ratio_Z_RadiusToSightRadius = RATIO_Z_RADIUS_TO_SIGHT_RADIUS;
        sightLibrary SightLibrary = sightLibrary(RATIO_Z_RADIUS_TO_SIGHT_RADIUS);


        parameters Parameters;
        database Database;

        world World;

};















#endif // SCONTROLLER_H_INCLUDED
