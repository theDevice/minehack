#include "UserAccount.h"
#include "character.h"

#include <string>
#include <iostream>
#include <cstring>   //strcpy und bzero
#include <sys/time.h>           //needed for timestamp

#include "config.h"


using namespace std;

UserAccount::UserAccount( int In_DefaultSightRadiusForCharacter, int In_AccountID ){
    UserName = "";
    Password = "";

    CurrentCharacter = new character( In_DefaultSightRadiusForCharacter, In_AccountID );

}


//!Copy-Constructor:
UserAccount::UserAccount( const UserAccount& In_UserAccount ){

    cout << getTimestampInMiliseconds() << " UserAccount::UserAccount(): copy-constructor has been called" << endl;

    UserName = In_UserAccount.UserName;
    Password = In_UserAccount.Password;
    UserNameIsLocked = In_UserAccount.UserNameIsLocked;
    CurrentCharacter = In_UserAccount.CurrentCharacter;
    IDofClientAttachedWithThisAccount = In_UserAccount.IDofClientAttachedWithThisAccount;

}


UserAccount::~UserAccount(){

    cout << getTimestampInMiliseconds() << " UserAccount::~UserAccount(): destroyed. IDofClientAttachedWithThisAccount: " << IDofClientAttachedWithThisAccount << endl;

}


void UserAccount::setUserNameAndPasswordAndCharacterName(string In_Password, string In_AccountName, string In_CharacterName){



    if( UserNameIsLocked == false ){
        UserName = In_AccountName;
        Password = In_Password;
        CurrentCharacter->setCharactername( In_CharacterName );
        #if DEBUG_OUTPUT
            cout << getTimestampInMiliseconds() << " UserAccount::setUserNameAndPassword(): username and password got changed!: " << UserName << "/" << Password << endl;
        #endif // DEBUG_OUTPUT
    }else{
        cout << getTimestampInMiliseconds() << " username is locked. it cant be changed!" << endl;
    }
    UserNameIsLocked = true;
}

string UserAccount::getUsername(){
    return UserName;
}

string UserAccount::getPassword(){
    return Password;
}

bool UserAccount::IsAlreadyAttachedToAClient(){
    bool Output = false;

    if ( IDofClientAttachedWithThisAccount == -1 ){
        Output = false;
    }else if( IDofClientAttachedWithThisAccount >= 0 ){
        Output = true;
    }

    return Output;
}


void UserAccount::setIDofClientAttachedWithThisAccount(int64_t In_IDofConnectedClient){
    IDofClientAttachedWithThisAccount = In_IDofConnectedClient;
}

int64_t UserAccount::getIDofClientAttachedWithThisAccount(){
    return IDofClientAttachedWithThisAccount;
}

void UserAccount::unsetIDofClientAttacedWithThisAccount(){
    IDofClientAttachedWithThisAccount = -1;
}


string UserAccount::getTimestampInMiliseconds(){

    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;

    string Output = to_string(ms);
    Output.erase( 0, 4 );

    return Output;
}

void UserAccount::characterIsDead(){

    delete CurrentCharacter;

}


character* UserAccount::getPointerToCharacter(){

    return CurrentCharacter;

}


