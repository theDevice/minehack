#include <sys/time.h>           //needed for timestamp
#include <map>

#include <iostream>

#include "character.h"





using namespace std;



bool IsPositionInSightRadius( position In_Position );

character::character(int In_DefaultSightRadiusForCharacter, int In_IDofAccountThisCharacterBelongsTo ){
    IDofAccountThisCharacterBelonsTo = In_IDofAccountThisCharacterBelongsTo;

    Position.PosX = 0;
    Position.PosY = 0;
    Position.PosZ = AVERAGE_LEVEL_OF_GROUND+1;
    Charactername = "dummyName";

    SightRadius = In_DefaultSightRadiusForCharacter;

    position Temp_ZeroRelativePosition;
    Temp_ZeroRelativePosition.PosY = 0;
    Temp_ZeroRelativePosition.PosX = 0;
    Temp_ZeroRelativePosition.PosZ = 0;

//!    BareFists.setIsEquiped(true);


 //   UsedWeaponForPrimaryAttack = &BareFists;

    PrimaryAttack.setUsedWeaponForThatAttack(&BareFists);


    weapon* Temp_Slingshot = new weapon("Slingshot", 5, 0, 1);
    Inventory.push_back(Temp_Slingshot);
 //   UsedWeaponForSecondaryAttack = Temp_Slingshot;
//!    Temp_Slingshot->setIsEquiped(true);
    SecondaryAttack.setUsedWeaponForThatAttack(Temp_Slingshot);

    craftingTool* Temp_Saw = new craftingTool("Saw", trunk);
    Inventory.push_back( Temp_Saw );
    ChopThatWood.setCraftingTool(Temp_Saw);

}


character::~character(){

    cout << getTimestampInMiliseconds() << " character::~character(): destroyed" << endl;

}


position character::getPosition(){
    return Position;
}


void character::setPosition(position In_Pos){
    Position = In_Pos;
}



bool character::tryToMove( direction In_Direction){//called from sController::interpredReceivedMessage()

    cout << "\nCharacter " << Charactername << " Receives a Request to move ";
    bool MovementSuccesfull = false;

    if(In_Direction == d_left){
        cout << "left";
        Position.PosX--;
        MovementSuccesfull = true;
    }else if(In_Direction == d_right){
        cout << "right";
        Position.PosX++;
        MovementSuccesfull = true;
    }else if(In_Direction == d_up){
        cout << "up";
        Position.PosY--;
        MovementSuccesfull = true;
    }else if(In_Direction == d_down){
        cout << "down";
        Position.PosY++;
        MovementSuccesfull = true;
    }else if(In_Direction == d_downleft){
        cout << "down-left";
        Position.PosX--;
        Position.PosY++;
        MovementSuccesfull = true;
    }else if(In_Direction == d_downright){
        cout << "down-right";
        Position.PosX++;
        Position.PosY++;
        MovementSuccesfull = true;
    }else if(In_Direction == d_upleft){
        cout << "up-left";
        Position.PosX--;
        Position.PosY--;
        MovementSuccesfull = true;
    }else if(In_Direction == d_upright){
        cout << "up-right";
        Position.PosX++;
        Position.PosY--;
        MovementSuccesfull = true;
    }
        cout << endl << endl;

    return MovementSuccesfull;

}

string character::getCharactername(){
    return Charactername;
}

void character::setCharactername( string In_CharacterName ){
    Charactername = In_CharacterName;
}

/*!
void character::updateSightVector( vector < vector < vector < cube*>* > > In_NewSightVector ){   //called from UserAccount::updateSight()
    cout << getTimestampInMiliseconds() << " character::updateSightVector()" << endl;
bool operator!= (const position& In_LeftHandSide, const position& In_RightHandSide){
    SightField = In_NewSightVector;

    cout << getTimestampInMiliseconds() << " leaving character::updateSightVector() " << endl;

}
*/

int64_t character::getSightRadius(){
    return SightRadius;
}

void character::setSightRadius( int In_SightRadius ){
    SightRadius = In_SightRadius;
}


/*
vector < vector < vector <cube*>*>> character::getSightField(){
    return SightField;
}
*/

vector < pair<position, cube*> > character::getSightField(){
    return SightField;
}


vector < pair<position, cube*> >* character::getSightFieldPointer(){
    return &SightField;
}


bool character::isCharacterAbleToMove(){
    return IsCharacterAbleToMove;
}


string character::getTimestampInMiliseconds(){

    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;

    string Output = to_string(ms);
    Output.erase( 0, 4 );

    return Output;
}


//!
/*!
void character::calculateArrayOfMinivectorsForSightField(){

    cout << getTimestampInMiliseconds() << " entering character::calculateArrayOfMinivectorsForSightField()" << endl;


    int SightRadiusSquared = pow(SightRadius, 2);

    position CornerOfSightCube;
    CornerOfSightCube.PosY = 2 * SightRadius;         //plus one, because the Character is in the Middle of the SightCube
    CornerOfSightCube.PosX = 2 * SightRadius;
    CornerOfSightCube.PosZ = 2 * SightRadius;

    position PositionOfCharacter;
    PositionOfCharacter.PosY = SightRadius;
    PositionOfCharacter.PosX = SightRadius;
    PositionOfCharacter.PosZ = SightRadius;

    position AimedCube;
    AimedCube.PosY = 0;
    AimedCube.PosX = 0;
    AimedCube.PosZ = 0;

    position MaxiVector;    //pointing from the character to the aimed cube

    int MaxOf_PosY_And_PosX = 0;
    int MaxOf_PosY_And_PosY_And_PosZ = 0;

    miniVectorStruct Temp_MiniVector;
    float LengthOfMiniVector = 0;

    int CounterOfCubesInGeneral = 0;
    int CounterOfSurfaceCubes = 0;
    bool FirstSurfaceOfSqhaereFound = false;
    bool SecondSurfaceOfSquareFound = false;

    //!Debug:
    int Counter_A = 0;
    int Counter_B = 0;
    int Counter_C = 0;
    int Counter_D = 0;
    int Counter_E = 0;
    int Counter_F = 0;
    int Counter_G = 0;
    int Counter_H = 0;
    //!

    for( AimedCube.PosY; AimedCube.PosY < CornerOfSightCube.PosY; AimedCube.PosY++ ){
        AimedCube.PosX = 0;
        for( AimedCube.PosX; AimedCube.PosX < CornerOfSightCube.PosX ; AimedCube.PosX++  ){
            AimedCube.PosZ = 0;
            FirstSurfaceOfSqhaereFound = false;
            SecondSurfaceOfSquareFound = false;
            for( AimedCube.PosZ; AimedCube.PosZ < CornerOfSightCube.PosZ && SecondSurfaceOfSquareFound == false; AimedCube.PosZ++ ){
                CounterOfCubesInGeneral++;
                if(FirstSurfaceOfSqhaereFound == false){
                    if( pow( (PositionOfCharacter.PosZ - AimedCube.PosZ), 2 ) + pow( (PositionOfCharacter.PosY - AimedCube.PosY), 2  ) + pow( (PositionOfCharacter.PosX - AimedCube.PosX), 2 ) <= SightRadiusSquared ){
                        FirstSurfaceOfSqhaereFound = true;
                        CounterOfSurfaceCubes++;
                        //cout << "character::calculateArrayOfMinivectorsForSightField(): cube on surface found. Counter: " << CounterOfSurfaceCubes << " ; AimedCube: " << AimedCube.PosY << ";" << AimedCube.PosX << ";" << AimedCube.PosZ << endl;

                    }
                }else{  // i need that, because a square alsways has 2 points on the surface per Y-X-koordinate
                    if( pow( (PositionOfCharacter.PosZ - AimedCube.PosZ), 2 ) + pow( (PositionOfCharacter.PosY - AimedCube.PosY), 2  ) + pow( (PositionOfCharacter.PosX - AimedCube.PosX), 2 ) >= SightRadiusSquared ){
                        SecondSurfaceOfSquareFound = true;
                        CounterOfSurfaceCubes++;
                        //cout << "character::calculateArrayOfMinivectorsForSightField(): cube on surface found. Counter: " << CounterOfSurfaceCubes << " ; AimedCube: " << AimedCube.PosY << ";" << AimedCube.PosX << ";" << AimedCube.PosZ << "\n" << endl;
                    }
                }


                if( FirstSurfaceOfSqhaereFound || SecondSurfaceOfSquareFound ){//if some cube on the surface has been found

                //calculating the vector from character to AimedCube
                    MaxiVector.PosY = AimedCube.PosY - PositionOfCharacter.PosY;
                    MaxiVector.PosX = AimedCube.PosX - PositionOfCharacter.PosX;
                    MaxiVector.PosZ = AimedCube.PosZ - PositionOfCharacter.PosZ;

                    //finding out, which dimension is the longest:
                    MaxOf_PosY_And_PosX = max( abs(MaxiVector.PosY), abs(MaxiVector.PosX) );
                    MaxOf_PosY_And_PosY_And_PosZ = max( abs(MaxiVector.PosZ), MaxOf_PosY_And_PosX );

                    //!debug:
                    if( MaxOf_PosY_And_PosY_And_PosZ  == 0 ){
                        cout << "MaxOf_PosY_And_PosY_And_PosZ = " << MaxOf_PosY_And_PosY_And_PosZ << endl;
                    }
                    //!
                    if( MaxOf_PosY_And_PosY_And_PosZ != 0 ){
                        //calculating the deltas. these are acually the miniVectors
                        Temp_MiniVector.MiniVector.PosY = float(MaxiVector.PosY)/MaxOf_PosY_And_PosY_And_PosZ;
                        Temp_MiniVector.MiniVector.PosX = float(MaxiVector.PosX)/MaxOf_PosY_And_PosY_And_PosZ;
                        Temp_MiniVector.MiniVector.PosZ = float(MaxiVector.PosZ)/MaxOf_PosY_And_PosY_And_PosZ;

                        //calculating length of MiniVector
                        LengthOfMiniVector = sqrt( pow( Temp_MiniVector.MiniVector.PosY, 2) + pow( Temp_MiniVector.MiniVector.PosX, 2) + pow( Temp_MiniVector.MiniVector.PosZ, 2)  );
                        Temp_MiniVector.NumberOfAdditionsToGetTheSightRadius =  ( float(SightRadius) / LengthOfMiniVector ) + 0.5;
                        //cout << "Temp_MiniVector.NumberOfAdditionsToGetTheSightRadius: " << Temp_MiniVector.NumberOfAdditionsToGetTheSightRadius << endl;

                        // now its time to find out in which SightField-Segment the vector belongs
                        if( Temp_MiniVector.MiniVector.PosY > 0 && Temp_MiniVector.MiniVector.PosX > 0 && Temp_MiniVector.MiniVector.PosZ > 0 ){//!we are in SightFieldSegment_A
                            //!debug:
                            Counter_A++;
                            //!in other else-if-statemens analog
                            SightFieldSegment_A.push_back(Temp_MiniVector);
                        }else if( Temp_MiniVector.MiniVector.PosY <= 0 && Temp_MiniVector.MiniVector.PosX > 0 && Temp_MiniVector.MiniVector.PosZ > 0 ){//SightFieldSegment_B
                            SightFieldSegment_B.push_back(Temp_MiniVector);
                            Counter_B++;
                        }else if( Temp_MiniVector.MiniVector.PosY <= 0 && Temp_MiniVector.MiniVector.PosX <= 0 && Temp_MiniVector.MiniVector.PosZ > 0 ){//SightFieldSegment_C
                            SightFieldSegment_C.push_back(Temp_MiniVector);
                            Counter_C++;
                        }else if( Temp_MiniVector.MiniVector.PosY > 0 && Temp_MiniVector.MiniVector.PosX <= 0 && Temp_MiniVector.MiniVector.PosZ > 0 ){//SightFieldSegment_D
                            SightFieldSegment_D.push_back(Temp_MiniVector);
                            Counter_D++;
                        }else if( Temp_MiniVector.MiniVector.PosY > 0 && Temp_MiniVector.MiniVector.PosX > 0 && Temp_MiniVector.MiniVector.PosZ <= 0 ){//!SightFieldSegment_E
                            SightFieldSegment_E.push_back(Temp_MiniVector);
                            Counter_E++;
                        }else if( Temp_MiniVector.MiniVector.PosY <= 0 && Temp_MiniVector.MiniVector.PosX > 0 && Temp_MiniVector.MiniVector.PosZ <= 0 ){//SightFieldSegment_F
                            SightFieldSegment_F.push_back(Temp_MiniVector);
                            Counter_F++;
                        }else if( Temp_MiniVector.MiniVector.PosY <= 0 && Temp_MiniVector.MiniVector.PosX <= 0 && Temp_MiniVector.MiniVector.PosZ <= 0 ){//SightFieldSegment_G
                            SightFieldSegment_G.push_back(Temp_MiniVector);
                            Counter_G++;
                        }else if( Temp_MiniVector.MiniVector.PosY > 0 && Temp_MiniVector.MiniVector.PosX <= 0 && Temp_MiniVector.MiniVector.PosZ <= 0 ){//SightFieldSegment_H
                            SightFieldSegment_H.push_back(Temp_MiniVector);
                            Counter_H++;
                        }else{
                            cout << "!! error. couldn't sort MiniVector in Segment: " << Temp_MiniVector.MiniVector.PosY << ";" << Temp_MiniVector.MiniVector.PosX << ";" << Temp_MiniVector.MiniVector.PosZ << endl;
                            cout << "exit now!" << endl;
                            exit(0);
                        }
                    }else{
                        cout << "character::calculateArrayOfMinivectorsForSightField(): tryed division by zero!" << endl;
                    }
                }
            }
        }
    }


    cout << getTimestampInMiliseconds() << " leaving character::calculateArrayOfMinivectorsForSightField()" << endl;




}
*/
//!

/*!
sightNode character::getSightFieldSegment(string In_WhichSightFieldSegment){
    if(In_WhichSightFieldSegment == "A"){
        return SightFieldSegment_A;
    }else if(In_WhichSightFieldSegment == "B"){
        return  SightFieldSegment_B;
    }else if(In_WhichSightFieldSegment == "C"){
        return  SightFieldSegment_C;
    }else if(In_WhichSightFieldSegment == "D"){
        return  SightFieldSegment_D;
    }else if(In_WhichSightFieldSegment == "E"){
        return  SightFieldSegment_E;
    }else if(In_WhichSightFieldSegment == "F"){
        return  SightFieldSegment_F;
    }else if(In_WhichSightFieldSegment == "G"){
        return  SightFieldSegment_G;
    }else if(In_WhichSightFieldSegment == "H"){
        return  SightFieldSegment_H;
    }else{
        cout << "requested letter is not valid: " << In_WhichSightFieldSegment << endl;
        exit(0);

    }


}
*/


/*!
void character::initializeSightField(){

    cout << getTimestampInMiliseconds() << " entering character::initializeSightField()" << endl;

    position AimedCube;

    position CornerOfSightCube;
    CornerOfSightCube.PosY = 2 * SightRadius;
    CornerOfSightCube.PosX = 2 * SightRadius;
    CornerOfSightCube.PosZ = 2 * SightRadius;

    position PositionOfCharacter;
    PositionOfCharacter.PosY = SightRadius;
    PositionOfCharacter.PosX = SightRadius;
    PositionOfCharacter.PosZ = SightRadius;

    int RadiusOfCurrentShell = -1;
    int RadiusOfCurrentShellSquared1 = -1;
    int RadiusOfCurrentShellSquared2 = -1;

    bool FirstSurfaceOfSqhaereFound = false;
    bool SecondSurfaceOfSqhaereFound = false;

    int CounterOfSurfaceCubes = -1;
    bool DoItNow = false;

    //!Debug:
    int CounterA = 0;
    int CounterB = 0;
    int CounterC = 0;
    int CounterD = 0;
    int CounterE = 0;
    int CounterF = 0;
    int CounterG = 0;
    int CounterH = 0;
    //!

    for( int NumberOfShell = 1; NumberOfShell <= NUMBER_OF_SHELLS; NumberOfShell++ ){

        cout << "NumberOfShell: " << NumberOfShell << " ;[1=A, 2=B, 3=C, 4=D, 5=E, 6=F, 7=G, 8=H]" << endl;

        AimedCube.PosY = 0;
        AimedCube.PosX = 0;
        AimedCube.PosZ = 0;

        RadiusOfCurrentShell = NumberOfShell * ( SightRadius / NUMBER_OF_SHELLS );

        //!RadiusOfCurrentShellSquared = pow(RadiusOfCurrentShell, 2);
        RadiusOfCurrentShellSquared1 = pow(float(RadiusOfCurrentShell)+0.5, 2);
        RadiusOfCurrentShellSquared2 = pow(float(RadiusOfCurrentShell)-0.5, 2);


        CounterOfSurfaceCubes = 0;


        for( AimedCube.PosY; AimedCube.PosY <= CornerOfSightCube.PosY; AimedCube.PosY++ ){
            AimedCube.PosX = 0;
            for( AimedCube.PosX; AimedCube.PosX <= CornerOfSightCube.PosX ; AimedCube.PosX++  ){
                AimedCube.PosZ = 0;
                FirstSurfaceOfSqhaereFound = false;
                SecondSurfaceOfSqhaereFound = false;

                for( AimedCube.PosZ; AimedCube.PosZ <= CornerOfSightCube.PosZ && SecondSurfaceOfSqhaereFound == false; AimedCube.PosZ++ ){



                    //!Debug:
                    cout << "distance: " << sqrt( pow( (PositionOfCharacter.PosZ - AimedCube.PosZ), 2 ) + pow( (PositionOfCharacter.PosY - AimedCube.PosY), 2  ) + pow( (PositionOfCharacter.PosX - AimedCube.PosX), 2 ) ) << endl;
                    if( AimedCube.PosY == 10 && AimedCube.PosX == 14 ){
                        cout << "Debug" << endl;
                    }
                    position RelativePositionOfTheNode;
                    RelativePositionOfTheNode.PosY = AimedCube.PosY - PositionOfCharacter.PosY;
                    RelativePositionOfTheNode.PosX = AimedCube.PosX - PositionOfCharacter.PosX;
                    RelativePositionOfTheNode.PosZ = AimedCube.PosZ - PositionOfCharacter.PosZ;
                    cout << "RelativePositionOfTheNode: " << RelativePositionOfTheNode.PosY << ";" << RelativePositionOfTheNode.PosX << ";" << RelativePositionOfTheNode.PosZ << endl;
                    if( RelativePositionOfTheNode.PosZ == 0 && (RelativePositionOfTheNode.PosX == -5) ){
                        cout << endl;
                    }
                    //!



                    //cout << "AimedCube: " << AimedCube.PosY << ";" << AimedCube.PosX << ";" << AimedCube.PosZ << endl;
                    if(FirstSurfaceOfSqhaereFound == false){
                        cout << "pow( (PositionOfCharacter.PosZ - AimedCube.PosZ), 2 ) + pow( (PositionOfCharacter.PosY - AimedCube.PosY), 2  ) + pow( (PositionOfCharacter.PosX - AimedCube.PosX), 2 ): " << pow( (PositionOfCharacter.PosZ - AimedCube.PosZ), 2 ) + pow( (PositionOfCharacter.PosY - AimedCube.PosY), 2  ) + pow( (PositionOfCharacter.PosX - AimedCube.PosX), 2 ) << endl;
                        if( pow( (PositionOfCharacter.PosZ - AimedCube.PosZ), 2 ) + pow( (PositionOfCharacter.PosY - AimedCube.PosY), 2  ) + pow( (PositionOfCharacter.PosX - AimedCube.PosX), 2 ) <= RadiusOfCurrentShellSquared1 ){

                            //!Debug:
                            if( RelativePositionOfTheNode.PosY == -2 && RelativePositionOfTheNode.PosX == -4 ){
                                cout << endl;
                            }
                            //!

                            FirstSurfaceOfSqhaereFound = true;
                            //cout << "character::calculateArraysOfSightNodesAndMiniVectors(): cube on surface found. Counter: " << CounterOfSurfaceCubes << " ; AimedCube: " << AimedCube.PosY << ";" << AimedCube.PosX << ";" << AimedCube.PosZ << " set FirstSurfaceOfSqhaereFound true" << endl;
                            CounterOfSurfaceCubes++;
                            DoItNow = true;
                        }
                    }else{  //FirstSurfaceOfSqhaereFound == true
                        // i need that, because a sphare alsways has 2 points on the surface per Y-X-koordinate

                        cout << "pow( (PositionOfCharacter.PosZ - AimedCube.PosZ), 2 ) + pow( (PositionOfCharacter.PosY - AimedCube.PosY), 2  ) + pow( (PositionOfCharacter.PosX - AimedCube.PosX), 2 ): " << pow( (PositionOfCharacter.PosZ - AimedCube.PosZ), 2 ) + pow( (PositionOfCharacter.PosY - AimedCube.PosY), 2  ) + pow( (PositionOfCharacter.PosX - AimedCube.PosX), 2 ) << endl;
                        if( pow( (PositionOfCharacter.PosZ - AimedCube.PosZ), 2 ) + pow( (PositionOfCharacter.PosY - AimedCube.PosY), 2  ) + pow( (PositionOfCharacter.PosX - AimedCube.PosX), 2 ) >= RadiusOfCurrentShellSquared2 ){

                            //!Debug:
                            if( RelativePositionOfTheNode.PosY == -2 && RelativePositionOfTheNode.PosX == -4  ){
                                cout << endl;
                            }
                            //!

                            SecondSurfaceOfSqhaereFound = true;
                            //cout << "character::calculateArraysOfSightNodesAndMiniVectors(): cube on surface found. Counter: " << CounterOfSurfaceCubes << " ; AimedCube: " << AimedCube.PosY << ";" << AimedCube.PosX << ";" << AimedCube.PosZ << " set SecondSurfaceOfSqhaereFound true" << endl;
                            CounterOfSurfaceCubes++;
                            DoItNow = true;
                        }
                    }

                    if( (FirstSurfaceOfSqhaereFound || SecondSurfaceOfSqhaereFound) && DoItNow ){//if some cube on the surface has been found


                        DoItNow = false;

                        position RelativePositionOfTheNode;
                        RelativePositionOfTheNode.PosY = AimedCube.PosY - PositionOfCharacter.PosY;
                        RelativePositionOfTheNode.PosX = AimedCube.PosX - PositionOfCharacter.PosX;
                        RelativePositionOfTheNode.PosZ = AimedCube.PosZ - PositionOfCharacter.PosZ;


                        //!Debug
                        cout << "Cube on Surface Found: RelativePosition: " << RelativePositionOfTheNode.PosY << ";" << RelativePositionOfTheNode.PosX << ";" << RelativePositionOfTheNode.PosZ << endl;
                        //!


                        bool Temp_IsEndNode = true;
                        if( NumberOfShell < NUMBER_OF_SHELLS ){
                            Temp_IsEndNode = false;
                        }
                        sightNode Temp_SightNode( RelativePositionOfTheNode, Temp_IsEndNode, NumberOfShell );
                        string WhichSegment = "";

                        vector <sightNode> Temp_Nodes;

                        //cout << "RealtivePositionOftheNode: " << RelativePositionOfTheNode.PosY << ";" << RelativePositionOfTheNode.PosX << ";" << RelativePositionOfTheNode.PosZ << endl;

                        if( RelativePositionOfTheNode.PosY > 0 && RelativePositionOfTheNode.PosX > 0 && RelativePositionOfTheNode.PosZ > 0 ){//!we are in SightFieldSegment_A
                            //cout << "loading SightFieldSegment_A" << endl;
                            Temp_Nodes = SightFieldSegment_A.getAllConnectedSightNodesPlusTheRootNode();
                            WhichSegment = "A";
                        }else if( RelativePositionOfTheNode.PosY <= 0 && RelativePositionOfTheNode.PosX > 0 && RelativePositionOfTheNode.PosZ > 0 ){//SightFieldSegment_B
                            //cout << "loading SightFieldSegment_B" << endl;
                            Temp_Nodes = SightFieldSegment_B.getAllConnectedSightNodesPlusTheRootNode();
                            WhichSegment = "B";
                        }else if( RelativePositionOfTheNode.PosY <= 0 && RelativePositionOfTheNode.PosX <= 0 && RelativePositionOfTheNode.PosZ > 0 ){//SightFieldSegment_C
                            //cout << "loading SightFieldSegment_C" << endl;
                            Temp_Nodes = SightFieldSegment_C.getAllConnectedSightNodesPlusTheRootNode();
                            WhichSegment = "C";
                        }else if( RelativePositionOfTheNode.PosY > 0 && RelativePositionOfTheNode.PosX <= 0 && RelativePositionOfTheNode.PosZ > 0 ){//SightFieldSegment_D
                            //cout << "loading SightFieldSegment_D" << endl;
                            Temp_Nodes = SightFieldSegment_D.getAllConnectedSightNodesPlusTheRootNode();
                            WhichSegment = "D";
                        }else if( RelativePositionOfTheNode.PosY > 0 && RelativePositionOfTheNode.PosX > 0 && RelativePositionOfTheNode.PosZ <= 0 ){//!SightFieldSegment_E
                            //cout << "loading SightFieldSegment_E" << endl;
                            Temp_Nodes = SightFieldSegment_E.getAllConnectedSightNodesPlusTheRootNode();
                            WhichSegment = "E";
                        }else if( RelativePositionOfTheNode.PosY <= 0 && RelativePositionOfTheNode.PosX > 0 && RelativePositionOfTheNode.PosZ <= 0 ){//SightFieldSegment_F
                            //cout << "loading SightFieldSegment_F" << endl;
                            Temp_Nodes = SightFieldSegment_F.getAllConnectedSightNodesPlusTheRootNode();
                            WhichSegment = "F";
                        }else if( RelativePositionOfTheNode.PosY <= 0 && RelativePositionOfTheNode.PosX <= 0 && RelativePositionOfTheNode.PosZ <= 0 ){//SightFieldSegment_G
                            //cout << "loading SightFieldSegment_G" << endl;
                            Temp_Nodes = SightFieldSegment_G.getAllConnectedSightNodesPlusTheRootNode();
                            WhichSegment = "G";
                        }else if( RelativePositionOfTheNode.PosY > 0 && RelativePositionOfTheNode.PosX <= 0 && RelativePositionOfTheNode.PosZ <= 0 ){//SightFieldSegment_H
                            //cout << "loading SightFieldSegment_H" << endl;
                            Temp_Nodes = SightFieldSegment_H.getAllConnectedSightNodesPlusTheRootNode();
                            WhichSegment = "H";
                        }



                        //!for Debugging issues: (is needed when only looking at SightFieldSegment_A (or in generalTemp_Nodes: just on Segment...))
                        //if( Temp_Nodes.size() > 0 ){
                        //!

                            //!searching for the nearest node.
                            float SmallestDistanceBetweenTheNodes = 99999999;
                            //!int IndexOfNearestNode = -1;
                            int IndexOfNearestNode = 0;
                            float Distance = -1;
                            int DEBUG_I = -1;
                            //cout << "size of Temp_Nodes: " << Temp_Nodes.size() << endl;
                            position PositionOfNearestNodeFound;
                            for( int I = 0; I < Temp_Nodes.size(); I++ ){
                                DEBUG_I = I;
                                position Temp_PositionOfCurrentNode = Temp_Nodes[I].getRelativePosition();
                                if( Temp_Nodes[I].isEndNode() == false ){
                                    Distance = sqrt( pow( (Temp_PositionOfCurrentNode.PosY - RelativePositionOfTheNode.PosY),2 ) + pow( (Temp_PositionOfCurrentNode.PosX - RelativePositionOfTheNode.PosX),2 ) + pow( (Temp_PositionOfCurrentNode.PosZ - RelativePositionOfTheNode.PosZ),2 ) );
                                    if( Distance <= SmallestDistanceBetweenTheNodes ){
                                        SmallestDistanceBetweenTheNodes = Distance;
                                        IndexOfNearestNode = I;
                                    }
                                }
                            }
                            //cout << "character::calculateArraysOfSightNodesAndMiniVectors(): trying to getRelativePosition from Node with Index: " << IndexOfNearestNode << endl;
                            PositionOfNearestNodeFound = Temp_Nodes[IndexOfNearestNode].getRelativePosition();



                            //cout << getTimestampInMiliseconds() << " character::calculateArraysOfSightNodesAndMiniVectors(): trying to insertNodeOnNodeWithThisPosition(). PositionOfNearestNodeFound: " << PositionOfNearestNodeFound.PosY << ";" << PositionOfNearestNodeFound.PosX << ";" << PositionOfNearestNodeFound.PosZ << " ;Temp_SightNode: Position: " << Temp_SightNode.getRelativePosition().PosY << ";" << Temp_SightNode.getRelativePosition().PosX << ";" << Temp_SightNode.getRelativePosition().PosZ << endl;
                            if( WhichSegment == "A" ){
                                //cout << "character::initializeSightField(): entering searchAndInsertNodeOnNodeWithThisPosition() For Segment A" << endl;
                                SightFieldSegment_A.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                CounterA++;
                            }else if( WhichSegment == "B" ){
                                cout << "entering searchAndInsertNodeOnNodeWithThisPosition() For Segment B" << endl;
                                SightFieldSegment_B.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                CounterB++;
                            }else if( WhichSegment == "C" ){
                                cout << "entering searchAndInsertNodeOnNodeWithThisPosition() For Segment C" << endl;
                                SightFieldSegment_C.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                CounterC++;
                            }else if( WhichSegment == "D" ){
                                cout << "entering searchAndInsertNodeOnNodeWithThisPosition() For Segment D" << endl;
                                SightFieldSegment_D.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                CounterD++;
                            }else if( WhichSegment == "E" ){
                                cout << "entering searchAndInsertNodeOnNodeWithThisPosition() For Segment E" << endl;
                                SightFieldSegment_E.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                CounterE++;
                            }else if( WhichSegment == "F" ){
                                 cout << "entering searchAndInsertNodeOnNodeWithThisPosition() For Segment F" << endl;
                                SightFieldSegment_F.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                CounterF++;
                            }else if( WhichSegment == "G" ){
                                cout << "entering searchAndInsertNodeOnNodeWithThisPosition() For Segment G" << endl;
                                SightFieldSegment_G.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                CounterG++;
                            }else if( WhichSegment == "H" ){
                                cout << "entering searchAndInsertNodeOnNodeWithThisPosition() For Segment H" << endl;
                                SightFieldSegment_H.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                CounterH++;
                            }

                        //}else{
                        //    //cout << "Temp_Nodes.size() <= 0" << endl;
                        //}


                }


                }

                //!Debug:
                if( SecondSurfaceOfSqhaereFound == false && FirstSurfaceOfSqhaereFound == true ){
                    cout << "only detected one surface-cube at a Y-X-Position." << endl;
                    //exit(0);
                }
                //!


            }


        }
    }


    cout << "CounterA: " << CounterA << endl;
    cout << "CounterB: " << CounterB << endl;
    cout << "CounterC: " << CounterC << endl;
    cout << "CounterD: " << CounterD << endl;
    cout << "CounterE: " << CounterE << endl;
    cout << "CounterF: " << CounterF << endl;
    cout << "CounterG: " << CounterG << endl;
    cout << "CounterH: " << CounterH << endl;
    cout << "CounterOfSurfaceCubes: " << CounterOfSurfaceCubes << endl;

    cout << getTimestampInMiliseconds() << " leaving character::initializeSightField()" << endl;



}

*/


/*!
void character::initializeSightField(){

    //ok, the new idea is, that we go through every cube in The SightCube and put a vector from
    //the character through this cube, we are aiming at. then we extend that vector untill we reached the
    //radius sight. the cube, where we hit the sight-shaere is the node, we need to save


    vector<position> NodesAlreadyFound;   //i just use cube here, because i already implemented the < operator for cube, which is needed for cubes
                                        //the int is actually not used. i just need this map to check, if i already had this node, to avoid duplicate elements...

    cout << getTimestampInMiliseconds() << " entering character::initializeSightField()" << endl;

    position CornerOfSightCube;
    CornerOfSightCube.PosY = 2 * SightRadius;
    CornerOfSightCube.PosX = 2 * SightRadius;
    CornerOfSightCube.PosZ = 2 * SightRadius;

    position PositionOfCharacter;
    PositionOfCharacter.PosY = SightRadius;
    PositionOfCharacter.PosX = SightRadius;
    PositionOfCharacter.PosZ = SightRadius;

    position Temp_PositionAfterAddingSomeMiniVectors;

    bool SurfaceCubeFound = false;

    position AimedCube;
    position RelativeVector_CubeAimingAt;

    beam MiniVector;

    int RadiusOfCurrentShell = -1;
    int RadiusOfCurrentShellSquared = -1;

    bool FirstSurfaceOfSqhaereFound = false;
    bool SecondSurfaceOfSqhaereFound = false;

    //!Debug:
    int CounterA = 0;
    int CounterB = 0;
    int CounterC = 0;
    int CounterD = 0;
    int CounterE = 0;
    int CounterF = 0;
    int CounterG = 0;
    int CounterH = 0;
    //!

    bool IsValidPosition = true;

    for( int NumberOfShell = 1; NumberOfShell <= NUMBER_OF_SHELLS; NumberOfShell++ ){
        cout << "NumberOfShell: " << NumberOfShell << " ;[1=A, 2=B, 3=C, 4=D, 5=E, 6=F, 7=G, 8=H]" << endl;

        RadiusOfCurrentShell = NumberOfShell * ( SightRadius / NUMBER_OF_SHELLS );
        RadiusOfCurrentShellSquared = pow(float(RadiusOfCurrentShell), 2);

        AimedCube.PosY = 0;
        AimedCube.PosX = 0;
        AimedCube.PosZ = 0;

        for( AimedCube.PosY; AimedCube.PosY <= CornerOfSightCube.PosY; AimedCube.PosY++ ){
            AimedCube.PosX = 0;
            for( AimedCube.PosX; AimedCube.PosX <= CornerOfSightCube.PosX ; AimedCube.PosX++  ){
                AimedCube.PosZ = 0;
                FirstSurfaceOfSqhaereFound = false;
                SecondSurfaceOfSqhaereFound = false;
                IsValidPosition = true;
                for( AimedCube.PosZ; AimedCube.PosZ <= CornerOfSightCube.PosZ && SecondSurfaceOfSqhaereFound == false; AimedCube.PosZ++ ){
                    if( AimedCube.PosY == PositionOfCharacter.PosY && AimedCube.PosX == PositionOfCharacter.PosX && AimedCube.PosZ == PositionOfCharacter.PosZ ){
                    //    cout << "Position is Not Valid!" << endl;
                        IsValidPosition = false;
                    }else{
                    //    cout << "Position Is Valid" << endl;
                    }
                    //cout << "AimedCube: " << AimedCube.PosY << ";" << AimedCube.PosX << ";" << AimedCube.PosZ << endl,
                    //cout << "pow( (PositionOfCharacter.PosZ - AimedCube.PosZ), 2 ) + pow( (PositionOfCharacter.PosY - AimedCube.PosY), 2  ) + pow( (PositionOfCharacter.PosX - AimedCube.PosX), 2 ): " << pow( (PositionOfCharacter.PosZ - AimedCube.PosZ), 2 ) + pow( (PositionOfCharacter.PosY - AimedCube.PosY), 2  ) + pow( (PositionOfCharacter.PosX - AimedCube.PosX), 2 ) << endl;
                    if( IsValidPosition && pow( (PositionOfCharacter.PosZ - AimedCube.PosZ), 2 ) + pow( (PositionOfCharacter.PosY - AimedCube.PosY), 2  ) + pow( (PositionOfCharacter.PosX - AimedCube.PosX), 2 ) <= RadiusOfCurrentShellSquared ){

                        //cout << "AimedCube is inside Sphare" << endl;

                        RelativeVector_CubeAimingAt.PosY = AimedCube.PosY - PositionOfCharacter.PosY;
                        RelativeVector_CubeAimingAt.PosX = AimedCube.PosX - PositionOfCharacter.PosX;
                        RelativeVector_CubeAimingAt.PosZ = AimedCube.PosZ - PositionOfCharacter.PosZ;

                        int MaxOf_PosY_And_PosX = max( abs(RelativeVector_CubeAimingAt.PosY), abs(RelativeVector_CubeAimingAt.PosX) );
                        int MaxOf_PosY_And_PosY_And_PosZ;
                        MaxOf_PosY_And_PosY_And_PosZ = max( abs(RelativeVector_CubeAimingAt.PosZ), MaxOf_PosY_And_PosX );

                        //calculating the deltas
                        MiniVector.PosY = float(RelativeVector_CubeAimingAt.PosY)/MaxOf_PosY_And_PosY_And_PosZ;
                        MiniVector.PosX = float(RelativeVector_CubeAimingAt.PosX)/MaxOf_PosY_And_PosY_And_PosZ;
                        MiniVector.PosZ = float(RelativeVector_CubeAimingAt.PosZ)/MaxOf_PosY_And_PosY_And_PosZ;

                        Temp_PositionAfterAddingSomeMiniVectors = RelativeVector_CubeAimingAt;

                        SurfaceCubeFound = false;

                        for( int I=0; SurfaceCubeFound == false; I++ ){

                            float Temp_Float_PositionAfterAddingSomeMiniVectorsY = float(RelativeVector_CubeAimingAt.PosY) + (I * MiniVector.PosY);
                            float Temp_Float_PositionAfterAddingSomeMiniVectorsX = float(RelativeVector_CubeAimingAt.PosX) + (I * MiniVector.PosX);
                            float Temp_Float_PositionAfterAddingSomeMiniVectorsZ = float(RelativeVector_CubeAimingAt.PosZ) + (I * MiniVector.PosZ);

                            if( Temp_Float_PositionAfterAddingSomeMiniVectorsY > 0 ){
                                Temp_PositionAfterAddingSomeMiniVectors.PosY = Temp_Float_PositionAfterAddingSomeMiniVectorsY + 0.5;
                            }else if( Temp_Float_PositionAfterAddingSomeMiniVectorsY < 0 ){
                                Temp_PositionAfterAddingSomeMiniVectors.PosY = Temp_Float_PositionAfterAddingSomeMiniVectorsY - 0.5;
                            }else if( Temp_Float_PositionAfterAddingSomeMiniVectorsY == 0 ){
                                Temp_PositionAfterAddingSomeMiniVectors.PosY = 0;
                            }else{
                                cout << "that can never happen" << endl;
                            }
                            if( Temp_Float_PositionAfterAddingSomeMiniVectorsX > 0 ){
                                Temp_PositionAfterAddingSomeMiniVectors.PosX = Temp_Float_PositionAfterAddingSomeMiniVectorsX + 0.5;
                            }else if( Temp_Float_PositionAfterAddingSomeMiniVectorsX < 0 ){
                                Temp_PositionAfterAddingSomeMiniVectors.PosX = Temp_Float_PositionAfterAddingSomeMiniVectorsX - 0.5;
                            }else if( Temp_Float_PositionAfterAddingSomeMiniVectorsX == 0 ){
                                Temp_PositionAfterAddingSomeMiniVectors.PosX = 0;
                            }else{
                                cout << "that can never happen" << endl;
                            }

                            if( Temp_Float_PositionAfterAddingSomeMiniVectorsZ > 0 ){
                                Temp_PositionAfterAddingSomeMiniVectors.PosZ = Temp_Float_PositionAfterAddingSomeMiniVectorsZ + 0.5;
                            }else if( Temp_Float_PositionAfterAddingSomeMiniVectorsZ < 0 ){
                                Temp_PositionAfterAddingSomeMiniVectors.PosZ = Temp_Float_PositionAfterAddingSomeMiniVectorsZ - 0.5;
                            }else if( Temp_Float_PositionAfterAddingSomeMiniVectorsZ == 0 ){
                                Temp_PositionAfterAddingSomeMiniVectors.PosZ = 0;
                            }else{
                                cout << "that can never happen" << endl;
                            }


                            //cout << "Temp_PositionAfterAddingSomeMiniVectors: " << Temp_PositionAfterAddingSomeMiniVectors.PosY << ";" << Temp_PositionAfterAddingSomeMiniVectors.PosX << ";" << Temp_PositionAfterAddingSomeMiniVectors.PosZ << endl;
                            //cout << "checking if Position is on Surface. I: " << I << endl;
                            //cout << "pow( (Temp_Float_PositionAfterAddingSomeMiniVectorsY), 2 ) + pow( (Temp_Float_PositionAfterAddingSomeMiniVectorsX), 2  ) + pow( (Temp_Float_PositionAfterAddingSomeMiniVectorsZ), 2 ): " << pow( (Temp_Float_PositionAfterAddingSomeMiniVectorsY), 2 ) + pow( (Temp_Float_PositionAfterAddingSomeMiniVectorsX), 2  ) + pow( (Temp_Float_PositionAfterAddingSomeMiniVectorsZ), 2 ) << endl;
                            //if(  pow( (PositionOfCharacter.PosZ - AimedCube.PosZ), 2 ) + pow( (PositionOfCharacter.PosY - AimedCube.PosY), 2  ) + pow( (PositionOfCharacter.PosX - AimedCube.PosX), 2 ) >= RadiusOfCurrentShellSquared ){
                            if( pow( (Temp_Float_PositionAfterAddingSomeMiniVectorsY), 2 ) + pow( (Temp_Float_PositionAfterAddingSomeMiniVectorsX), 2  ) + pow( (Temp_Float_PositionAfterAddingSomeMiniVectorsZ), 2 ) >= RadiusOfCurrentShellSquared ){
                                SurfaceCubeFound = true;
                            }

                        }

                        if( SurfaceCubeFound ){
                            bool Temp_IsEndNode = true;

                            if( NumberOfShell < NUMBER_OF_SHELLS ){
                                Temp_IsEndNode = false;
                            }

                            string WhichSegment = "";

                            vector <sightNode> Temp_Nodes;

                            if( Temp_PositionAfterAddingSomeMiniVectors.PosY > 0 && Temp_PositionAfterAddingSomeMiniVectors.PosX > 0 && Temp_PositionAfterAddingSomeMiniVectors.PosZ > 0 ){//!we are in SightFieldSegment_A
                                //cout << "loading SightFieldSegment_A" << endl;
                                Temp_Nodes = SightFieldSegment_A.getAllConnectedSightNodesPlusTheRootNode();
                                WhichSegment = "A";
                            }else if( Temp_PositionAfterAddingSomeMiniVectors.PosY <= 0 && Temp_PositionAfterAddingSomeMiniVectors.PosX > 0 && Temp_PositionAfterAddingSomeMiniVectors.PosZ > 0 ){//SightFieldSegment_B
                                //cout << "loading SightFieldSegment_B" << endl;
                                Temp_Nodes = SightFieldSegment_B.getAllConnectedSightNodesPlusTheRootNode();
                                WhichSegment = "B";
                            }else if( Temp_PositionAfterAddingSomeMiniVectors.PosY <= 0 && Temp_PositionAfterAddingSomeMiniVectors.PosX <= 0 && Temp_PositionAfterAddingSomeMiniVectors.PosZ > 0 ){//SightFieldSegment_C
                                //cout << "loading SightFieldSegment_C" << endl;
                                Temp_Nodes = SightFieldSegment_C.getAllConnectedSightNodesPlusTheRootNode();
                                WhichSegment = "C";
                            }else if( Temp_PositionAfterAddingSomeMiniVectors.PosY > 0 && Temp_PositionAfterAddingSomeMiniVectors.PosX <= 0 && Temp_PositionAfterAddingSomeMiniVectors.PosZ > 0 ){//SightFieldSegment_D
                                //cout << "loading SightFieldSegment_D" << endl;
                                Temp_Nodes = SightFieldSegment_D.getAllConnectedSightNodesPlusTheRootNode();
                                WhichSegment = "D";
                            }else if( Temp_PositionAfterAddingSomeMiniVectors.PosY > 0 && Temp_PositionAfterAddingSomeMiniVectors.PosX > 0 && Temp_PositionAfterAddingSomeMiniVectors.PosZ <= 0 ){//!SightFieldSegment_E
                                //cout << "loading SightFieldSegment_E" << endl;
                                Temp_Nodes = SightFieldSegment_E.getAllConnectedSightNodesPlusTheRootNode();
                                WhichSegment = "E";
                            }else if( Temp_PositionAfterAddingSomeMiniVectors.PosY <= 0 && Temp_PositionAfterAddingSomeMiniVectors.PosX > 0 && Temp_PositionAfterAddingSomeMiniVectors.PosZ <= 0 ){//SightFieldSegment_F
                                //cout << "loading SightFieldSegment_F" << endl;
                                Temp_Nodes = SightFieldSegment_F.getAllConnectedSightNodesPlusTheRootNode();
                                WhichSegment = "F";
                            }else if( Temp_PositionAfterAddingSomeMiniVectors.PosY <= 0 && Temp_PositionAfterAddingSomeMiniVectors.PosX <= 0 && Temp_PositionAfterAddingSomeMiniVectors.PosZ <= 0 ){//SightFieldSegment_G
                                //cout << "loading SightFieldSegment_G" << endl;
                                Temp_Nodes = SightFieldSegment_G.getAllConnectedSightNodesPlusTheRootNode();
                                WhichSegment = "G";
                            }else if( Temp_PositionAfterAddingSomeMiniVectors.PosY > 0 && Temp_PositionAfterAddingSomeMiniVectors.PosX <= 0 && Temp_PositionAfterAddingSomeMiniVectors.PosZ <= 0 ){//SightFieldSegment_H
                                //cout << "loading SightFieldSegment_H" << endl;
                                Temp_Nodes = SightFieldSegment_H.getAllConnectedSightNodesPlusTheRootNode();
                                WhichSegment = "H";
                            }


                            //!searching for the nearest node.
                            float SmallestDistanceBetweenTheNodes = 99999999;
                            int IndexOfNearestNode = 0;
                            float Distance = -1;
                            int DEBUG_I = -1;
                            //cout << "size of Temp_Nodes: " << Temp_Nodes.size() << endl;
                            position PositionOfNearestNodeFound;
                            for( int I = 0; I < Temp_Nodes.size(); I++ ){
                                DEBUG_I = I;
                                position Temp_PositionOfCurrentNode = Temp_Nodes[I].get_RelativePositionFromCharacterToNode();
                                if( Temp_Nodes[I].isEndNode() == false && ( NumberOfShell  - 1 == Temp_Nodes[I].getShellNumber()  ) ){
                                    Distance = sqrt( pow( (Temp_PositionOfCurrentNode.PosY - Temp_PositionAfterAddingSomeMiniVectors.PosY),2 ) + pow( (Temp_PositionOfCurrentNode.PosX - Temp_PositionAfterAddingSomeMiniVectors.PosX),2 ) + pow( (Temp_PositionOfCurrentNode.PosZ - Temp_PositionAfterAddingSomeMiniVectors.PosZ),2 ) );
                                    if( Distance <= SmallestDistanceBetweenTheNodes ){
                                        SmallestDistanceBetweenTheNodes = Distance;
                                        IndexOfNearestNode = I;
                                    }
                                }
                            }

                            PositionOfNearestNodeFound = Temp_Nodes[IndexOfNearestNode].get_RelativePositionFromCharacterToNode();

                            position RelativePositionFromNearestNodeFoundToTheToHandleNode;
                            RelativePositionFromNearestNodeFoundToTheToHandleNode.PosY = Temp_PositionAfterAddingSomeMiniVectors.PosY - PositionOfNearestNodeFound.PosY;
                            RelativePositionFromNearestNodeFoundToTheToHandleNode.PosX = Temp_PositionAfterAddingSomeMiniVectors.PosX - PositionOfNearestNodeFound.PosX;
                            RelativePositionFromNearestNodeFoundToTheToHandleNode.PosZ = Temp_PositionAfterAddingSomeMiniVectors.PosZ - PositionOfNearestNodeFound.PosZ;



    //!
                            sightNode Temp_SightNode( Temp_PositionAfterAddingSomeMiniVectors, Temp_IsEndNode, NumberOfShell, RelativePositionFromNearestNodeFoundToTheToHandleNode );


                            //!checking, if the node is already used
                            bool AlreadyUsed = false;
                            for(int Index = 0; Index < NodesAlreadyFound.size() && AlreadyUsed == false; Index++){
                                if( NodesAlreadyFound[Index].PosY == Temp_SightNode.get_RelativePositionFromCharacterToNode().PosY && NodesAlreadyFound[Index].PosX == Temp_SightNode.get_RelativePositionFromCharacterToNode().PosX && NodesAlreadyFound[Index].PosZ == Temp_SightNode.get_RelativePositionFromCharacterToNode().PosZ ){
                                    AlreadyUsed = true;
                                }
                            }

                            //!Debug
                            if( Temp_SightNode.get_RelativePositionFromCharacterToNode().PosY == 1 && Temp_SightNode.get_RelativePositionFromCharacterToNode().PosX == 3 && Temp_SightNode.get_RelativePositionFromCharacterToNode().PosZ == 0 ){
                                cout << "break" << endl;
                            }
                            //!

                            if( AlreadyUsed == false ){

                                NodesAlreadyFound.push_back( Temp_SightNode.get_RelativePositionFromCharacterToNode() );

                                if( WhichSegment == "A" ){
                                    //cout << "character::initializeSightField(): entering searchAndInsertNodeOnNodeWithThisPosition() For Segment A" << endl;
                                //!    cout << "Shell: " << NumberOfShell << " entering searchAndInsertNodeOnNodeWithThisPosition() For Segment A " << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosY << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosX << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosZ << endl;
                                    SightFieldSegment_A.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                    CounterA++;
                                }else if( WhichSegment == "B" ){
                                 //!   cout << "Shell: " << NumberOfShell << " entering searchAndInsertNodeOnNodeWithThisPosition() For Segment B " << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosY << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosX << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosZ << endl;
                                    SightFieldSegment_B.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                    CounterB++;
                                }else if( WhichSegment == "C" ){
                                //!    cout << "Shell: " << NumberOfShell << " entering searchAndInsertNodeOnNodeWithThisPosition() For Segment C " << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosY << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosX << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosZ << endl;
                                    SightFieldSegment_C.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                    CounterC++;
                                }else if( WhichSegment == "D" ){
                                //!    cout << "Shell: " << NumberOfShell << " entering searchAndInsertNodeOnNodeWithThisPosition() For Segment D " << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosY << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosX << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosZ << endl;
                                    SightFieldSegment_D.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                    CounterD++;
                                }else if( WhichSegment == "E" ){
                                //!    cout << "Shell: " << NumberOfShell << " entering searchAndInsertNodeOnNodeWithThisPosition() For Segment E " << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosY << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosX << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosZ << endl;
                                    SightFieldSegment_E.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                    CounterE++;
                                }else if( WhichSegment == "F" ){
                                //!    cout << "Shell: " << NumberOfShell << " entering searchAndInsertNodeOnNodeWithThisPosition() For Segment F " << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosY << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosX << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosZ << endl;
                                    SightFieldSegment_F.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                    CounterF++;
                                }else if( WhichSegment == "G" ){
                                //!    cout << "Shell: " << NumberOfShell << " entering searchAndInsertNodeOnNodeWithThisPosition() For Segment G " << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosY << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosX << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosZ << endl;
                                    SightFieldSegment_G.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                    //cout << "SightFieldSegment_G has now size: " << SightFieldSegment_G.NodesThisNodeIsPointingAt.size() << endl;
                                    CounterG++;
                                }else if( WhichSegment == "H" ){
                                //!    cout << "Shell: " << NumberOfShell << " entering searchAndInsertNodeOnNodeWithThisPosition() For Segment H " << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosY << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosX << ";" << Temp_SightNode.get_RelativePositionFromCharacterToNode().PosZ << endl;
                                    SightFieldSegment_H.searchAndInsertNodeOnNodeWithThisPosition(PositionOfNearestNodeFound, Temp_SightNode);
                                    CounterH++;
                                }
                            }

                        }else{
                            cout << "error! no surface-cube found! exit now!" << endl;
                            exit(0);
                        }

                    }else{
                        //cout << "AimedCube is not in the Shaere" << endl;
                    }
                }
            }
        }



    }

    cout << "CounterA: " << CounterA << endl;
    cout << "CounterB: " << CounterB << endl;
    cout << "CounterC: " << CounterC << endl;
    cout << "CounterD: " << CounterD << endl;
    cout << "CounterE: " << CounterE << endl;
    cout << "CounterF: " << CounterF << endl;
    cout << "CounterG: " << CounterG << endl;
    cout << "CounterH: " << CounterH << endl;
    //cout << "CounterOfSurfaceCubes: " << CounterOfSurfaceCubes << endl;


    //!sorting out double-enterys



    cout << getTimestampInMiliseconds() << " leaving character::initializeSightField()" << endl;

}
*/




/*
vector <vector <cube>> character::returnCubesTheCharacterCanSee(world In_World, string In_LetterOfSightFieldSegment){

    if( In_LetterOfSightFieldSegment == "A" ){
        SightFieldSegment_A.returnCubesTheCharacterCanSee(In_World, Position);
    }else if(In_LetterOfSightFieldSegment == "B"){
        SightFieldSegment_B.returnCubesTheCharacterCanSee(In_World, Position);
    }else if(In_LetterOfSightFieldSegment == "C"){
        SightFieldSegment_C.returnCubesTheCharacterCanSee(In_World, Position);
    }else if(In_LetterOfSightFieldSegment == "D"){
        SightFieldSegment_D.returnCubesTheCharacterCanSee(In_World, Position);
    }else if(In_LetterOfSightFieldSegment == "E"){
        SightFieldSegment_E.returnCubesTheCharacterCanSee(In_World, Position);
    }else if(In_LetterOfSightFieldSegment == "F"){
        SightFieldSegment_F.returnCubesTheCharacterCanSee(In_World, Position);
    }else if(In_LetterOfSightFieldSegment == "G"){
        SightFieldSegment_G.returnCubesTheCharacterCanSee(In_World, Position);
    }else if(In_LetterOfSightFieldSegment == "H"){
        SightFieldSegment_H.returnCubesTheCharacterCanSee(In_World, Position);
    }
}

*/

/*!
sightNode* character::returnPointerToSightNode(string In_WhichSightFieldSegment){

    if( In_WhichSightFieldSegment == "A" ){
        return &SightFieldSegment_A;
    }else if( In_WhichSightFieldSegment == "B" ){
        return &SightFieldSegment_B;
    }else if( In_WhichSightFieldSegment == "C" ){
        return &SightFieldSegment_C;
    }else if( In_WhichSightFieldSegment == "D" ){
        return &SightFieldSegment_D;
    }else if( In_WhichSightFieldSegment == "E" ){
        return &SightFieldSegment_E;
    }else if( In_WhichSightFieldSegment == "F" ){
        return &SightFieldSegment_F;
    }else if( In_WhichSightFieldSegment == "G" ){
        return &SightFieldSegment_G;
    }else if( In_WhichSightFieldSegment == "H" ){
        return &SightFieldSegment_H;
    }

}*/


float character::getRangeOfPrimaryWeapon(){

    float Output = -1;


    weapon* Temp_Pointer = PrimaryAttack.getPointerToWeapon();
    if ( Temp_Pointer != NULL ){
        Output = Temp_Pointer->getRange();
    }else{
        cout << "no primaryWeapon equiped" << endl;
    }
    return Output;
}

float character::getRangeOfSecondaryWeapon(){

    float Output = -1;

    weapon* Temp_Pointer = SecondaryAttack.getPointerToWeapon();
    if( Temp_Pointer != NULL ){
        Output = Temp_Pointer->getRange();
    }else{
        cout << "no secondaryWeapon equiped" << endl;
    }
    return Output;

}


void character::receiveDamage(int In_DamagePoints){
    Energy = Energy - ( In_DamagePoints / Armor );
    cout << getTimestampInMiliseconds() << " Character " << Charactername << " lost " << to_string( ( In_DamagePoints / Armor ) ) << " Energy. now has " << to_string( Energy ) << endl;
}


int character::getHitpointsOfPrimaryWeapon(){

    int Output = -1;

    weapon* Temp_Pointer = PrimaryAttack.getPointerToWeapon();
    if ( Temp_Pointer != NULL ){
        Output = ( PrimaryAttack.getMultiplicator() * Temp_Pointer->getHitpoints() );
    }else{
        cout << "no primaryWeapon equiped" << endl;
    }
    return Output;

}

int character::getHitpointsOfSecondaryWeapon(){

    int Output = -1;

    weapon* Temp_Pointer = SecondaryAttack.getPointerToWeapon();
    if( Temp_Pointer != NULL ){
        Output = ( SecondaryAttack.getMultiplicator() * Temp_Pointer->getHitpoints() );
    }else{
        cout << "no secondaryWeapon equiped" << endl;
    }

    return Output;


}

int character::getEnergy(){

    return Energy;

}


void character::unequipPrimaryWeapon(){
    PrimaryAttack.setUsedWeaponForThatAttack(NULL);
}


void character::unequipSecondaryWeapon(){
    SecondaryAttack.setUsedWeaponForThatAttack(NULL);
}



string character::getStringOfPosition(){

    return ( "Y:" + to_string(Position.PosY) + "*X:" + to_string(Position.PosX) + "*Z:" + to_string(Position.PosZ) );

}


void character::setZeroGravity( bool In_NewValue ){
    HasZeroGravity = In_NewValue;
}

bool character::getZeroGravity(){
    return HasZeroGravity;
}bool IsPositionInSightRadius( position In_Position );

/*
int* character::getPointerTo_Debug_Counter_IDsAddedToCubes(){
    return &Debug_Counter_IDsAddedToCubes;
}
*/


bool character::isPositionInSightRadius( position In_Position ){
    bool Output = false;

    position Difference = In_Position - this->Position;
    int DistanceSquared = Difference.PosY*Difference.PosY + Difference.PosX*Difference.PosX + Difference.PosZ*Difference.PosZ;
    int SightRadiusSquared = SightRadius * SightRadius;
    if( DistanceSquared <= SightRadiusSquared ){
        Output = true;
    }

    return Output;
}

int character::getIDofAccountThisCharacterBelongsTo(){
    return IDofAccountThisCharacterBelonsTo;
}
