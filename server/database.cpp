#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>       //filesystem operations
#include <unistd.h>         //usleep
#include <sys/time.h>           //needed for timestamp
#include <dirent.h>
#include <iterator>
#include <map>
#include <sstream>

#include "database.h"
#include "character.h"



using namespace std;


database::database(){
    if( isThereAnyDatabase() == false ){
        initDatabasse();
    }
}



database::~database(){

}

bool database::isThereAnyDatabase(){
    cout << getTimestampInMiliseconds() << " entering database::isThereAnyDatabase()" << endl;
    FilesystemOperation.lock();

    bool Output = false;

    //! taken from: https://stackoverflow.com/questions/18100097/portable-way-to-check-if-directory-exists-windows-linux-c
    char Foldername[] = "database";
    struct stat Info;
    stat(Foldername, &Info);


    if( Info.st_mode & S_IFDIR ){   //doesn't exist
        cout << "database::isThereAnyDatabase(): database-folder exists!" << endl;
        Output = true;
    }else{
        cout << "database::isThereAnyDatabase(): database-folder doesn't exist!" << endl;
        Output = false;
    }




    FilesystemOperation.unlock();
    cout << getTimestampInMiliseconds() << " leaving database::isThereAnyDatabase()" << endl;
    return Output;

}


bool database::initDatabasse(){     // if not existing, this funktion will create the folder for the database
    FilesystemOperation.lock();
    bool Output = false;


    system( "mkdir \"./database\"" );
    system( "mkdir \"./database/world\"" );
    system( "mkdir \"./database/accounts\"" );



    FilesystemOperation.unlock();
    return Output;
}




void database::writeWorld( vector<vector<vector<head>>>* In_WorldVector, worldSize In_WorldSize ){

    cout << getTimestampInMiliseconds() << " entering database::writeWorld()" << endl;

    cout << "trying to lock mutex" << endl;
    FilesystemOperation.lock();
    cout << "locked" << endl;

    //cout << "trying to write WorldSize" << endl;
    system("touch ./database/WorldSize");
    system( ("echo '" + to_string( In_WorldSize.Height ) + "' >> ./database/WorldSize").c_str() );
    system( ("echo '" + to_string(In_WorldSize.Width) + "' >> ./database/WorldSize").c_str() );
    system( ("echo '" + to_string(In_WorldSize.MaxLevelOverZero) + "' >> ./database/WorldSize").c_str() );
    //cout << "done" << endl;
    //system("sleep 3");


    for( size_t Index_Y = 0; Index_Y < In_WorldSize.Height; Index_Y++ ){
        for( size_t Index_X = 0; Index_X < In_WorldSize.Width; Index_X++ ){
            for( size_t Index_Z = 0; Index_Z < In_WorldSize.MaxLevelOverZero; Index_Z++ ){
                position Temp_Position;
                Temp_Position.PosY = Index_Y;
                Temp_Position.PosX = Index_X;
                Temp_Position.PosZ = Index_Z;
                //writeHeadInDatabase( In_World->getPointerToHeadOnPositionFromNormalWorld( Temp_Position ), Temp_Position );
                writeHeadInDatabase( &( (*In_WorldVector)[Temp_Position.PosY][Temp_Position.PosX][Temp_Position.PosZ] ), Temp_Position );
            }
        }
    }
    cout << "trying to unlock mutex" << endl;
    FilesystemOperation.unlock();
    cout << "unlocked" << endl;

    cout << getTimestampInMiliseconds() << " leaving database::writeWorld()" << endl;

}




//! make sure path exists
void database::writeCubestack( vector<cube*>* In_CubeVector, string In_ExistingPath ){
    cout << getTimestampInMiliseconds() << " entering database::writeCubestack() at " << In_ExistingPath << endl;

    //! create files with info about cube
    for( size_t Index = 0; Index < In_CubeVector->size(); Index++ ){    //all the files in the cube-stack
        cube* Temp_Cube = (*In_CubeVector)[Index];
        string Temp_Path = In_ExistingPath + "/" + to_string(Index) ;
        writeCube( Temp_Cube, Temp_Path );
    }
    cout << getTimestampInMiliseconds() << " leaving database::writeCubestack()" << endl;
}

void database::writeCube( cube* In_Cube, string In_Path ){

    ofstream File( In_Path );

        string Energy = to_string( In_Cube->getEnergy() );

        string IsMassive = "false";
        if( In_Cube->isMassive() ){
            IsMassive = "true";
        }

        /*
        string canBlockSight = "false";
        if( In_Cube->isFlat() ){
            canBlockSight = "true";
        }
        */

        string KindOfCube = In_Cube->getStringOfCubeType();

        string IDofAccountSittingAtThisCube = to_string( ((character*)In_Cube)->getIDofAccountThisCharacterBelongsTo() );

        File << "Energy " << Energy << endl;
        File << "isMassive " << IsMassive << endl;
        //File << "canBlockSight " << canBlockSight << endl;
        File << "KindOfCube " << KindOfCube << endl;
        File << "IDofAccountSittingAtThisCube " << IDofAccountSittingAtThisCube << endl;


}




void database::writeFileWithInts( vector<int>* In_IntVector, string In_ExistingPath, string In_Filename ){
    string Temp_CompletePath = "touch " + In_ExistingPath + In_Filename;
    system( Temp_CompletePath.c_str() );
    for( size_t Index = 0; Index < In_IntVector->size(); Index++ ){
        system( ("echo '" + to_string( ( (*In_IntVector)[Index] ) ) + "' >> " + In_ExistingPath + In_Filename).c_str() );
    }
}


void database::writeHeadInDatabase( head* In_Head, position In_Position ){

    cout << getTimestampInMiliseconds() << " entering database::writeHeadInDatabase()" << endl;
    /*
    cout << "trying to lock mutex" << endl;
    FilesystemOperation.lock();
    cout << "locked" << endl;
    */

    string Pathname = "./database/world/" + to_string(In_Position.PosY) + "/" + to_string(In_Position.PosX) + "/" + to_string(In_Position.PosZ);

    //!checking, if folders for that position already exist
    string Position_Y = to_string(In_Position.PosY);
    string String_Foldername_Y = "./database/world/" + Position_Y;
    const char* char_Foldername_Y = String_Foldername_Y.c_str();
    if(  directoryExists(char_Foldername_Y) == false ){   //doesn't exist -> create it now
        mkdir(char_Foldername_Y, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        while( directoryExists(char_Foldername_Y) == false ){
            cout << "waiting for OS to create String_Foldername_Y: " << String_Foldername_Y << endl;
            system("sleep 1");
        }
    }

    string Position_X = to_string(In_Position.PosX);
    string String_Foldername_X = String_Foldername_Y + "/" + Position_X;
    const char* char_Foldername_X = String_Foldername_X.c_str();
    if( directoryExists(char_Foldername_X) == false ){   //doesn't exist -> create it now
        mkdir(char_Foldername_X, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        while( directoryExists(char_Foldername_X) == false ){
            cout << "waiting for OS to create String_Foldername_X:" << String_Foldername_X << endl;
            system("sleep 1");
        }
    }

    string Position_Z = to_string(In_Position.PosZ);
    string String_Foldername_Z = String_Foldername_X + "/" + Position_Z;
    const char* char_Foldername_Z = String_Foldername_Z.c_str();
    if( directoryExists(char_Foldername_Z) == false ){
        mkdir( char_Foldername_Z, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        while( directoryExists(char_Foldername_Z) == false ){
            cout << "waiting for OS to create String_Foldername_Z:" << String_Foldername_Z << endl;
            system("sleep 1");
        }
    }


    string Temp_Path_Cubestack = String_Foldername_Z + "/cubestack/";
    if( directoryExists( Temp_Path_Cubestack.c_str() ) == false ){
        mkdir( Temp_Path_Cubestack.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        while( directoryExists( Temp_Path_Cubestack.c_str() ) == false ){
            cout << "waiting for OS to create Temp_Path_Cubestack:" << Temp_Path_Cubestack << endl;
            system("sleep 1");
        }
    }
    writeCubestack( &(In_Head->CubeStack), Temp_Path_Cubestack );

    writeFileWithInts( &( In_Head->AccountsCurrentlySeeThisPosition ), String_Foldername_Z + "/", "AccountsSeeThisPosition" );

    /*
    cout << "trying to unlock mutex" << endl;
    FilesystemOperation.unlock();
    cout << "unlocked" << endl;
    */

    cout << getTimestampInMiliseconds() << " leaving database::writeHeadInDatabase()" << endl;
}


vector<string> database::getFilesInFolder( string In_Path ){
    cout << getTimestampInMiliseconds() << " entering database::getFilesInFolder(). In_Path: " << In_Path << endl;

    vector<string> Output;


    DIR *Dir;
    struct dirent *dirp;
    if((Dir  = opendir( In_Path.c_str() )) == NULL) {
        cout << "Error(" << errno << ") opening " << In_Path << "; exit now!" << endl;
        exit(1);
    }

    while ((dirp = readdir(Dir)) != NULL) {
        Output.push_back(string(dirp->d_name));
    }
    closedir(Dir);


    //!removing the "." and ".." entery
    for( size_t Index = 0; Index < Output.size(); Index++ ){
        if( Output[Index] == "." || Output[Index] == ".." ){
            Output.erase(Output.begin()+Index);
            Index--;
        }
    }

    cout << getTimestampInMiliseconds() << " leaving database::getFilesInFolder()." << endl;
    return Output;
}


vector<string> database::getFoldersInFolder( string In_Path ){
    cout << getTimestampInMiliseconds() << " entering database::getFoldersInFolder()" << endl;

    vector<string> Output;
    DIR *Dir = opendir( In_Path.c_str() );
    struct dirent *entry = readdir( Dir );
    while( entry != NULL ){
        string FolderName = "";
        if( entry->d_type == DT_DIR ){
            FolderName = entry->d_name;
            Output.push_back( FolderName );
        }
        entry = readdir(Dir);
    }
    closedir( Dir );

    //!removing the "." and ".." entery
    for( size_t Index = 0; Index < Output.size(); Index++ ){
        if( Output[Index] == "." || Output[Index] == ".." ){
            Output.erase(Output.begin()+Index);
            Index--;
        }
    }
    cout << getTimestampInMiliseconds() << " leaving database::getFoldersInFolder()" << endl;
    return Output;
}

/*
void database::printAllEnterysInVector( vector<string> In_Vector, string In_Vectorname ){

    for( size_t Index = 0; Index < In_Vector.size(); Index++ ){
        cout << In_Vectorname << "[" << Index << "]: " << In_Vector[Index] << endl;
    }
}
*/

standardCube database::getStandardtypeCubeFromString( string In_Type ){
    cout << getTimestampInMiliseconds() << " entering database::getStandardtypeCubeFromString(): In_Type: " << In_Type << endl;


    standardCube Output;
    if(In_Type == "none"){
        Output = none;
    }else if(In_Type == "stone"){
        Output = stone;
    }else if(In_Type == "trunk"){
        Output = trunk;
    }else if(In_Type == "water"){
        Output = water;
    }else if(In_Type == "grass"){
        Output = grass;
    }else if(In_Type == "solid"){
        Output = solid;
    }else if(In_Type == "branch"){
        Output = branch;
    }else if(In_Type == "leafsAndTwigs"){
        Output = leafsAndTwigs;
    }else if(In_Type == "sand"){
        Output = sand;
    }else if(In_Type == "characterCube"){
        Output = characterCube;
    }else if(In_Type == "gravel"){
        Output = gravel;
    }else{
        cout << "unknown type of cube. exit now" << endl;
        exit(1);
    }
    cout << getTimestampInMiliseconds() << " leaving database::getStandardtypeCubeFromString()" << endl;
    return Output;
}


void database::getWorld( vector<vector<vector<head>>>* In_EmptyWorldFrame, const worldSize In_WorldSizePointerOfOutputVector ){
    cout << getTimestampInMiliseconds() << " entering database::getWorld()" << endl;


    cout << "trying to lock mutex" << endl;
    FilesystemOperation.lock();
    cout << "locked" << endl;

    vector<string> Inside_database = getFoldersInFolder("./database/");

    bool WorldFolderFound = false;
    for( size_t Index0 = 0; Index0 < Inside_database.size() && WorldFolderFound == false; Index0++ ){
        if( Inside_database[Index0] == "world" ){
            WorldFolderFound = true;
        }
    }

    int Biggest_Y_Value = -1;
    int Biggest_X_Value = -1;
    int Biggest_Z_Value = -1;
    //bool WorldFrameHasBeenGenerated = false;
    //worldSize Temp_WorldSize;

    if( WorldFolderFound ){
        vector<string> Folders_Y = getFoldersInFolder("./database/world/");

        Biggest_Y_Value = getBiggestValueInStringVector( Folders_Y );
        if( Biggest_Y_Value != In_WorldSizePointerOfOutputVector.Height - 1 ){
            cout << "invalid world-dimensons in y-direction: Temp_WorldSize.Height = " << In_WorldSizePointerOfOutputVector.Height << " but getBiggestValueInStringVector( Folders_Y ) = " << getBiggestValueInStringVector( Folders_Y ) << " ; exit now!" << endl;
            exit(1);
        }

        for( size_t Index_Y = 0; Index_Y < Folders_Y.size(); Index_Y++ ){
            position Position;
            Position.PosY = stoi( Folders_Y[Index_Y] );

            string Temp_Path_X = "./database/world/" + Folders_Y[Index_Y] + "/";
            vector<string> Folders_X = getFoldersInFolder(Temp_Path_X);

            Biggest_X_Value = getBiggestValueInStringVector( Folders_X );
            if( Biggest_X_Value != In_WorldSizePointerOfOutputVector.Width - 1 ){
                cout << "invalid world-dimensons in x-direction: Temp_WorldSize.Width = " << In_WorldSizePointerOfOutputVector.Width << " but getBiggestValueInStringVector( Folders_X ) = " << getBiggestValueInStringVector( Folders_X ) << " ; exit now!" << endl;
                exit(1);
            }

            for( size_t Index_X = 0; Index_X < Folders_X.size(); Index_X++ ){
                Position.PosX = stoi( Folders_X[Index_X] );

                string Temp_Path_Z = Temp_Path_X + Folders_X[Index_X] + "/";
                vector<string> Folders_Z = getFoldersInFolder( Temp_Path_Z );

                Biggest_Z_Value = getBiggestValueInStringVector( Folders_Z );

                if( Biggest_Z_Value != In_WorldSizePointerOfOutputVector.MaxLevelOverZero - 1 ){
                    cout << "invalid world-dimensons in z-direction: In_WorldSizePointerOfOutputVector->MaxLevelOverZero = " << In_WorldSizePointerOfOutputVector.MaxLevelOverZero << " but getBiggestValueInStringVector( Folders_Z ) = " << getBiggestValueInStringVector( Folders_Z ) << " ; exit now!" << endl;
                    exit(1);
                }
                for( size_t Index_Z = 0; Index_Z < Folders_Z.size(); Index_Z++ ){


                    //!loading cubes
                    Position.PosZ = stoi(Folders_Z[Index_Z]);
                    string Temp_PathToCubes = Temp_Path_Z + Folders_Z[Index_Z] + "/cubestack/";
                    vector<string> StringsOfCubes = getFilesInFolder(Temp_PathToCubes);
                    head Temp_Head;
                    for( size_t Index_Cube = 0; Index_Cube < StringsOfCubes.size(); Index_Cube++ ){
                        //!generate cubes and add them to stack


                        vector<string> LinesInFile = getLinesInFile( Temp_PathToCubes + "/" + StringsOfCubes[Index_Cube] );

                        //!Temp_Head.CubeStack.push_back( Temp_Cube );
                        Temp_Head.CubeStack.push_back( getCubeOutOfStrings( LinesInFile ) );

                        cout << "loading head on Position " << Position << endl;
                        //vector< vector< vector< head > > >* Temp_WorldVectorPointer = (In_DummyWorld->getPointerToWorldVector());
                        (*In_EmptyWorldFrame)[Position.PosY][Position.PosX][Position.PosZ] = ( Temp_Head );

                        //!loading list of Accounts who see this cubestack
                        string Temp_PathToListWithIDs = Temp_Path_Z + Folders_Z[Index_Z] + "/AccountsSeeThisPosition";
                        vector<string> Lines;
                        string LineInFile = "";
                        ifstream File(Temp_PathToListWithIDs);
                        while( getline(File, LineInFile) ){
                            Lines.push_back(LineInFile);
                        }
                        for( size_t Index = 0; Index < Lines.size(); Index++ ){
                            int Temp_ID = stoi(Lines[Index]);
                            ((*In_EmptyWorldFrame)[Position.PosY][Position.PosX][Position.PosZ]).AccountsCurrentlySeeThisPosition.push_back(Temp_ID);
                        }

                    }
                }
            }
        }
    }else{
        cout << "corrupt database. 'world'-folder is missing. exit now!" << endl;
        exit(1);
    }

    cout << "trying to unlock mutex" << endl;
    FilesystemOperation.unlock();
    cout << "unlocked" << endl;
    cout << getTimestampInMiliseconds() << " leaving database::loadWorld()" << endl;
}



bool database::directoryExists( const char* In_Path ){
    bool Output = false;
    if( In_Path == NULL){
        Output =  false;
    }else{
        DIR *pDir;

        pDir = opendir (In_Path);

        if(pDir != NULL)        {
            Output = true;
            (void) closedir (pDir);
        }
    }
    return Output;
}

string database::getTimestampInMiliseconds(){

    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    string Output = to_string(ms);
    Output.erase( 0, 4 );

    return Output;
}



int database::getBiggestValueInStringVector( vector<string> In_Vector ){
    int Output = -1;

    for( size_t Index = 0; Index < In_Vector.size(); Index++ ){
        int Value = stoi( In_Vector[Index] );
        if( Value > Output ){
            Output = Value;
        }
    }

    return Output;

}


void database::loadWorldSizeInformation( worldSize* In_WorldSize ){
    cout << "entering database::loadWorldSizeInformation()" << endl;

    cout << "trying to lock mutex" << endl;
    FilesystemOperation.lock();
    cout << "locked" << endl;

    //!reading worldSize-Information from file:
    vector<string> Lines_WorldSize_File;
    string LineInFile = "";
    ifstream WorldSizeFile("./database/WorldSize");
    while( getline(WorldSizeFile, LineInFile) ){
        Lines_WorldSize_File.push_back(LineInFile);
    }
    if( Lines_WorldSize_File.size() != 3 ){
        cout << "currupt WorldSize-file. has size " << Lines_WorldSize_File.size() << endl;
        cout << "exit now!" << endl;
        exit(1);
    }
    In_WorldSize->Height = stoi(Lines_WorldSize_File[0]);
    In_WorldSize->Width = stoi(Lines_WorldSize_File[1]);
    In_WorldSize->MaxLevelOverZero = stoi(Lines_WorldSize_File[2]);

    cout << "trying to unlock mutex" << endl;
    FilesystemOperation.unlock();
    cout << "unlocked" << endl;

    cout << "leaving database::loadWorldSizeInformation()" << endl;
}


/*!
void database::replaceCubeWithCharacterCube( cube* In_Cube, position In_Position ){
    cout << getTimestampInMiliseconds() << " entering database::replaceCubeWithCharacterCube(). In_Position: " << In_Position << endl;

    cout << "trying to lock mutex" << endl;
    FilesystemOperation.lock();
    cout << "locked" << endl;

    if( In_Cube->getTypeOfCube() != characterCube ){
        cout << "In_Cube has not type 'characterCube'. exit now!" << endl;
        exit(1);
    }
    string PathToCubestack = "./database/" + to_string(In_Position.PosY) + "/" + to_string(In_Position.PosX) + "/" + to_string(In_Position.PosZ) + "/cubestack/";
    string PathToCube = PathToCubestack + "0";


    #if VALIDATE_DATABASE_BEFORE_OVERWRITING_CUBE
    //! in the first part we just check for a valid database in this point.
    //! normally this should be already the case (if we keep the database always syncron with the in-memory-world
    //! maybe i remove that later
    cout << "validating database" << endl;
    vector<string> FilesInCubestackFolder = getFilesInFolder( PathToCubestack );
    if( FilesInCubestackFolder.size() != 1 ){
        cout << "there number of files at this position in database is expected to be exactly 1. but is " << FilesInCubestackFolder.size()  << ". exit now!" << endl;
        exit(1);
    }
    // now we are sure, ther is only one cube
    cube* OldCube = getCubeOutOfStrings( getLinesInFile(PathToCube) );
    if( OldCube->getTypeOfCube() != none ){
        cout << "the type of the old cube in database (which should be overwritten) is not from type 'none'. exit now!" << endl;
        exit(1);
    }
    cout << "is valid on this position" << endl;
    #endif // VALIDATE_DATABASE_BEFORE_OVERWRITING_CUBE


    //! ok, now we are sure, the database is valid. as said: the part above could be skipped, but i leave it here for the moment
    cout << "deleting old cube" << endl;
    system( ( "rm " + PathToCube ).c_str() );
    cout << "writing new" << endl;
    writeCube( In_Cube, PathToCube );

    cout << "trying to unlock mutex" << endl;
    FilesystemOperation.unlock();
    cout << "unlocked" << endl;


    cout << getTimestampInMiliseconds() << " leaving database::replaceCubeWithCharacterCube() " << endl;
}
*/


cube* database::getCubeOutOfStrings( vector<string> In_Lines ){
    cout << getTimestampInMiliseconds() << " entering database:.getCubeOutOfStrings()" << endl;

    map<string, string> AttributeToValue;
    for( size_t Index_Line = 0; Index_Line < In_Lines.size(); Index_Line++ ){
        istringstream StringStream(In_Lines[Index_Line]);
        vector<string> Words{
            istream_iterator<string>{StringStream}, istream_iterator<string>{}
        };
        AttributeToValue[Words[0]] = Words[1];
    }
    string Temp_energy_string = "Energy";
    int Temp_Energy = stoi( AttributeToValue[Temp_energy_string] );
    bool Temp_IsFlat = false;
    if( AttributeToValue["isFlat"] == "false" ){
        Temp_IsFlat = false;
    }else if( AttributeToValue["isFlat"] == "true" ){
        Temp_IsFlat = true;
    }else{
        cout << "isFlat: something went wrong. exit now!" << endl;
        exit(1);
    }
    bool Temp_CanBlockSight = false;
    if( AttributeToValue["canBlockSight"] == "false" ){
        Temp_CanBlockSight = false;
    }else if( AttributeToValue["canBlockSight"] == "true" ){
        Temp_CanBlockSight = true;
    }else{
        cout << "canBlockSight: something went wrong. Temp_CanBlockSight is not 'true' or 'false'. exit now!" << endl;
        exit(1);
    }
    standardCube Temp_Type = getStandardtypeCubeFromString( AttributeToValue["KindOfCube"] );
    int Temp_IDofAccountSittingAtThisCube = stoi( AttributeToValue["IDofAccountSittingAtThisCube"] );



    cube* Output_Cube = new cube( Temp_Energy, Temp_IsFlat/*!, Temp_CanBlockSight*/, Temp_Type/*!, Temp_IDofAccountSittingAtThisCube*/ );


    cout << getTimestampInMiliseconds() << " leaving database:.getCubeOutOfStrings()" << endl;

    return Output_Cube;
}




vector<string> database::getLinesInFile( string In_Path ){

    vector<string> LinesInFile;
    string Line = "";
    ifstream File( In_Path );
    while( getline(File, Line) ){
        LinesInFile.push_back(Line);
    }

    return LinesInFile;

}



/*!
void database::addCharacterCubeToCubestack( cube* In_Cube, position In_Position ){
    cout << getTimestampInMiliseconds() << " entering database::addCharacterCubeToCubestack(). In_Position: " << In_Position << endl;

    cout << "trying to lock mutex" << endl;
    FilesystemOperation.lock();
    cout << "locked" << endl;

    string PathToCubestack = "./database/world/" + to_string(In_Position.PosY) + "/" + to_string(In_Position.PosX) + "/" + to_string(In_Position.PosZ) + "/cubestack/" ;

    vector<string> FilesInCubestack = getFilesInFolder( PathToCubestack );

    #if VALIDATE_DATABASE_BEFORE_OVERWRITING_CUBE
    int BiggestNumberInCubestack = -1;
    for( size_t Index = 0; Index < FilesInCubestack.size(); Index++ ){
        if( stoi(FilesInCubestack[Index]) >= BiggestNumberInCubestack ){
            BiggestNumberInCubestack = stoi(FilesInCubestack[Index]);
        }
    }
    if( BiggestNumberInCubestack != (FilesInCubestack.size() - 1) ){
        cout << "invalid database at Position: " << In_Position << "; exit now!" << endl;
        exit(1);
    }
    #endif // VALIDATE_DATABASE_BEFORE_OVERWRITING_CUBE



    string PathToNewCube = PathToCubestack + to_string(FilesInCubestack.size());
    string Command = "touch " + PathToCubestack;
    cout << "Command: " << Command << endl;
    system( Command.c_str() );


    writeCube(In_Cube, PathToNewCube);

    cout << "trying to unlock mutex" << endl;
    FilesystemOperation.unlock();
    cout << "unlocked" << endl;
    cout << getTimestampInMiliseconds() << " leaving database::addCharacterCubeToCubestack(). In_Position: " << In_Position << endl;
}
*/



/*!
void database::removeCharacterCubeFromCubestack( position In_Position ){
    cout << getTimestampInMiliseconds() << " entering database::removeCharacterCubeFromCubestack(): In_Position " << In_Position << endl;

    cout << "trying to lock mutex" << endl;
    FilesystemOperation.lock();
    cout << "locked" << endl;
    string PathToCubestack = "./database/world/" + to_string(In_Position.PosY) + "/" + to_string(In_Position.PosX) + "/" + to_string(In_Position.PosZ) + "/cubestack/" ;
    vector<string> FilesInCubestack = getFilesInFolder( PathToCubestack );

    #if VALIDATE_DATABASE_BEFORE_OVERWRITING_CUBE
    //!there can only be one characterCube in a Cubestack
    cout << "validating database" << endl;
    bool CharacterCubeHasBeenFound = false;
    for( size_t FileIndex = 0; FileIndex < FilesInCubestack.size(); FileIndex++ ){

        string PathToCube = PathToCubestack + to_string(FileIndex);
        vector<string> LinesInFile = getLinesInFile(PathToCube);
        if( getTypeOfCubeFileStrings( LinesInFile ) == characterCube ){

            if( CharacterCubeHasBeenFound ){
                cout << "invalid database. more then one CharacterCube has been found. exit now!" << endl;
                exit(1);
            }else{
                CharacterCubeHasBeenFound = true;
            }
        }
    }
    cout << "is valid at this position" << endl;
    #endif // VALIDATE_DATABASE_BEFORE_OVERWRITING_CUBE

    //!now the actuall removing
    int IndexOfToRemoveCube = -1;
    for( size_t FileIndex = 0; FileIndex < FilesInCubestack.size(); FileIndex++ ){
        string PathToCube = PathToCubestack + "/" + to_string(FileIndex);
        if( getTypeOfCubeFileStrings(getLinesInFile(PathToCube)) == characterCube ){
            IndexOfToRemoveCube = FileIndex;
        }
    }
    if( IndexOfToRemoveCube != -1 ){
        string PathToRemoveCube = PathToCubestack + "/" + to_string(IndexOfToRemoveCube);
        string Command = "rm " + PathToRemoveCube;
        system( Command.c_str() );
    }else{
        cout << "no characterCube has been found to remove. invalid database? exit now!" << endl;
        exit(1);
    }

    cout << "trying to unlock mutex" << endl;
    FilesystemOperation.unlock();
    cout << "unlocked" << endl;
    cout << getTimestampInMiliseconds() << " leaving database::removeCharacterCubeFromCubestack()" << endl;
}
*/


standardCube database::getTypeOfCubeFileStrings( vector<string> In_Lines ){
    cout << getTimestampInMiliseconds() << " entering database::getTypeOfCubeFile()" << endl;

    map<string, string> AttributeToValue;
    for( size_t Index_Line = 0; Index_Line < In_Lines.size(); Index_Line++ ){
        istringstream StringStream(In_Lines[Index_Line]);
        vector<string> Words{
            istream_iterator<string>{StringStream}, istream_iterator<string>{}
        };
        AttributeToValue[Words[0]] = Words[1];
    }

    cout << getTimestampInMiliseconds() << " leaving database::getTypeOfCubeFile()" << endl;
    cout << "AttributeToValue[KindOfCube]: " << AttributeToValue["KindOfCube"] << endl;
    return getStandardtypeCubeFromString( AttributeToValue["KindOfCube"] );
}



void database::sync( position In_Position, const head* In_NewHead ){

//!missing all
}
