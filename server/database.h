#ifndef DATABASE_H
#define DATABASE_H

#include <mutex>
#include <vector>

#include "structs.h"
#include "position.h"
#include "head.h"
#include "config.h"

using namespace std;

class database{


    public:
        database();
        ~database();

        //functions
        //! all this public functions must use the FilesystemOperation-mutex!!!
        void writeWorld( vector<vector<vector<head>>>* In_WorldVector, worldSize In_WorldSize );
        void getWorld( vector<vector<vector<head>>>* In_EmptyWorldFrame, const worldSize In_WorldSizePointerOfOutputVector );
        void loadWorldSizeInformation( worldSize* In_WorldSize );

        //!void replaceCubeWithCharacterCube( cube* In_Cube, position In_Position );
        //!void addCharacterCubeToCubestack( cube* In_Cube, position In_Position );
        //!void removeCharacterCubeFromCubestack( position In_Position );

        void sync( position In_Position, const head* In_NewHead );

    protected:

    private:
        mutex FilesystemOperation;

        //functions
        bool isThereAnyDatabase();
        bool initDatabasse();
        string getTimestampInMiliseconds();
        bool directoryExists( const char* pzPath );
        void writeHeadInDatabase( head* In_Head, position In_Position );
        vector<string> getFilesInFolder( string In_Path );
        vector<string> getFoldersInFolder( string In_Path );
        standardCube getStandardtypeCubeFromString( string In_Type );
        int getBiggestValueInStringVector( vector<string> In_Vector );
        void writeCubestack( vector<cube*>* In_CubeVector, string In_ExistingPath );
        void writeCube( cube* In_Cube, string In_Path );
        void writeFileWithInts( vector<int>* In_IntVector, string In_ExistingPath, string In_Filename );
        cube* getCubeOutOfStrings( vector<string> In_Lines );
        vector<string> getLinesInFile( string In_Path );
        standardCube getTypeOfCubeFileStrings( vector<string> In_Lines );

        //!vector<vector<vector<head>>> generateEmptyVectorFramesWithoutHeads( const worldSize* In_WorldSize );


};




#endif // DATABASE_H
