#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <mutex>
#include <sys/timeb.h>          //needed for timestamp in log-file
#include <fstream>              //this aswell
#include <algorithm>            //needed for removing '\n' in json-serial       https://stackoverflow.com/questions/1488775/c-remove-new-line-from-multiline-string
#include <sys/time.h>           //needed for timestamp

#include <../include/cereal/archives/json.hpp>
#include <../include/cereal/types/vector.hpp>      //stuff for json. we might not need all of them.


#include <signal.h>     //might be needed for try-catch stuff


#include "client.h"




using namespace std;




client::client(int64_t In_NewSocketFileDescriptor, int64_t In_ID){



    ClientID = In_ID;
    SocketFileDescriptor = In_NewSocketFileDescriptor;  //that sets the member-variable


    //!why did i do that?
    pfd.fd = SocketFileDescriptor;
    pfd.events = POLLIN | POLLHUP | POLLRDNORM;
    pfd.revents = 0;


    cout << getTimestampInMiliseconds() << " client connected! ID: " << ClientID << endl;
    ClientIsConnected = true;

}

client::~client(){

}


string client::waitForMessageFromClient( UserAccount *In_PointerUserAccount ){           //get called by startThreadForWaitingForMessages()

    cout << getTimestampInMiliseconds() << " entering client::waitForMessageFromClient() " << endl;

    string Output;

    int64_t SuccesfullyTransmitted = 0;               //return value for the read() and write() calls; i.e. it contains the number of characters read or written.
    char ReceivedMessage_char[200000];    //The server reads characters from the socket connection into this buffer.

    bzero(ReceivedMessage_char,200000);       //initializes the buffer using the bzero() function,

    char EmptyMessageForCompare[200000];                   //this is needed some lines below to check, if the reading was successful and


    strcpy(EmptyMessageForCompare, ReceivedMessage_char);   //if i do this instead, i get the same hickup also on x86.


    if(ClientIsConnected){
        //Debug_IDofClient = ClientID;
        cout << getTimestampInMiliseconds() << " client::waitForMessageFromClient(): trying to read from client " << ClientID << " ; ClientIsConnected: " << ClientIsConnected << " (1=true; 0=false)" << endl;

        //bool Debug_SocketFileDescriptor = SocketFileDescriptor;
        //Debug_IDofClient = ClientID;
        try{
            SuccesfullyTransmitted = read(SocketFileDescriptor ,ReceivedMessage_char, 199999);        //!!!! got a BIG problem here. in this commit, the SocketFileDescriptor gets overwritten by something right after reading from the socket. but it seems, that this problem only accures when more then one client is connected... i guess it's because of unvalid pointer-handling of the character-stuff
        }catch(...){
            cout << getTimestampInMiliseconds() << " waitForMessageFromClient(): catching at reading from Socket" << endl;
            markClientAsDissconnected();
        }                                                                                                                     //there is something for it to read in the socket, i.e. after
                                                                                                                                //the client has executed a write(). It will read either the
                                                                                                                                //total number of characters in the socket or 255, whichever
                                                                                                                                //is less, and return the number of characters read. The read()
                                                                                                                                //man page has more information.



        //!in case the client hasn't answered for a long time the heartbleed, and then weaks up again, i need to catch that and exit the thread
        /*
        if( ClientIsConnected == false ){
            cout << "client::waitForMessageFromClient(): client sent request but is already marked as offline. exit thread now!" << endl;
            //terminate();
            exit(0);
        }
        */




        cout << getTimestampInMiliseconds() << " client::waitForMessageFromClient(): Client: " << ClientID << " ; Successfullytransmitted: " << SuccesfullyTransmitted << " ; ReceivedMessage: " << ReceivedMessage_char << endl;
    }


    //!Debug
    timeb Tb;
    ftime (&Tb);
    int64_t TimeStamp = Tb.millitm + (Tb.time & 0xfffff) * 1000;
    fstream Fstream;                          //this writes the waitForMessageFromClient.log
    Fstream.open("waitForMessageFromClient.log", ios::out|ios::app );
    Fstream << "[" <<to_string(TimeStamp) << "]: " << ReceivedMessage_char << endl;
    Fstream.close();

    if(strcmp(EmptyMessageForCompare, ReceivedMessage_char) == 0 && ClientIsConnected){                          //i did this an the if(ClientIsConnected) above to finnally get rid of the problem, that if the first connected client disconnected made the server crash
        if( AccountID != -1 ){  //if has been already attached to some account
            In_PointerUserAccount->setIDofClientAttachedWithThisAccount(-1);
        }
        markClientAsDissconnected();

        cout << getTimestampInMiliseconds() << " client::waitForMessageFromClient(): reading an empty Message from Client " <<  ClientID << " . marking it as dissconnected!" << endl;
    }

    //Debug_online = ClientIsConnected;
    if(SuccesfullyTransmitted == 0 && ClientIsConnected ){
        markClientAsDissconnected();
        cout << getTimestampInMiliseconds() << " Client "<< ClientID << ": waitForMsg(): Client " << ClientID << " disconnected. set ClientIsConnected = 'false'" << endl;
    }

    //!Debug
    //  cout << "receivedMsg: " << ReceivedMessage_char << endl;


    if (SuccesfullyTransmitted < 0 && ClientIsConnected){                                        // so i copy it to this place but without the exit(1)
        cout << getTimestampInMiliseconds() << " client::waitForMsg(): ERROR reading from socket" << endl;
        markClientAsDissconnected();
     //   exit(1);
    }


    Output = string(ReceivedMessage_char);

    cout << getTimestampInMiliseconds() << " leaving client::waitForMessageFromClient()" << endl;

    return Output;

}


int64_t client::getIDOfClient(){
    return ClientID;
}

void client::aNewChatMessageArrived(string In_NewMsg){
    In_NewMsg = "ChatMessage-----;" + In_NewMsg;

    sendMessageToClient(In_NewMsg);
}

void client::sendMessageToClient(string In_Msg_str){

    In_Msg_str = "#" + In_Msg_str + ";end";



    //this routine should take a string and write it to the socket,
    //so the client on the other side can read it.

    //!Debug
 //   usleep(1000);  //whith that, i just want to quick and dirty get rid of the problem, that the client meshes up the received messages. but that doesn't seem to be the reason for the server/client to crash

    //!Debug
    timeb Tb;
    ftime (&Tb);
    int64_t TimeStamp = Tb.millitm + (Tb.time & 0xfffff) * 1000;
    fstream Fstream;                          //this writes the sendMessageToClient.log
    Fstream.open("sendMessageToClient.log", ios::out|ios::app );
    Fstream << "[" <<to_string(TimeStamp) << "]: " << In_Msg_str << endl;
    Fstream.close();



    //!Debug
    //int64_t Debug_IDofClient = ClientID;

    char ToSendMessage_char[200000];

    bzero(ToSendMessage_char, 200000);


    strcpy( ToSendMessage_char, In_Msg_str.c_str() );

//    bool Debug_ClientIsConnected = ClientIsConnected;

    int64_t SuccessfullyWritten = -1;

    //int64_t Debug_SocketFileDescriptor = SocketFileDescriptor;

    try{          //well lets try to catch the case, that the client has disconnected

        if(ClientIsConnected){                  //i guess i need to check that here again, because the status of the client can has changed in the meantime.
            SuccessfullyWritten = write(        //!Debug this is crashing when the first client is disconnecting
                                                SocketFileDescriptor,
                                                ToSendMessage_char,
                                                strlen(ToSendMessage_char)
                                                                        );

            //!Debug
            cout << getTimestampInMiliseconds() << " client::sendMessageToClient(): Debug: SuccessfullyWritten = " << SuccessfullyWritten << endl;
            //!
        }
    }catch(...){
        markClientAsDissconnected();
        cout << getTimestampInMiliseconds() << " Client " << ClientID << ": 'catch'ing writing error! set ClientIsConnected to 'false'!" << endl;
    }

    if (SuccessfullyWritten < 0 && ClientIsConnected  ){   //error-handling
        cout << "\nERROR writing to socket, somethin went veeeeerrrrrryyy wrong! ; variable 'SuccessfullyWritten' = " << SuccessfullyWritten << " ; set ClientIsConnected = 'false'" << endl;
        markClientAsDissconnected();
    }

    #if DEBUG_GENERAL_INFORMATION
        cout << getTimestampInMiliseconds() << " end of sendMessageToClient()." << endl;
    #endif // DEBUG_GENERAL_INFORMATION

}


bool client::getIsClientStillConnected(){
    return ClientIsConnected;
}

void client::askForHeartbleed(/*int64_t In_Heartbleed*/){
    if( WaitingForHeartbleed == false ){
        string Heartbleed_string = "empty";

        string Temp = "foo";                        //need something after the ";" otherwise it would crash the client

        Heartbleed_string = "Heartbleed------;"  + Temp  ;


        cout << getTimestampInMiliseconds() << " Client " << ClientID << "; Heartbleed_string: " << Heartbleed_string << endl;


        sendMessageToClient(Heartbleed_string);
        WaitingForHeartbleed = true;
    }else{
        cout << getTimestampInMiliseconds() << " client::askForHeartbleed(): client " << ClientID << " didn't answer heartbleed. disconnecting..." << endl;
        markClientAsDissconnected();
    }

}




void client::sendOrderToTurnHeartIntoBlue(){
    sendMessageToClient("Heartbleed2Blue-;foo");
}



void client::updatePosition(position In_NewPosition){

    string ToSendMessage = "UpdatePosition--;PosY" + to_string(In_NewPosition.PosY) + "*PosX" + to_string(In_NewPosition.PosX) + "*PosZ" + to_string(In_NewPosition.PosZ);
    sendMessageToClient(ToSendMessage);
}

#if UNLOCKWRITINGMUTEX
void client::sendOrderToUnlockMutexForWritingSocket(){
    string ToSendMessage = "UnlockWritingMtx;foo";
    sendMessageToClient(ToSendMessage);
    cout << getTimestampInMiliseconds() << " Client " << ClientID << ": sent Message to Unlock MutexForWritingSocket!" << endl;
}
#endif // UNLOCKWRITINGMUTEX


void client::sendInformationOfUnsuccessfullLogin( int64_t In_IndexOfTheRequestedAccountInVector ){     //called from sController::interpretReceivedMessage()

    if( In_IndexOfTheRequestedAccountInVector == -1 ){
        sendMessageToClient("LoginFailed-----;AccountDoesntExist");
        cout << getTimestampInMiliseconds() << " client::sendInformationOfUnsuccessfullLogin(): Requested Account Doesn't exist. Client: " << ClientID << endl;
    }else{
        sendMessageToClient("LoginFailed-----;AccountAlreadyLoggedIn");
        cout << getTimestampInMiliseconds() << " client::sendIformationOfUnsuccessfullLogin(): Requested Account is already logged in. Client " << ClientID << " ; Requested Account: " << In_IndexOfTheRequestedAccountInVector << endl;
    }
}



void client::tellClientUserAccountNameIsAlreadyInUse(){
    sendMessageToClient("NameAlreadyInUse;foo");
}


void client::markClientAsDissconnected(){

    ClientIsConnected = false;

    //!#ifdef USE_COMPLICATED_VOODOO
    //!cleaning up...
    cout << "cleaning up ListOfPositionsThisAccountSawInThisSession" << endl;
    for( const auto &PointerToHead : ListOfPositionsThisAccountSawInThisSession ){

        if( AccountID == -1 ){
            cout << "AccountID has been set to -1 before cleaning up" << endl;
            exit(1);
        }

        (*PointerToHead).ClientsSawThisPositionInThisSession.erase(ClientID);
        //cout << "deleted" << endl;
    }
    //!#endif // USE_COMPLICATED_VOODOO


    cout << getTimestampInMiliseconds() << " client::markClientAsDissconnected(): Client " << ClientID << " . marked as dissconnected" << endl;


}


int64_t client::getAccountID(){
    return AccountID;
}

void client::setAccountID(int64_t In_NewAccountID){
    AccountID = In_NewAccountID;
}

void client::unsetAccountID(){
    AccountID = -1;
}

void client::ClientAnsweredHeartbleed(){
    WaitingForHeartbleed = false;
}


string client::getTimestampInMiliseconds(){

    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;

    string Output = to_string(ms);
    Output.erase( 0, 4 );

    return Output;
}


bool client::return_ClientIsConnected(){
    return ClientIsConnected;
}



void client::setHeartbleedIsAlreadyStarted(){

    HeartbleedIsAlreadyStarted = true;

}




bool client::getHeartbleedIsAlreadyStarted(){
    return HeartbleedIsAlreadyStarted;
}




//!#ifdef USE_COMPLICATED_VOODOO
void client::addIDto_ListOfPositionsThisAccountSawInThisSession( head* In_Head ){
    ListOfPositionsThisAccountSawInThisSession.push_back( In_Head );
}
//!#endif // USE_COMPLICATED_VOODOO
