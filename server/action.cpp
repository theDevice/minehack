#include "action.h"

using namespace std;

action::action(){
    //ctor
}

action::~action(){
    //dtor
}

/*
action::action(const action& other){
    //copy ctor
}
*/

action& action::operator=(const action& rhs){
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}
