#ifndef CHARACTER_H
#define CHARACTER_H

using namespace std;

#include <map>
#include <string>

#include "attack.h"
#include "crafting.h"
#include "weapon.h"
#include "config.h"
#include "structs.h"
#include "position.h"

class character{

    public:
        character( int In_DefaultSightRadiusForCharacter, int In_IDofAccountThisCharacterBelongsTo );
        //character( const character &In_Character );      //copy-constructor
        ~character();

        //overloaded assignment:
        //character& operator=(const character& In_Source);

        position getPosition();
        void setPosition( position In_Pos );
        bool tryToMove( direction In_Direction );
        string getCharactername();
        void setCharactername( string In_CharacterName );
        void updateSightVector( vector < vector < vector <cube*>* > > In_NewSightVector );       //this function updates the Sight-Vector with all positions, the character is able to see right now.
        int64_t getSightRadius();
        void setSightRadius( int In_SightRadius );
        vector < pair<position, cube*> > getSightField();          //two dimensional becuase, we just need the positions. and the second dimenson is the stack-dimension
                                                                        // and organized in shells -> thre-dimensional
        vector < pair<position, cube*> >* getSightFieldPointer();
        bool isCharacterAbleToMove();
        string getTimestampInMiliseconds();
        float getRangeOfPrimaryWeapon();
        float getRangeOfSecondaryWeapon();
        void receiveDamage(int In_DamagePoints);
        int getHitpointsOfPrimaryWeapon();
        int getHitpointsOfSecondaryWeapon();
        int getEnergy();
    //    void setBareFistsAsPrimaryWeapon();
        void unequipPrimaryWeapon();
        void unequipSecondaryWeapon();
        string getStringOfPosition();
        void setZeroGravity( bool In_NewValue );
        bool getZeroGravity();
        int* getPointerTo_Debug_Counter_IDsAddedToCubes();
        bool isPositionInSightRadius( position In_Position );
        int getIDofAccountThisCharacterBelongsTo();

    private:
        /*
        int Debug_Counter_IDsAddedToCubes = 0;        //this Counter gets incremented every time, a ID has been added to a cube, to indicate, that a character can see the cube. and gets decremented after the id gots removed.
                                                        // i do that to check whether the number is not increasing over everything normal (and that would be strange)
        */

        position Position;
        int Energy = 100;                   // -> Healthpoints
        string Charactername;
        bool HasZeroGravity = false;

        int IDofAccountThisCharacterBelonsTo = -1;

        //!vector < vector < vector <cube*>* > > SightField; // contains all cubes (with positions), the character can currently see
                                                            // and its also organized in shells, means, sorted by its largest dimension
//actually, do i really need a sightField as a member variable= isn't it enough to calculate and send it?
// especially, when i switch to differential sightField calculation anyways
// ... hm. ok, i leave it valid, but won't use it any more but in buildSightFieldFromScratchAndUpdatePosition()
// by that it will not need to re-allocate all the time
//! 17.04.2016: once again, i thought, i don't need it here, but its better like that. of course it is possible to just return the sightField in a function
//! and then put it into another function. but then it must be passed by value. (or must be allocated all the time).
//! and with this current solution, it has its fix memory-space in the character, where it can be motified and the pointer passed
//! to everything and all good..

        //!well obviously i need the sightField at some point. but does it really need to be that complicated?
        //!lets try it with a simple vector to cube-pointers...
        vector < pair<position, cube*> > SightField;


        int64_t SightRadius = -1;            //will be set in constructor
        int Level = 0;
        bool IsCharacterAbleToMove = true;
        int Armor = 1;                      //the received Damage get divided by armor

        vector <item*> Inventory;
        int MaxSizeOfInventory = 30;

        // items on body
        weapon BareFists = weapon( "Bare Fists", 1.5, 0, 3 );

        attack PrimaryAttack;
        attack SecondaryAttack;


   //     weapon* UsedWeaponForPrimaryAttack = NULL;
   //     weapon* UsedWeaponForSecondaryAttack = NULL;

        //actions:
        crafting ChopThatWood = crafting("ChopThatWood");

};









#endif // CHARACTER_H
