

from threading import Thread

import view
import uplink
import character
import cube
import audio
import subprocess

import json
import array

import os, platform
import curses
import time     #needed for sleep()
#from thread import start_new_thread
from threading import Thread
from random import randint
import math


class controller:

    
    

    def __init__(self):                #konstruktor
        self.Screen=0
        self.ClientIsConnected=0
        self.Uplink=0
        self.View=0

        self.Character=0
        self.UserName="WurstJuergen"
        self.LoggedIn = False

        self.DoPing = True
        self.TimeBetweenPingRequests = 2     #seconds between ping requests
        self.Ping = -1                       #saves ping-value to server

        self.Temp_HeightOfWorld = -1
        self.Temp_WidthOfWorld = -1
        self.Temp_SightRatio = -1
        self.Temp_MaxLevelOverZero = -1
        
        self.Audio = 0

        self.Sight_Ratio = 0     #the ratio between the sight radius in z-direction and the sight-radius in x/y-direction. will be send from server with login/account-creation-message and send stored here

        self.RandomMovementMode = False
        self.OutstandingRandomSteps = 0
        self.KillServerAfterRandomMoves = False      #only workes with default server-password 'password'

        self.GoDownLeftForSomeSteps_Mode = False
        self.OutstandingStepsFor_GoDownLeftForSomeSteps_Mode = 0
        
        self.ClientIsConnected = False
        self.Screen = curses.initscr()
        self.Uplink = uplink.uplink()
        self.Audio = audio.audio()
        #self.View=view.view()
        
        self.TargetID = -1            #id of account in target
        #self.AbsolutePositionOfCurrentTarget_PosY = -1
        #self.AbsolutePositionOfCurrentTarget_PosX = -1
        


    def welcomeScreen(self):
        curses.curs_set(0)
        pass
        PosY = int((self.Screen.getmaxyx()[0])/2) -3
        PosX = int((self.Screen.getmaxyx()[1])/2) - 32
        try:
            self.Screen.addstr(PosY  , PosX, "███╗   ███╗██╗███╗   ██╗███████╗██╗  ██╗ █████╗  ██████╗██╗  ██╗")
            self.Screen.addstr(PosY+1, PosX, "████╗ ████║██║████╗  ██║██╔════╝██║  ██║██╔══██╗██╔════╝██║ ██╔╝")
            self.Screen.addstr(PosY+2, PosX, "██╔████╔██║██║██╔██╗ ██║█████╗  ███████║███████║██║     █████╔╝ ")
            self.Screen.addstr(PosY+3, PosX, "██║╚██╔╝██║██║██║╚██╗██║██╔══╝  ██╔══██║██╔══██║██║     ██╔═██╗ ")
            self.Screen.addstr(PosY+4, PosX, "██║ ╚═╝ ██║██║██║ ╚████║███████╗██║  ██║██║  ██║╚██████╗██║  ██╗")
            self.Screen.addstr(PosY+5, PosX, "╚═╝     ╚═╝╚═╝╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝")
        except:
            self.debug("skipping 'ASCII-Art-Logo' terminal to small?")
            PosY = int((self.Screen.getmaxyx()[0])/2)
            PosX = int((self.Screen.getmaxyx()[1])/2) - 4
            self.Screen.addstr(PosY, PosX, "minehack")
        self.Screen.refresh()
        #curses.nonl()
        time.sleep(1)
        self.Screen.erase()
        self.Screen.refresh()
        pass
        Charactername = "blank"
        EnergyOfCharacter = -1
        curses.noecho()
        curses.cbreak()                        #Applications will also commonly need to react to keys instantly, without requiring the Enter key to be pressed; this is called cbreak mode, as opposed to the usual buffered input mode.
        self.Screen.keypad(1)
        self.Screen.addstr(0,0, "this is mineHack")
        self.Screen.addstr(1,0, "world's first virtual-reality-rogue-like-massivly-multiplayer-online-role-playing-game (VRRLMMORPG)")
        self.Screen.addstr(2,0, "press any key to connect to server")
        self.Screen.getch()
        self.Screen.addstr(3,0, "...connecting...")
        self.ClientIsConnected=self.Uplink.connectToServer()
        pass
        if ( self.ClientIsConnected == True ):
            self.Screen.addstr(4,0, "welcome to doomsday-device.de, sir!")
            self.Screen.addstr(6,0, "press 1 to log in with an existing account, press 2 to creat a new one")
            Input = self.Screen.getch()
            if Input == 49:         #ascii for 1
                self.Screen.addstr(7,0, "you pressed 1. so deal with it!")
                self.Screen.addstr(8,0, "type in your user-name, NOT YOUR CHARACTER NAME!!! (max. 32 characters!)")
                curses.echo()
                TryAgain = True
                while TryAgain == True:
                    TryAgain = False
                    self.Screen.move(9,0)
                    self.Screen.clrtoeol()
                    Username=self.Screen.getstr(9,0, 32)
                    try:
                        Username = Username.decode('utf-8')
                    except UnicodeDecodeError:
                        self.Screen.move(8, 0)
                        self.Screen.clrtoeol()                      #clears line
                        self.Screen.addstr(8, 0, "error. but that's not your fault. please enter your user-name again:")
                        self.Screen.refresh()
                        TryAgain = True
                self.Screen.addstr(10,0, "type in your password")
                TryAgain = True
                Password = ""
                while TryAgain == True:
                    TryAgain = False
                    self.Screen.move(11,0)
                    self.Screen.clrtoeol()                      #clears line
                    Password=self.Screen.getstr(11, 0 , 32)
                    self.Screen.refresh()
                    try:
                        Password = Password.decode('utf-8')
                    except UnicodeDecodeError:
                        self.Screen.addstr(10,0, "error. but that's not your fault. please enter your password again: ")
                        self.Screen.refresh()
                        TryAgain = True
                self.Uplink.sendStringToServer( "#RequestToLogin--", Username + "*" + Password )
                #self.Uplink.sendStringToServer( "#RequestToLogin--", Username.decode('latin-1') + "*" + Password.decode('latin-1') )
                self.Screen.refresh()
                #time.sleep(7)                        #need to sleep a second here, to make sure, the server sent his "unlockWritingMtx"-command and the "loginSuccessfull"-command. on this point, we ignore the Mutex, but to ignore it, you need to be sure, you should care!
                #ReceivedMessage = self.Socket.recv(1024)         #i hope, this blocks
                ReceivedMessage = self.Uplink.readFromSocket()
                #self.Screen.addstr(12,0, ReceivedMessage)
                self.Screen.refresh()
                #self.Screen.getch()
                MessageType = (((ReceivedMessage.split("#", 2))[1]).split(";",3) )[0]
                MessageContent = (((ReceivedMessage.split("#", 2))[1]).split(";",3) )[1]
                #self.debug("MessageContent: " + str(MessageContent) )
                if MessageType == "LoginFailed-----":
                    if MessageContent == "AccountDoesntExist":
                        self.Screen.addstr(13,0, "login faild. account doesn't exist!")
                    if MessageContent == "AccountAlreadyLoggedIn":
                        self.Screen.addstr(13,0, "login faild. account already logged in!")
                if MessageType == "LoginSuccessfull":
                    self.Screen.addstr(13,0, "login successfull. (entering world in 2 second)")
                    self.LoggedIn = True
                    #self.debug("login successful")
                    Charactername = MessageContent.split("*")[2]
                    EnergyOfCharacter = MessageContent.split("*")[3]
                    self.Temp_SightRatio = float( MessageContent.split("*")[4].split(":")[1] )
                    self.Temp_HeightOfWorld = int( MessageContent.split("*")[5].split(":")[1] )
                    self.Temp_WidthOfWorld = int( MessageContent.split("*")[6].split(":")[1] )
                    self.Temp_MaxLevelOverZero = int( MessageContent.split("*")[7].split(":")[1] )
                self.Screen.refresh()
            elif Input == 50:         #ascii for 2
                self.Screen.addstr(7,0, "you pressed 2. lets create a new account!")
                self.Screen.addstr(8,0, "type in your user-account name. this is not your character name! \n(max. 32 characters!, no special characters)")
                curses.echo()
                NewUserName=self.Screen.getstr(10,0, 32)
                self.Screen.addstr(12,0, "type in your password. this will be transmitted and stored in plain text \non the server, so don't use your common one here! (max. 32 characters, no special characters)")
                Password=self.Screen.getstr(14,0, 32)
                self.Screen.addstr( 16,0, "please choose character-name." )
                self.Screen.addstr(17, 0, "Remember: you will die a lot of times. And player-characters do not respawn. " )
                self.Screen.addstr(18, 0, "If he's dead, jim - he's dead, jim.")
                Charactername = self.Screen.getstr(19, 0, 32)
                self.Uplink.sendStringToServer( "#CreateNewAccount", NewUserName.decode('utf-8') + "*" + Password.decode('utf-8') + "*" + Charactername.decode('utf-8') )
                #self.Uplink.sendStringToServer( "CreateNewAccount", NewUserName.decode('latin-1') + "*" + Password.decode('latin-1') + "*" + Charactername.decode('latin-1') )
                self.Screen.refresh()
                #time.sleep(7)
                ReceivedMessage=self.Uplink.readFromSocket()
                #self.debug(ReceivedMessage)

                #MessageType = (((ReceivedMessage.split("#", 2))[2]).split(";",3) )[0]
                #thats needed, when orderToUnlockMutexIs activ

                #and that is needed, when not active:
                MessageType = ReceivedMessage.split("#", 1)[1].split(";",3)[0]
                MessageContent = ReceivedMessage.split("#", 1)[1].split(";",3)[1]
                if MessageType == "NuAccountCreated":
                    self.Screen.addstr(21,0, "new account successfully created! (entering world in 2 second)")
                    self.Screen.refresh()
                    self.LoggedIn = True
                    EnergyOfCharacter = MessageContent.split("*")[3]
                    self.Temp_SightRatio = float( MessageContent.split("*")[4].split(":")[1] )
                    self.Temp_HeightOfWorld = int( MessageContent.split("*")[5].split(":")[1] )
                    self.Temp_WidthOfWorld = int( MessageContent.split("*")[6].split(":")[1] )
                    self.Temp_MaxLevelOverZero = int( MessageContent.split("*")[7].split(":")[1] )
                if MessageType == "NameAlreadyInUse":
                    self.Screen.addstr(21,0, "can't create new account. '" + str( NewUserName.decode('utf-8') ) + "' is already in use!")
                    self.Screen.refresh()
                pass
                #self.Screen.getch()
                #time.sleep(1)                            #sleep is better because, 
                pass
                #self.Screen.refresh
            else:
                self.shutdownGame()
                print("didn't press 1 or 2")
                self.Screen.refresh()
        else:
            self.Screen.addstr(3,0, "seems to be no doomsday today! server offline :-(")
            self.Screen.getch()
        pass
        time.sleep(2)
        self.Character=character.character()
        self.Character.Name = str(Charactername)
        self.Character.Energy = int(EnergyOfCharacter)
        pass
        pass
        #curses.nocbreak()
        #that ^ is bad here!
        #self.Screen.keypad(0)
        #curses.echo()                        #Terminating a curses application is much easier than starting one. You'll need to call
        #curses.endwin()

        
    def enterMaingame(self):
        self.welcomeScreen()
        #that is annoying when i work on the view. but i need to fake it:
        #self.ClientIsConnected = True

        if self.LoggedIn == True:
            self.buildView()
        else:
            self.shutdownGame()

    def buildView(self):
        curses.endwin()         #end windows which might be still open (will happen when resizing terminal)
        self.View=view.view( self.Character.PosZ )    
#        self.View.HeightOfWorldPad = int(self.Temp_HeightOfWorld) * 2
#        self.View.WidthOfWorldPad = int(self.Temp_WidthOfWorld) * 2
        self.View.MaxLevelOverZero = self.Temp_MaxLevelOverZero
        self.View.SightRatio = self.Temp_SightRatio
        if self.View.initView() == False:        #init maingame-window
            self.beep()
            print("initView() failed. probably because of terminal size. contact your local drug dealer")
            self.shutdownGame()
        pass
        self.View.drawBorders()
        self.View.restoreChatHistory()
        self.View.CharacterBackup = self.Character
        self.View.updateInfoScreen()
        self.startThreadManagement()





    def pingLoop(self):
        #self.debug("entering pingLoop()")
        Hostname = self.Uplink.getServer()
        while(self.ClientIsConnected):
            PingProcess = subprocess.Popen( ["ping", "-c", "1", Hostname], stdout = subprocess.PIPE, stderr = subprocess.PIPE )
            Out = PingProcess.communicate()[0]
            Out = Out.decode("utf-8").split()
            try:
                self.Ping = [s for s in Out if "time" in s][0].split("=")[1]
            except:
                self.Ping = -1

            #self.debug("Ping: " + str(self.Ping))
            self.View.updatePing(self.Ping)
            time.sleep(self.TimeBetweenPingRequests)
        #self.debug("leaving entering pingLoop()")


    def startThreadManagement(self):
        Thread_EventHandler = Thread(target=self.eventHandler, args=())
        Thread_EventHandler.start()

        if self.DoPing:
            Thread_Ping = Thread(target=self.pingLoop, args=())
            Thread_Ping.start()

        self.waitForIncommingMessages()


        
    def eventHandler(self):
        #self.debug("eventHandler")
        while (self.ClientIsConnected == True):
            #self.debug("entering while-loop of eventHandler()")
            pass
            Event = ""
            if self.RandomMovementMode == False and self.GoDownLeftForSomeSteps_Mode == False:            #autopilote (relevant for benchmarking..)
                Event = self.View.waitForEvent()
                
                ToWriteMessage = "[" + str(time.time()) + "]: " + str(Event) + "\n"
                with open("log/events.log", 'a') as out:
                    out.write( ToWriteMessage )
                
            elif (self.GoDownLeftForSomeSteps_Mode == True and self.RandomMovementMode == False):
                time.sleep(1)
                self.OutstandingStepsFor_GoDownLeftForSomeSteps_Mode = self.OutstandingStepsFor_GoDownLeftForSomeSteps_Mode - 1
                Event = "moveDownRight"
                if self.OutstandingStepsFor_GoDownLeftForSomeSteps_Mode == 0:
                    self.GoDownLeftForSomeSteps_Mode = False
                    self.startRandomMovements(500)
            elif self.RandomMovementMode == True and self.GoDownLeftForSomeSteps_Mode == False:
                #self.beep()
                self.OutstandingRandomSteps = self.OutstandingRandomSteps - 1
                time.sleep(1)
                Random = randint(0,7)
                if Random == 0:
                    Event = "moveUpLeft"
                elif Random == 1:
                    Event = "moveUp"
                elif Random == 2:
                    Event = "moveUpRight"
                elif Random == 3:
                    Event = "moveRight"
                elif Random == 4:
                    Event = "moveDownRight"
                elif Random == 5:
                    Event = "moveDown"
                elif Random == 6:
                    Event = "moveDownLeft"
                elif Random == 7:
                    Event = "moveLeft"
                pass
                if self.OutstandingRandomSteps == 0:
                    self.RandomMovementMode = False
                    self.View.displayNewChatMessage("done with RandomMovements", True)
                    if self.KillServerAfterRandomMoves == True:
                        #self.debug("killing Server after RandomMoves (because of profiling)")
                        self.Uplink.sendStringToServer("#AdminCommand----", "password*killServer")
            else:
                self.debug("eventHandler(): something fishy")
            
            if ( (Event == "moveLeft") and ( int(self.Character.PosX) > 0)):
                self.sendRequestToMove("Left")
            elif (Event == "moveRight"):
                self.sendRequestToMove("Right")
            elif (Event == "moveUp" and int(self.Character.PosY) > 0):
                self.sendRequestToMove("Up")
            elif (Event == "moveDown"):
                self.sendRequestToMove("Down")
            elif (Event == "moveUpRight"):
                self.sendRequestToMove("UpRight")
            elif (Event == "moveUpLeft" and int(self.Character.PosY) > 0 and int(self.Character.PosX) > 0 ): 
                self.sendRequestToMove("UpLeft")
            elif (Event == "moveDownRight"):
                self.sendRequestToMove("DownRight")
            elif (Event == "moveDownLeft" and int(self.Character.PosX) > 0):
                self.sendRequestToMove("DownLeft")
            elif (Event == "chatMode"):
                self.chatMode(False, "")
            elif ( Event == "scrollChatUp" ):
                self.View.scrollChatHistoryUp()
            elif ( Event == "scrollChatDown" ):
                self.View.scrollChatHistoryDown()
            elif ( Event == "exitGame" ):
                self.Uplink.sendStringToServer( "#CloseConnection-", "foo" )
                #self.beep()
                self.shutdownGame()
            elif (Event == "zoomUp"):
                #self.beep()
                self.View.zoomUp( self.Character )
            elif (Event == "zoomDown"):
                self.View.zoomDown( self.Character )
            elif (Event == "resetZoom"):
                self.View.resetZoom( self.Character )
            elif (Event == "rebuildView"):
                self.View.rebuildWindow()        # that is not working well
            elif (Event == "tabulator"):
                self.selectNextCharacter()
            elif (Event == "ESC"):
                #self.View.flashIt()
                #self.View.clearChatInput()
                #if self.Character.PositionInListThisCharacterIsTargetting != -1:
                #    self.Character.PositionInListThisCharacterIsTargetting = -1
                #    self.Character.Target = -1
                #    self.View.updateWorldWindow(self.Character)
                if self.TargetID != -1:
                    #self.View.flashIt()
                    self.TargetID = -1
                    self.View.Target_String = ""
                    self.View.AbsolutePositionInTarget_PosY = -1
                    self.View.AbsolutePositionInTarget_PosX = -1
                    self.View.updateWorldWindow( self.Character )
                    self.View.updateInfoScreen()
                elif self.View.CurrentlyShowingOldChatMessage:
                    self.View.clearChatInput()
                    #self.View.flashIt()
                    #self.View.OffsetFromTheLastMessageInListToTheOneToDisplay = 0
                else:
                    pass
                    #missing: main-menu or something.
            elif (Event == "PrimaryAttack"):
                self.sendPrimaryAttack()
                self.Audio.playAudio("ntfbp")
            elif (Event == "SecondaryAttack"):
                self.sendSecondaryAttack()
#            elif (Event == "deleteSightField"):
#                self.deleteSightField()
            elif (Event == "ArrowUp"):
                #self.View.flashIt()
                self.View.incrementPointerToOldChatInput()
                self.View.showOldMessageInChat()
            elif (Event == "ArrowDown"):
                if self.View.decrementPointerToOldChatInput() == True:
                    self.View.showOldMessageInChat()
                else:
                    self.View.clearChatInput()
            elif Event == "Return":
                if self.View.CurrentlyShowingOldChatMessage:
                    self.chatMode( True, self.View.getCurrentlyShowedOldMessage() )
                    self.View.clearChatInput()
                    
            



    #adminCommands must have the form: '|*somePassword*someCommand'
    def chatMode(self, In_TakeThisMessage, In_Message):
        Temp_MessageContent = ""
        if In_TakeThisMessage == False:
            Temp_MessageContent = self.View.enterChatMode()
        else:
            Temp_MessageContent = In_Message
        
        #checking if player is trying to send an adminCommand
        MessageType = ""
        if len(Temp_MessageContent) > 0:
            if Temp_MessageContent[0] == 124:        # '|' -> server-command
                try:            
                    Password = str(Temp_MessageContent).split('*')[1]
                    LengthOfPassword = len(Password)
                    if LengthOfPassword <= 16:
                        MessageType = "#AdminCommand----"
                        MessageContent = Temp_MessageContent.decode( 'utf-8')
                        MessageContent = MessageContent[2:]
                        self.Uplink.sendStringToServer( MessageType, str( MessageContent ) )
                    else:
                        self.beep()
                        #missing: you tryied to send a command, but the password is to long
                except IndexError:
                    self.View.displayNewChatMessage( "you tryed to send a server-command? fail!", True)   # syntax-error!
                except UnicodeDecodeError:
                    self.View.displayNewChatMessage( "error while processing server-command. UnicodeDecodeError.", True) 
            #that is for client-commad messages:
            elif Temp_MessageContent[0] == 38:          # '&' -> client-command
                try:
                    Command = str(Temp_MessageContent).split('*')[1]
                    self.interpretClientCommand(Command)
                except IndexError: 
                    self.View.displayNewChatMessage( "you tryed to send a client-command? fail!", True)
                except UnicodeDecodeError:
                    self.View.displayNewChatMessage( "error while processing client-command. UnicodeDecodeError.", True)
            else:                                       # normal chat message
                MessageType = "#ChatMessage-----"
                try:
                    self.Uplink.sendStringToServer( MessageType, Temp_MessageContent.decode( 'utf-8' ) )
                except UnicodeDecodeError:
                    self.View.displayNewChatMessage( "error while processing message. maybe used non-ascii-symbols?", True) 




    def shutdownGame(self):
        self.ClientIsConnected = False
        #time.sleep(2)
        pass
        #self.Uplink.closeConnection()
        curses.echo()                        #Terminating a curses application is much easier than starting one. You'll need to call
        curses.endwin()
        #need more stuff here...
        #os.system("bash -c \"reset\"")
        print("minehack exit")
        self.debug("exit minehack from shutdownGame()")
        exit()

    def getTimeInMilliseconds(self):
        return time.time()

    def beep(self):
        os.system("bash -c \"speaker-test -t sine -f 440 -l 1 -p 2 >/dev/null\"")
        
        
    def waitForIncommingMessages(self):
        while (self.ClientIsConnected == True):
            IncommingMessage = self.Uplink.readFromSocket()        #blocks until new message arrived
            pass
            pass
            if IncommingMessage == "TerminalResized":        #just in case, the terminal got resized. that would be detected as an IO-error in uplink and can be handled here
                self.View.initView()    #init maingame-window
                self.View.drawBorders()
                self.View.restoreChatHistory()
                self.View.updateWorldWindow(self.Character)
                self.View.updateInfoScreen()
            elif( IncommingMessage == "ServerOffline" ):
                self.shutdownGame()
            else:                             #normal case
                self.interpretReceivedMessage(IncommingMessage)

    def sendRequestToMove(self, In_direction):    #called from eventHandler
        MessageType = "#RequestToMove---"
        
        if (In_direction == "Left"):
            Temp = "left"
        elif(In_direction == "Right"):
            Temp = "right"
        elif(In_direction == "Up"):
            Temp = "up"
        elif(In_direction == "Down"):
            Temp = "down"
        elif(In_direction == "DownLeft"):
            Temp = "downleft"
        elif(In_direction == "DownRight"):
            Temp = "downright"
        elif(In_direction == "UpLeft"):
            Temp = "upleft"
        elif(In_direction == "UpRight"):
            Temp = "upright"
        else:
            exit()            #something went wrong
        self.Uplink.sendStringToServer(MessageType, Temp);



    def interpretReceivedMessage(self, In_IncommingMessage):    #called from: waitForIncommingMessages()
        pass
        #self.debug("In_IncommingMessage:")
        #self.debug(In_IncommingMessage)
        pass
        ArrayWithMessages = In_IncommingMessage.split("#")
        ArrayWithMessages.remove("")                #because we have an empty element in the array
        #MessageType=0
        #MessageContent=0
        pass
        #debug:
        #if len(ArrayWithMessages) > 1:
        #    self.beep()
        #
        pass
#        self.debug(ArrayWithMessages)
        pass
        for Message in ArrayWithMessages:
            ToWriteMessage = "[" + str(time.time()) + "]: " + str(Message) + "\n"
            with open("log/interpretReceivedMessage.log", 'a') as out:
                out.write( ToWriteMessage )
            Message=Message.split(";")
            MessageType=Message[0]
            MessageContent=Message[1]
            #self.beep();
            pass
            pass
            pass
            if( Message[2] != "end" ):
                print("error in interpretReceivedMessage! ';end' missing!")
                exit()
            elif MessageType == "ChatMessage-----":
                self.View.displayNewChatMessage(MessageContent, False)
            elif MessageType == "Heartbleed------":
                self.handleReceivedHeartbleed(MessageContent)
            elif MessageType == "Heartbleed2Blue-":
                self.View.turnOnBlueHeartbleed()
            elif MessageType == "UnlockWritingMtx":
                pass                    #i leave that out for now. maybe python can deal without it 
                self.shutdownGame()
            elif MessageType == "SightField------":
                self.extractSightField(MessageContent, False)
                self.View.updateWorldWindow( self.Character )
                self.View.updateInfoScreen()
            elif MessageType == "UpdateEnergy----":
                #self.View.flashIt()
                #self.beep()
                pass
                self.updateEnergy(MessageContent)
            elif MessageType == "unknownCommand--":
                self.View.displayMessageIn_Server_MsgContent( "unknown Command: '" + MessageContent + "'" )
            elif MessageType == "AdmnCmmndWrngPw-":
                self.View.displayMessageIn_Server_MsgContent( "Wrong server-password. Command: '" + MessageContent + "'" )
            elif MessageType == "AdmnCmmndMsngCmd":
                self.View.displayMessageIn_Server_MsgContent( "Missing command" )
            elif MessageType == "AdmnCmmnd2MnyWrd":
                self.View.displayMessageIn_Server_MsgContent( "to many words for a correct server-command" )
            elif MessageType == "broadcast-------":
                self.View.displayMessageIn_Server_MsgContent( MessageContent )
            else:
                print("unknown message")
                self.debug("exit minehack from interpretReceivedMessage()")
                exit()
        #self.debug("leaving interpretReceivedMessage()")


    def handleReceivedHeartbleed(self, In_MessageContent):
        self.View.showHeartbleed()
        Thread_AnswerHeartbleed = Thread(target=self.startThreadToAnswerHeartbleed, args=())
        Thread_AnswerHeartbleed.start()
        


    def startThreadToAnswerHeartbleed(self):
        time.sleep(3)
        self.Uplink.sendStringToServer("HeartbleedFromCl", "foo")

    def debug(self, In):
        ToWriteMessage = "[" + str(time.time()) + "]: " + str(In) + "\n"
        with open("log/debug.log", 'a') as out:
            out.write( ToWriteMessage )



    def extractSightField(self, In_MessageContent, In_IsDifferentialSightField):
        #self.debug("entering extractSightField()")
        LowestZalreadySet = False
        Message=In_MessageContent.split("%")
        Header = Message[0]                     #header contains the sightRadius and the current position of the character
        #self.debug("int(Header.split('*')[0].split(':')[1]):" + str( int(Header.split("*")[0].split(":")[1]) ) )
        self.Character.SightRadius = int(Header.split("*")[0].split(":")[1])
        self.Character.PosY = int(Header.split("*")[1].split(":")[1])
        self.Character.PosX = int(Header.split("*")[2].split(":")[1])
        self.Character.PosZ = int(Header.split("*")[3].split(":")[1])
        Message.pop(0)
        Message = [x for x in Message if x != '']    #removing emty elements
        Temp_SightField_Y_Keyed = {}
        Z_Key = ""            #the key must be the position in one string. like for example "Y:2*X:3*Z:5"
        X_Key = ""
        Y_Key = ""
        PosX = ""
        PosY = ""
        PosZ = ""
        Energy = ""
        KindOfCube = ""
        LowestZinSightField = -1
        for first in Message:
            Position=first.split("!")[0]
            Position = Position.split("*")
            PosY = Position[0].split(":")[1]
            PosX = Position[1].split(":")[1]
            PosZ = Position[2].split(":")[1]
            first = first.split("!!")
            first[0] = first[0].split("!", 1)[1]
            for foo in first:
                foo = foo.split("!")
                foo = [x for x in foo if x != '']
                KindOfCube=foo[0].split("*")[0]
                Energy=foo[1]
                if int(LowestZinSightField) > int(PosZ) or int(LowestZinSightField) == -1:
                    LowestZinSightField = PosZ
                
                Stuff=foo[0].split("*")
                Temp_Cube = cube.cube(int(PosY), int(PosX), int(PosZ), int(Energy), str(KindOfCube))    #creating the new sightField
                if KindOfCube == "characterCube":
                    Temp_Cube.AccountID = Stuff[1]
                    Temp_Cube.CharacterName = Stuff[2]
                    Temp_Cube.AccountName = Stuff[3]
                    self.Character.handleNewCharacter( Temp_Cube )
                    #self.Character.ListWithCharactersSeenByMe.append(Temp_Cube)
                if Temp_Cube.PosY in Temp_SightField_Y_Keyed:
                    Temp_SightField_Y_Keyed[Temp_Cube.PosY].append(Temp_Cube)
                else:
                    ListOfCubes = []
                    ListOfCubes.append(Temp_Cube)
                    Temp_SightField_Y_Keyed[Temp_Cube.PosY] = ListOfCubes
                
        self.View.writeSightFieldInWorldPads( Temp_SightField_Y_Keyed, self.Character.PosY, self.Character.PosX )




#    def selectNextCharacter(self):
#        self.View.flashIt()
#        self.Character.PositionInListThisCharacterIsTargetting = self.Character.PositionInListThisCharacterIsTargetting + 1
#        self.debug("self.Character.Target: " + str(self.Character.Target))
#        self.debug("len( self.Character.ListWithCharactersSeenByMe_sortedByDistance: )" + str(len( self.Character.ListWithCharactersSeenByMe_sortedByDistance )))
#        if self.Character.PositionInListThisCharacterIsTargetting == len( self.Character.ListWithCharactersSeenByMe_sortedByDistance ):
#            self.Character.PositionInListThisCharacterIsTargetting = -1
#            self.Character.Target = -1
#            self.View.updateWorldWindow(self.Character)
#        else:
#            self.Character.Target = self.Character.ListWithCharactersSeenByMe_sortedByDistance[ self.Character.PositionInListThisCharacterIsTargetting ][1].AccountID
#            self.View.updateWorldWindow(self.Character)
#            self.View.markPositionWithX( self.Character.ListWithCharactersSeenByMe_sortedByDistance[self.Character.PositionInListThisCharacterIsTargetting][1].PosY, self.Character.ListWithCharactersSeenByMe_sortedByDistance[self.Character.PositionInListThisCharacterIsTargetting][1].PosX )
        


    def selectNextCharacter(self):
        self.Character.cleanUpList_CharactersThisCharacterCanSee()
        #calc distance to each other character:
        CharactersThisCharacterCanSee_dict = {}
        for CharacterCube in self.Character.CharactersThisCharacterCanSee:      #find the nearest character:
            Temp_Distance = math.sqrt( (self.Character.PosY - CharacterCube.PosY)**2 + (self.Character.PosX - CharacterCube.PosX)**2 + (self.Character.PosZ - CharacterCube.PosZ)**2 )
            CharactersThisCharacterCanSee_dict[Temp_Distance] = CharacterCube
        
        if self.TargetID == -1:         #nothing in target right now
            shortestDistance = (sorted(CharactersThisCharacterCanSee_dict))[0]
            self.debug("shortestDistance: " + str(shortestDistance))
            NearestCharacterCube = CharactersThisCharacterCanSee_dict[shortestDistance]
            self.TargetID = NearestCharacterCube.AccountID
            self.View.takePositionInTarget( NearestCharacterCube.PosY, NearestCharacterCube.PosX )
            self.View.Target_String = NearestCharacterCube.CharacterName
            self.View.updateInfoScreen()
        if self.TargetID != -1:         #something in target
            pass
            #missing



    def sendPrimaryAttack(self):
        pass
        if self.Character.PositionInListThisCharacterIsTargetting != -1:
            pass
            self.Uplink.sendStringToServer( "#PrimaryAttack---" , str( (self.Character.ListWithCharactersSeenByMe_sortedByDistance[self.Character.PositionInListThisCharacterIsTargetting])[1].AccountID ) )
        else:
            pass
            #missing: handling, "no target"



    def sendSecondaryAttack(self):
        pass
        if self.Character.PositionInListThisCharacterIsTargetting != -1:
            pass
            self.Uplink.sendStringToServer( "#SecondaryAttack-" , str( (self.Character.ListWithCharactersSeenByMe_sortedByDistance[self.Character.PositionInListThisCharacterIsTargetting])[1].AccountID ) )
        else:
            pass
            #missing: handling, "no target"




    def updateEnergy(self, In_NewEnergy):
        #self.beep()
    #    self.debug("entering UpdateEnergy()")
        if self.Character.Energy > int(In_NewEnergy):
            self.View.flashIt()
            self.Character.Energy = int(In_NewEnergy)
            self.View.CharacterBackup = self.Character
            self.View.updateInfoScreen()
        else:
            pass
            #missing: you got healed
        #self.debug("leaving UpdateEnergy()")





    def interpretClientCommand(self, In_Command):               #called from ChatMode()
        #self.debug("interpretClientCommand: " + In_Command)
        pass
        Temp_Command = str(In_Command)[:-1]
        Command = Temp_Command.split()[0]
        #self.debug("Command: " + Command)
        if str(Command) == "startRandomMovements":
            NumberOfSteps = int( Temp_Command.split()[1] )
            self.View.displayNewChatMessage("client-command: startRandomMovements. starting now!", True)
            self.startRandomMovements(NumberOfSteps)
        elif str(Command) == "benchmark":
            #self.debug("Command was 'benchmark'")
            self.View.displayNewChatMessage("client-command: starting profiling-routine", True)
            self.GoDownLeftForSomeSteps_Mode = True
            self.OutstandingStepsFor_GoDownLeftForSomeSteps_Mode = 50
            self.KillServerAfterRandomMoves = True
        elif str(Command) == "clearPad":
            self.debug("clearing current pad after client command")
            self.View.clearPadsWithMetaIndex( self.View.IndexOfCurrentWorldPad )
            self.View.updateWorldWindow( self.Character )
        else:
            self.View.displayNewChatMessage("unknown client-command: " + In_Command, True)





    def startRandomMovements(self, In_NumberOfRandomMovements):
        self.debug("entering startRandomMovements")
        self.OutstandingRandomSteps = In_NumberOfRandomMovements
        self.RandomMovementMode = True
        self.debug("leaving startRandomMovements")




