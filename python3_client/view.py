#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

import sys

import os, platform
import curses
import pdb
from datetime import datetime
import time
import gc
from copy import deepcopy

from threading import Thread #, Lock

class view:
    
    
    def __init__(self, In_PosZ):
        self.CHAT_SIZE  = 5
        self.CHAT_INPUT_SIZE = 2
        self.SERVER_MSG_SIZE = 2
        self.INFO_SIZE = 37
        self.HEARTBLEED_SIZE = 1
        self.PING_SIZE = 5

        #MAX_HEIGHT_OF_WORLD = 14           # this value should later be set by server at login
        self.MaxLevelOverZero = -1
        self.SightRatio = -1

        self.HeightOfWorldWindow=0
        self.WidthOfWorldWindow=0
        self.WidthOfServer_MsgContentWindow=0
        self.HeightOfServer_MsgContentWindow=0
        self.NumberOfReceivedChatMessages=0

        # World Pads Dimensions:
        self.HeightOfWorldPad = 60
        self.WidthOfWorldPad = 100

        self.IndexOfCurrentWorldPad = 0      # a value from 0 to 3 (0 or 1 or 2 or 3) which indicates the index of WorldPads in WorldPads_Meta[] on which the character is sitting right now. per default, it's '0'
        self.Szenario = -1
        self.CharacterIsOnRightSideOfCurrentWorldPad = False
        self.CharacterIsOnLeftSideOfCurrentWorldPad = False
        self.CharacterIsOnLowerSideOfCurrentWorldPad = False
        self.CharacterIsOnUpperSideOfCurrentWorldPad = False
        
        self.InChatMode = False      #importat, because need to torn ping-refreshing off when in chat mode
        self.LastInput = ""
        self.NextInputCommingIsFunctionKey = False
        self.OldChatInputs = []
        self.OffsetFromTheLastMessageInListToTheOneToDisplay = 0
        self.CurrentlyShowingOldChatMessage = False

        self.Debug0 = "debug0"         #this variable will get displayed in the side-bar (Info-Window) and can be used for debugging as because auf curses, debugging with a debugger is buggy 
        self.Debug1 = "debug1"
        self.Debug2 = "debug2"
        self.Debug3 = "debug3"
        self.Debug4 = "debug4"
        self.Debug5 = "debug5"
        self.Debug6 = "debug6"
        self.Debug7 = "debug7"

        self.Old_InfoScreenPage = -1
        self.InfoScreenPage = 1      #per default the game starts with the info-screen-page number 1

        self.IsInLeftSideOfScreen = -1
        self.IsInUpperSideOfScreen = -1        #needed to decide where to display other character names

        self.PosYInWorldWindow = -1
        self.PosXInWorldWindow = -1

        self.CurrentlyFlashing = False           #is The Terminal currently flashing? 
        
        #i try to define a square which is about 1/3 of the size of the World-window.
        #it should be about in the middle of the World-window. 
        #if the character moves from Y0,X0 (start position) to the middle of the screen,
        #and (for example) further down and crosses the lower side of this square, the 
        #whole world should move and the character should remain on that position. 
        #by that, the character is always in about the middle of the screen. only when moving up
        #or left to X=0 or Y=0, the @-symbol will move to the edges again.
        #i call it
        #MiddleSquare:
        self.Y_DistanceUpperLeftCornerOfMiddleSquare = 0        #distance from the upper left corner of the worldWindow
        self.X_DistanceUpperLeftCornerOfMiddleSquare = 0
        self.HeightOfMiddleSquare = 0
        self.WidthOfMiddleSquare = 0
        

        self.Screen = 0
        self.Server_Msg_Frame = 0
        self.Server_MsgContent = 0
        self.Heartbleed_Frame = 0
        self.Heartbleed = 0
        self.Ping_Frame = 0
        self.WorldFrame = 0
        self.World = 0
        #WorldOverlay = 0
        self.ChatHistoryFrame = 0
        self.ChatHistory = 0
        self.ChatInputFrame = 0
        self.ChatInput = 0
        self.InfoFrame = 0
        self.Info = 0
        self.XZ_View = 0
        self.YZ_View = 0
        self.XZ_View_Height = 0
        self.XZ_View_Width = 0
        self.YZ_View_Height = 0
        self.YZ_View_Width = 0
        self.XZ_ViewPads_Height = 0
        self.XZ_ViewPads_Width = 0
        self.YZ_ViewPads_Height = 0
        self.YZ_ViewPads_Width = 0

        self.UpperLeftCornerOfPad_Y = []
        self.UpperLeftCornerOfPad_X = []

        self.LowerRightCornerOfPad_Y = []
        self.LowerRightCornerOfPad_X = []

        #RecentlyReceivedCubesOnPad = [] #this array will contain four booleans. after receiving the sight-field, the booleans will be set true if a pad (with the same index) has received a symbol

        self.WorldPads_Meta = []
        #Pads (per default):
        #       |-----|-----|
        #       |     |     |
        #       |  0  |  1  |
        #       |     |     |
        #       |-----|-----|
        #       |     |     |
        #       |  2  |  3  |
        #       |     |     |
        #       |-----|-----|
        #
        #

        self.SightFieldOverlay = 0

        self.PositionOfTheUpperLeftCornerInWorld_Y = 0
        self.PositionOfTheUpperLeftCornerInWorld_X = 0

        self.PositionOfTheLowerRightCornerInWorld_Y = 0
        self.PositionOfTheLowerRightCornerInWorld_X = 0

        self.Z_Level = 0                #z-level which will be displayed
        self.Z_LevelAttachedToCharacter = True

        self.NumberOfOtherCharacterToPlace = 0

        self.Z_Level = In_PosZ

        self.AbsolutePositionInTarget_PosY = -1
        self.AbsolutePositionInTarget_PosX = -1
        self.Target_String = "" #this should contain a stirng with the target's name


    def initView(self):
        
        curses.initscr()

        curses.start_color()
        curses.use_default_colors()

        self.Screen = curses.initscr()

        HeightOfTerminal = self.Screen.getmaxyx()[0]
        WidthOfTerminal = self.Screen.getmaxyx()[1]
        
        #                                               height                                                                                      width                                                                       position Y                                                                                                                      position X
        try:
            self.InfoFrame = curses.newwin(             HeightOfTerminal - (self.CHAT_SIZE + self.CHAT_INPUT_SIZE + self.SERVER_MSG_SIZE),          self.INFO_SIZE+1,                                                           self.SERVER_MSG_SIZE,                                                                                                           WidthOfTerminal-self.INFO_SIZE-1 )
            self.Server_Msg_Frame = curses.newwin(      self.SERVER_MSG_SIZE+1,                                                                     WidthOfTerminal - (self.HEARTBLEED_SIZE+1) - (self.PING_SIZE+1),            0,                                                                                                                              0 )
            self.Server_MsgContent = curses.newwin(     self.SERVER_MSG_SIZE-1,                                                                     WidthOfTerminal - 2  - (self.HEARTBLEED_SIZE+1) - (self.PING_SIZE+1),       1,                                                                                                                              1 )
            self.Heartbleed_Frame = curses.newwin(      self.SERVER_MSG_SIZE+1,                                                                     self.HEARTBLEED_SIZE+2,                                                     0,                                                                                                                              WidthOfTerminal-self.HEARTBLEED_SIZE-2 )
            self.Heartbleed = curses.newwin(            self.SERVER_MSG_SIZE-1,                                                                     self.HEARTBLEED_SIZE,                                                       1,                                                                                                                              WidthOfTerminal-self.HEARTBLEED_SIZE-1 )
            self.Ping_Frame = curses.newwin(            self.SERVER_MSG_SIZE+1,                                                                     self.PING_SIZE+2,                                                           0,                                                                                                                              WidthOfTerminal-self.HEARTBLEED_SIZE-3-self.PING_SIZE)
            #self.PingWindow = curses.newwin(            self.SERVER_MSG_SIZE-1,                                                                     self.PING_SIZE,                                                             1,                                                                                                                              WidthOfTerminal-self.HEARTBLEED_SIZE-2-self.PING_SIZE )
            self.WorldFrame = curses.newwin(            HeightOfTerminal - (self.CHAT_SIZE + self.CHAT_INPUT_SIZE + self.SERVER_MSG_SIZE),          WidthOfTerminal - self.INFO_SIZE,                                           self.SERVER_MSG_SIZE,                                                                                                           0 )
            self.World = curses.newwin(                 ( HeightOfTerminal - (self.CHAT_SIZE + self.CHAT_INPUT_SIZE + self.SERVER_MSG_SIZE) ) - 2,  (WidthOfTerminal - self.INFO_SIZE)-2,                                       self.SERVER_MSG_SIZE+1,                                                                                                         1 )
            self.ChatHistoryFrame = curses.newwin(      self.CHAT_SIZE+1,                                                                           WidthOfTerminal,                                                            HeightOfTerminal - (self.CHAT_SIZE + self.CHAT_INPUT_SIZE)-1,                                                                   0 )
            self.ChatHistory = curses.newwin(           self.CHAT_SIZE - 1,                                                                         WidthOfTerminal-2,                                                          (HeightOfTerminal - (self.CHAT_SIZE + self.CHAT_INPUT_SIZE)) ,                                                                  1 )
            self.ChatInputFrame = curses.newwin(        self.CHAT_INPUT_SIZE+1,                                                                     WidthOfTerminal,                                                            HeightOfTerminal - self.CHAT_INPUT_SIZE-1,                                                                                      0 )
            self.ChatInput = curses.newwin(             self.CHAT_INPUT_SIZE-1,                                                                     WidthOfTerminal-2,                                                          HeightOfTerminal - self.CHAT_INPUT_SIZE ,                                                                                       1 )
            self.Info = curses.newwin(                  HeightOfTerminal - (self.CHAT_SIZE + self.CHAT_INPUT_SIZE + self.SERVER_MSG_SIZE) - 2,      self.INFO_SIZE - 2,                                                         self.SERVER_MSG_SIZE + 1,                                                                                                       WidthOfTerminal-self.INFO_SIZE )
            self.XZ_View = curses.newwin(               int((self.Info.getmaxyx()[0]+2)/2) - 1,                                                     self.INFO_SIZE - 1 ,                                                        self.SERVER_MSG_SIZE + 1,                                                                                                       WidthOfTerminal-self.INFO_SIZE )#- 1 )
            self.YZ_View = curses.newwin(               int((self.Info.getmaxyx()[0]+2)/2) - 1,                                                     self.INFO_SIZE - 1 ,                                                        (self.SERVER_MSG_SIZE + 1) + int( (self.Info.getmaxyx()[0]+2)/2 - 1 ),                                                          WidthOfTerminal-self.INFO_SIZE )#- 1 )
        except curses.error:
            self.debug("detected error in first try{} of initView()")
            return False
        self.XZ_ViewPads_Height = self.MaxLevelOverZero
        self.XZ_ViewPads_Width = self.XZ_View.getmaxyx()[1]
        self.YZ_ViewPads_Width = self.YZ_View.getmaxyx()[1]
        

        for Index in range(0, 4):
            self.UpperLeftCornerOfPad_Y.append(-1)
            self.UpperLeftCornerOfPad_X.append(-1)
            self.LowerRightCornerOfPad_Y.append(-1)
            self.LowerRightCornerOfPad_X.append(-1)


        try:
            #making pad-arrays for World-window
            #self.debug( "self.HeightOfWorldPad: " + str(self.HeightOfWorldPad) + " ; self.WidthOfWorldPad:" + str(self.WidthOfWorldPad) )
            #this blueprint-pad-array consist of three pads. 
            #every pad has a different symbol of the same cube (on the same position then)
            #
            #the first ( [1] ) (SecondarySymbol) will be used for all layers wich are way below character. they must be transparrend for none-blocks and other-wise only consist of a centered dot 
            #the second ( [2] ) (TertiarySymbol) stores the same information as the but for solid cubes it uses a gread field which then markes the level, the character is standing at.
            #the zeroest ( [0] ) (PrimarySymbol) the this layer contains all symbols for the layer the character is at. which means, everything one cube above the cube the character is standing on top
            for MetaMapIndex in range(0, 4):
                LayerStack = []
                for Layer in range(0, self.MaxLevelOverZero):
                    ThreePads = []
                    for MapSetNumber in range(0, 3): 
                        #ThreePads.append( curses.newpad( self.HeightOfWorldPad + 1, self.WidthOfWorldPad ) )
                        ThreePads.append( curses.newpad( self.HeightOfWorldPad + 1, self.WidthOfWorldPad ) )
                    LayerStack.append(ThreePads)                             #   ^ I put this '+1' in here, because i have the problem, that i get errors when writing the last symbol in a pad/winddow (means, the character in the lower-right corner. for some reasone, curses wants to set the curser in the next row, but offcourse there is no line. and then i get an error. i used to catch these errors, but now i get this black slots the pad (like, the symbol has not be written. so i try it with pads, one row higher the necessary, so i can avoid this error-catching...
                self.WorldPads_Meta.append( LayerStack )  

            #overlay:
            self.SightFieldOverlay = curses.newpad( self.WorldFrame.getmaxyx()[0]+1, self.WorldFrame.getmaxyx()[1] )
            pass
            #and XZ_View / YZ_View:
            self.XZ_ViewPads = ( curses.newpad( self.MaxLevelOverZero, self.XZ_ViewPads_Width ), curses.newpad( self.MaxLevelOverZero, self.XZ_ViewPads_Width ) )
            self.YZ_ViewPads = ( curses.newpad( self.MaxLevelOverZero, self.YZ_ViewPads_Width ), curses.newpad( self.MaxLevelOverZero, self.YZ_ViewPads_Width ) )
        except curses.error:
            self.debug( "detected error in second try{} of initView()" )
            return False
            
        self.HeightOfWorldWindow = self.World.getmaxyx()[0] - 1
        self.WidthOfWorldWindow = self.World.getmaxyx()[1] - 1
        self.debug("self.HeightOfWorldWindow: " + str(self.HeightOfWorldWindow))
        self.debug("self.WidthOfWorldWindow: " + str(self.WidthOfWorldWindow))
        
        self.XZ_View_Height = self.XZ_View.getmaxyx()[0]
        self.XZ_View_Width = self.XZ_View.getmaxyx()[1]
        
        self.YZ_View_Height = self.YZ_View.getmaxyx()[0]
        self.YZ_View_Width = self.YZ_View.getmaxyx()[1]
        
        self.HeightOfServer_MsgContentWindow = self.Server_MsgContent.getmaxyx()[0]
        self.WidthOfServer_MsgContentWindow = self.Server_MsgContent.getmaxyx()[1]

        #!test! can be deleted later:
        #self.Debug_TestPad = curses.newpad( 1000, 1000 )
        #for Y in range(0, 999):
        #    for X in range(0, 999):
        #        self.Debug_TestPad.addch( Y, X, "X" )
        #


#        self.PositionOfTheLowerRightCornerInWorld_Y = self.WorldFrame.getmaxyx()[0]
#        self.PositionOfTheLowerRightCornerInWorld_X = self.WorldFrame.getmaxyx()[1]

        #self.setWindows()
        #seems, there is an own function in python-curses for that:
        #self.drawBorders()

        #curses.scrollok(ChatHistory, True)
        #not working like this in pyton
        self.ChatHistory.scrollok(True)

        curses.noecho()
        curses.curs_set(0)

        #curses.init_pair(n, curses.SymbolCLR, curses.background)
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)    #standard
        curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)    #for leafsAndTwigs 
        curses.init_pair(3, curses.COLOR_YELLOW, curses.COLOR_BLACK)    #for sand
        curses.init_pair(4, curses.COLOR_BLUE, curses.COLOR_BLACK)    #for water
        curses.init_pair(5, curses.COLOR_WHITE, curses.COLOR_BLACK)    # standard inverse
        curses.init_pair(6, curses.COLOR_BLACK, curses.COLOR_RED)
        curses.init_pair(7, curses.COLOR_BLUE, curses.COLOR_WHITE)

        #now the same with bold
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)    #standard
        pass
        #trying to make it possible to draw non-ascii-chars
        pass
        pass
        #defining MiddleSquare:
        self.Y_DistanceUpperLeftCornerOfMiddleSquare = int(self.HeightOfWorldWindow / 2.5)
        self.X_DistanceUpperLeftCornerOfMiddleSquare = int(self.WidthOfWorldWindow / 2.5)
        self.HeightOfMiddleSquare = int(self.HeightOfWorldWindow / 2.5)
        self.WidthOfMiddleSquare = int(self.WidthOfWorldWindow / 2.5)

    def drawBorders(self):
        #make borders
        self.Ping_Frame.border(0)
        self.Server_Msg_Frame.border(0)
        self.Heartbleed_Frame.border(0)
        self.WorldFrame.border(0)
        self.ChatHistoryFrame.border(0)
        self.ChatInputFrame.border(0)
        self.InfoFrame.border(0)
        pass
        #write something into it
        try:
            #self.Server_Msg_Frame.addstr(1, 1, "Server_Msg_Frame")
            #self.Server_MsgContent.addstr(0, 0, "Server_MsgContent")
            #self.WorldFrame.addstr(1, 1, "WorldFrame")
            #self.World.addstr(0,0,"World")
            #self.ChatHistory.addstr(0, 0, "ChatHistory_Content")
            #self.ChatHistoryFrame.addstr(1,1, "ChatHistoryFrame")
            #self.InfoFrame.addstr(1, 1, "InfoFrame")
            #self.Info.addstr(0, 0, "Info")
            self.XZ_View.addstr(0, 0, "XZ_View");
            self.YZ_View.addstr(0, 0, "YZ_View");
        except curses.error:
            pass
            self.debug("drawBorders(): init windows failed. probably because of terminal-size-issues")
            #return False
        pass
        #refresh
        self.Ping_Frame.refresh()
        self.Server_Msg_Frame.refresh()
        self.Server_MsgContent.refresh()
        self.Heartbleed_Frame.refresh()
        self.Heartbleed.refresh()
        self.WorldFrame.refresh()
        #self.World.refresh()
        self.ChatHistoryFrame.refresh()
        self.ChatHistory.refresh()
        self.ChatInputFrame.refresh()
        self.ChatInput.refresh()
        self.InfoFrame.refresh()
        self.Info.refresh()
        #just for testing:
        self.XZ_View.refresh()
        self.YZ_View.refresh()


    def incrementPointerToOldChatInput(self):
        #self.PointerToOldChatMessage = self.PointerToOldChatMessage + 1
        if self.OffsetFromTheLastMessageInListToTheOneToDisplay < len(self.OldChatInputs):
            self.OffsetFromTheLastMessageInListToTheOneToDisplay = self.OffsetFromTheLastMessageInListToTheOneToDisplay + 1
        
    def decrementPointerToOldChatInput(self):
        Output = False
        if self.OffsetFromTheLastMessageInListToTheOneToDisplay > 1:
            self.OffsetFromTheLastMessageInListToTheOneToDisplay = self.OffsetFromTheLastMessageInListToTheOneToDisplay - 1
            Output = True
        return Output
        

    def showOldMessageInChat(self):
        self.debug("self.OffsetFromTheLastMessageInListToTheOneToDisplay: " + str(self.OffsetFromTheLastMessageInListToTheOneToDisplay))
        if len( self.OldChatInputs ) > 0:
            self.CurrentlyShowingOldChatMessage = True
            self.ChatInput.erase()
            self.ChatInput.addstr( 0, 0, self.OldChatInputs[ len(self.OldChatInputs) - self.OffsetFromTheLastMessageInListToTheOneToDisplay ] )
            self.ChatInput.refresh()


    def clearChatInput(self):
        self.ChatInput.erase()
        self.ChatInput.refresh()
        self.CurrentlyShowingOldChatMessage = False

    def getCurrentlyShowedOldMessage(self):
        self.debug("entering getCurrentlyShowedOldMessage(): self.OffsetFromTheLastMessageInListToTheOneToDisplay: " + str(self.OffsetFromTheLastMessageInListToTheOneToDisplay))
        if self.CurrentlyShowingOldChatMessage:
            TypedMessage = self.OldChatInputs[ len(self.OldChatInputs) - self.OffsetFromTheLastMessageInListToTheOneToDisplay ]
            if self.OffsetFromTheLastMessageInListToTheOneToDisplay != 1 and TypedMessage != "":
                self.OldChatInputs.append(TypedMessage)
            ToWriteMessage = "[" + str(time.time()) + "]: " + str(TypedMessage) + "\n"
            with open("log/receivedChatInput.log", 'a') as out:
                out.write( ToWriteMessage )
        else:
            self.debug("takeOldMessageAsNew()..  wut?")
            exit()
        self.OffsetFromTheLastMessageInListToTheOneToDisplay = 0
        return TypedMessage


    def enterChatMode(self):    #call from controller::ChatMode()
        self.InChatMode = True
        self.ChatInput.erase()
        self.ChatInput.move(0,0)                #set curser to beginn of input-line
        curses.echo()                           #see what we are typeing
        self.ChatInput.keypad(0)                #quote: window.keypad(yes) "If yes is 1, escape sequences generated by some keys (keypad, function keys) will be interpreted by curses. If yes is 0, escape sequences will be left as is in the input stream."
        curses.curs_set(1)                      #make curser visible
        TypedMessage=self.ChatInput.getstr()    #get input
        
        self.OldChatInputs.append(TypedMessage)
        #self.PointerToOldChatMessage = len( self.OldChatInputs ) - 2
        
        ToWriteMessage = "[" + str(time.time()) + "]: " + str(TypedMessage) + "\n"
        with open("log/receivedChatInput.log", 'a') as out:
                out.write( ToWriteMessage )
        
        self.ChatInput.erase()                  #clear again
        self.ChatInput.refresh()                #refresh
        curses.noecho()                         #clean up
        curses.curs_set(0)                      #clean up
        self.InChatMode = False
        self.OffsetFromTheLastMessageInListToTheOneToDisplay = 0
        return TypedMessage

    def waitForEvent(self):        #call from eventHandler
        self.WorldFrame.move(1,1)
        self.WorldFrame.nodelay(False)        #the getch will wait until the user pressed a key
        KeyboardInput = -1
        LastInputFromKeyboard = 0
        
        curses.flushinp()                       #well this seems to help against those uncontrolled "primary-attack"-triggers. but still have graphical hickups
                                                 #but with that, i cant use the up arrow any more..
                                                 
                                                 
        KeyboardInput = self.WorldFrame.getch()    #will block
        LastInputFromKeyboard = KeyboardInput
        
        
        #it seems, that the next block causes the major amount of graphical hick.ups in curses. but even with out that, i get some..
#        #all this following stuff is needed to make sure, that the character will stop to move right after you released the button for the movement!
#        self.World.nodelay(True)  #the getch will not wait until the user pressed a key       
#        while KeyboardInput != -1:        #empty the stuff which is still in the pipe
#            #MutexForOutput.lock();  
#            KeyboardInput = self.World.getch()    #will not block
#            #MutexForOutput.unlock();
#        self.World.nodelay(False)         #will block again
#        KeyboardInput = LastInputFromKeyboard      #write the first catched button back into the buffer
#        # acutally, workes much better without
        
        Output = self.interpretKeyboardInput(KeyboardInput)
        pass
        ToWriteMessage = "[" + str(time.time()) + "]: " + str(KeyboardInput) + " (" + chr(KeyboardInput) + ")" + "\n"
        with open("log/keyinput.log", 'a') as out:
            out.write( ToWriteMessage )
        pass 
        return Output

    def interpretKeyboardInput(self, In_KeyboardInput):    #call from waitForEvent
        Output = "none"
        if In_KeyboardInput == 97:            #a
            Output = "moveLeft"
        elif In_KeyboardInput == 100:           #d
            Output = "moveRight"
        elif In_KeyboardInput == 119:           #w
            Output = "moveUp"
        elif In_KeyboardInput == 115:           #s
            Output = "moveDown"
        elif In_KeyboardInput == 113:           #q
            Output = "moveUpLeft"    
        elif In_KeyboardInput == 101:           #e
            Output = "moveUpRight"    
        elif In_KeyboardInput == 121:           #y
            Output = "moveDownLeft"    
        elif In_KeyboardInput == 99:           #c
            Output = "moveDownRight"    
        elif In_KeyboardInput == 116:          #t
            Output = "chatMode"    
        #elif In_KeyboardInput == 112:           #p        #not implementet yet
        #    Output = "scrollChatUp"    
        #elif In_KeyboardInput == 110:           #n
        #    Output = "scrollChatDown"    
        elif In_KeyboardInput == 122:            #z
            Output = "exitGame"    
        elif In_KeyboardInput == 114:        #r
            Output = "zoomUp"
        elif In_KeyboardInput == 118:        #v
            Output = "zoomDown"
        elif In_KeyboardInput == 102:        #f
            Output = "resetZoom"
        elif In_KeyboardInput == 112:         #p
            Output = "rebuildView"
        elif In_KeyboardInput == 49:        #1
            self.InfoScreenPage = 1
            self.updateInfoScreen()
        elif In_KeyboardInput == 50:        #2
            self.InfoScreenPage = 2
            self.updateInfoScreen()
        elif In_KeyboardInput == 51:        #3
            self.InfoScreenPage = 3
            self.updateInfoScreen()
        elif In_KeyboardInput == 9:        #tab
            Output = "tabulator"
        elif In_KeyboardInput == 27:        #esc
            Output = "ESC"
        elif In_KeyboardInput == 120:        #x
            Output = "PrimaryAttack"
        elif In_KeyboardInput == 88:        #X
            Output = "SecondaryAttack"
        elif In_KeyboardInput == 111:      #o
            #self.beep()
            Output = "deleteSightField"
#        elif In_KeyboardInput == 117:       #u
#            self.testingPad()
        elif In_KeyboardInput == 65:        #A
            #self.flashIt()
            if self.NextInputCommingIsFunctionKey:
                Output = "ArrowUp"
        elif In_KeyboardInput == 66:        #B
            if self.NextInputCommingIsFunctionKey:
                Output = "ArrowDown"
        elif In_KeyboardInput == 91:        #[
            if self.LastInput == 27:        # LastInput = esc
                self.NextInputCommingIsFunctionKey = True
        elif In_KeyboardInput == 10:
            Output = "Return"
        else:
            self.debug("SHICE! the registered key has no event-equivalent: In_KeyboardInput: " + str(In_KeyboardInput) + " (" + chr(In_KeyboardInput) + ")")
            self.flashIt()
        
        if In_KeyboardInput != 91:
            self.NextInputCommingIsFunctionKey = False
        
        self.LastInput = In_KeyboardInput
        return Output





    def displayNewChatMessage(self, In_Msg, In_DoNotLogMessage):
        if self.NumberOfReceivedChatMessages <= 3:
            try:
                self.ChatHistory.addstr(self.NumberOfReceivedChatMessages, 0, In_Msg)
                self.ChatHistory.refresh()
            except curses.error:
                pass
                self.debug("displayNewChatMessage(): (NumberOfReceivedChatMessages <= 3) init windows failed. probably because of terminal-size-issues")
                #return False
            pass
        elif self.NumberOfReceivedChatMessages > 3:
            pass
            try:
                self.ChatHistory.scroll(1)
                self.ChatHistory.addstr(3,0,In_Msg)
            except curses.error:
                pass
                self.debug("displayNewChatMessage(): (NumberOfReceivedChatMessages > 3) init windows failed. probably because of terminal-size-issues")
        self.NumberOfReceivedChatMessages=self.NumberOfReceivedChatMessages+1
        self.ChatHistory.refresh()
        pass
        if In_DoNotLogMessage == False:
            #this writes the chat.log
            Stream = open("log/chat.log", "a")
            Stream.write(In_Msg+"\n")
            Stream.close()

    def wait(self):
        self.Screen.getch()

    def restoreChatHistory(self):
        try:
            for Line in open("log/chat.log"):
                #self.debug("restroeChatHistory: " + Line)
                Line = Line.rstrip("\n")
                self.displayNewChatMessage(Line, True)
        except OSError:
            self.debug("couldn't find chat.log . creat file now!")
            os.system("bash -c \"> log/chat.log\"")




    def printInfoScreenPage1(self):
            try:
                self.Info.erase()
                self.Info.addstr( 1, 0, "\tPosY: " + str(self.CharacterBackup.PosY) )
                self.Info.addstr( 2, 0, "\tPosX: " + str(self.CharacterBackup.PosX) )
                self.Info.addstr( 3, 0, "\tPoxZ: " + str(self.CharacterBackup.PosZ) )
                self.Info.addstr( 4, 0, "\tEnergy: " + str(self.CharacterBackup.Energy) )
                self.Info.addstr( 5, 0, "\tSpeed: " + "blank" )
                self.Info.addstr( 6, 0, "\tIndexWorldPad: " + str(self.IndexOfCurrentWorldPad) )
                self.Info.addstr( 7, 0, "\tSzenario: " + str(self.Szenario) )
                self.Info.addstr( 8, 0, "\tTarget: " + str(self.Target_String) )
                self.Info.addstr( 9, 0, "\tDbg0: " + str(self.Debug0) )
                self.Info.addstr( 10, 0, "\tDbg1: " + str(self.Debug1) )
                #self.Info.addstr( 11, 0, "\tDbg2: " + str(self.Debug2) )
                #self.Info.addstr( 12, 0, "\tDbg3: " + str(self.Debug3) )
                #self.Info.addstr( 13, 0, "\tDbg4: " + str(self.Debug4) )
                #self.Info.addstr( 14, 0, "\tDbg5: " + str(self.Debug5) )
                #self.Info.addstr( 15, 0, "\tDbg6: " + str(self.Debug6) )
                #self.Info.addstr( 16, 0, "\tDbg7: " + str(self.Debug7) )
                self.Info.refresh()
            except curses.error:
                self.flashIt()
                self.debug("updateInfoScreen(): init windows failed. probably because of terminal-size-issues")
            pass








    def updateInfoScreen(self):
        self.debug("entering updateInfoSreen()")
        if self.Old_InfoScreenPage != self.InfoScreenPage:      #if the page has changed, we need to reprint some stuff. but only once... that's why we set self.Old_InfoScreenPage to the current value at the end of this funktion
            #self.flashIt()
            if self.InfoScreenPage == 1:
                #self.InfoFrame.erase()
                self.InfoFrame.border(0)
                self.InfoFrame.addstr( 0, 1, "1", curses.color_pair(6))
                self.InfoFrame.addstr( 0, 2, "2" )
                self.InfoFrame.addstr( 0, 3, "3" )
                self.InfoFrame.refresh()
            if self.InfoScreenPage == 2:
                #self.InfoFrame.erase()
                self.InfoFrame.border(0)
                self.InfoFrame.addstr( 0, 1, "1" )
                self.InfoFrame.addstr( 0, 2, "2", curses.color_pair(6))
                self.InfoFrame.addstr( 0, 3, "3" )
                self.InfoFrame.refresh()
            if self.InfoScreenPage == 3:
                #self.InfoFrame.erase()
                self.InfoFrame.border(0)
                self.InfoFrame.addstr( 0, 1, "1" )
                self.InfoFrame.addstr( 0, 2, "2" )
                self.InfoFrame.addstr( 0, 3, "3", curses.color_pair(6))
                self.InfoFrame.refresh()
        
        if self.InfoScreenPage == 1:
            self.printInfoScreenPage1()
        if self.InfoScreenPage == 2:
            #alright, here is the problem: while printXZ_View() is fast enough, pintYZ_View() is slow as ass. 
            #thats because, there must be a pad-overlay done for EVERY SYMBOL in the view
            #i try it with makeing an own thrad for that one.. by that, at least it doesn't block play-flow...
            #yep, thats seems to work well. 
            #why not doing the same with XZ_View?
            
            
            #what if a thread is already running? then we need to kill that thread, to make sure, only one thread is running
            #if self.Mutex_ThreadStillRunning.acquire(False) == False:       #not blocking. might lock, if not already running
            #    #already running
            #    self.debug("Thread is already running. set variable to kill it now!")
            #    self.flashIt()
            #    self.Kill_Thread_printYZ_View = True                        #this will skipp everything in the running thread (it is not possible to directly kill a thread in python
            #    self.Mutex_ThreadStillRunning.acquire(True)                 #blocking. waiting here, until thread is unlocked ( released )
            #    self.Kill_Thread_printYZ_View = False                       #reset again
            #else:
            #    self.debug("there was no thread running")
            #in case, the acquire() returned False (means, the thread was already running) the Mutex got locked after waiting for the running thread to be 'killed' (acutally only be finished, but faster then normal)
            #in case, the acquire() returned True (means there was no thread running) the acquire() already locked the mutex
            #in both cases the mutex is locked and the thread can be started now
            #self.debug("starting new thread with self.printYZ_View()")
            Thread_printYZ_View = Thread( target=self.printYZ_View, args=() )
            Thread_printYZ_View.start()
            #well.. actually it wasn't that slow at all. it was only slow because i did debug-output in a file. 
            #without that is really snappe.. maybe i remove that thrad-thing again... but its working alright now..
            
            #same for XZ_View:
            #but acutally... printXZ_View is so fast... dont need it here
            #if self.Thread_printXZ_View != -1:
            #    self.Kill_Thread_printXZ_View = True
            #    while self.Thread_printXZ_View != -1:
            #        self.debug("waiting for XZ_Thread to be killed")
            #    self.Kill_Thread_printXZ_View = False
            #
            Thread_printXZ_View = Thread( target=self.printXZ_View, args=() )
            Thread_printXZ_View.start()
            #for testing, withouth thread
            #self.printXZ_View()
  
        if self.InfoScreenPage == 3:
            pass
            #missing: stuff to display on page 3
            
        self.Old_InfoScreenPage = self.InfoScreenPage
        self.debug("leaving updateInfoScreen()")

    def printYZ_View(self):
        self.debug("entering printYZ_View()")
        #self.Mutex_ThreadStillRunning.acquire(False)     #lock, not blocking. because it might already be locked after waiting for beeing released
        self.YZ_View.erase()
        for Index in range( 0, self.YZ_View_Width ):
            #if self.Kill_Thread_printYZ_View == False:
            Relative = ( (-int(self.YZ_View_Width/2)) + Index )
            #self.debug("Relative: " + str(Relative))
            self.printColomnIn_YZ_View(Relative)
            #else:
            #    self.debug("skipping rest of printYZ_View() because thread has been requested to be killed")
        self.YZ_View.addstr(0, 0, "YZ View:")
        self.YZ_View.refresh()
        #self.Thread_printYZ_View = -1
        #self.Mutex_ThreadStillRunning.release()     #unlock
        self.debug("leaving printYZ_View()")
        
        
        
    def printColomnIn_YZ_View(self, In_ColomnRelativeToCharacterPosition):     #just a single vertical line of symbols. its complicated enough in YZ_View
        self.debug("entering printColomnIn_YZ_View(). In_ColomnRelativeToCharacterPosition: " + str(In_ColomnRelativeToCharacterPosition))
        MinLineToPrint = -1     #
        MaxLineToPrint = -1     #both parameters refer to YZ_View (the window) not the Z-level or the position on pad
        
        #missing, das muss sp�ter noch nachgetragen werden. ich setzte das jetzt erstmal auf false zum testen
        CharacterIsNearTheEdge = False
        
        Pmin_Y = (self.CharacterBackup.PosY % self.HeightOfWorldPad) + In_ColomnRelativeToCharacterPosition
        Pmin_X = self.CharacterBackup.PosX % self.WidthOfWorldPad
        Smin_Y = -1
        Smin_X = -1
        Smax_Y = -1
        Smax_X = -1
        Smin_Y_static = -1
        Smax_Y_static = -1
        
        LayerToPrint = -1
        Y_PositionOfCharacterInWindow = -1
        
        if Pmin_Y >= 0:     #on same pad as character or one below
            PadIndexToPrint = self.IndexOfCurrentWorldPad
        else:               #not on the same pad as the character, but one above
            self.debug("not on the same pad as the character")
            Pmin_Y = Pmin_Y + self.HeightOfWorldPad
            PadIndexToPrint = self.getNumberOfPadOnSideOfCurrentPad("up")
            
        if Pmin_Y >= self.HeightOfWorldPad:  #one below
            #self.flashIt()
            Pmin_Y = Pmin_Y % self.HeightOfWorldPad
            PadIndexToPrint = self.getNumberOfPadOnSideOfCurrentPad("down")
            
        if self.YZ_View_Height >= self.MaxLevelOverZero:
            #self.debug("case that the view is higher then the available layers")
            MinLineToPrint = int( (self.YZ_View_Height - self.MaxLevelOverZero)/2 )
            MaxLineToPrint = MinLineToPrint + self.MaxLevelOverZero
            LayerToPrint = self.MaxLevelOverZero
            
            if CharacterIsNearTheEdge == False:
                #self.debug("CharacterIsNearTheEdge == False") 
                Smin_X = int(self.YZ_View_Width/2) + In_ColomnRelativeToCharacterPosition
                Smax_X = Smin_X
        else:
            self.debug("case that the view is not as high as the available layers")
            MinLineToPrint = 0
            MaxLineToPrint = self.YZ_View_Height
            
        #self.debug("self.YZ_View_Height: " + str(self.YZ_View_Height))
        #self.debug("self.YZ_View_Width: " + str(self.YZ_View_Width))
        #self.debug("MinLineToPrint: " + str(MinLineToPrint))
        #self.debug("MaxLineToPrint: " + str(MaxLineToPrint))
        for Layer in range(MinLineToPrint, MaxLineToPrint):
            Smin_Y = Layer
            Smax_Y = Layer
            
            LayerToPrint -= 1
            
            #!!! do not delete. that can be handy at some time
            #self.debug("")
            #self.debug( "Layer: " + str( Layer ) )
            #self.debug( "Pmin_Y: " + str( Pmin_Y ) )
            #self.debug( "Pmin_X: " + str( Pmin_X ) )
            #self.debug( "Smin_Y: " + str( Smin_Y ) )
            #self.debug( "Smin_X: " + str( Smin_X ) )
            #self.debug( "Smax_Y: " + str( Smax_Y ) )
            #self.debug( "Smax_X: " + str( Smax_X ) )
            #self.debug( "LayerToPrint: " + str(LayerToPrint) )
            
            self.WorldPads_Meta[PadIndexToPrint][LayerToPrint][0].overlay( self.YZ_View, Pmin_Y, Pmin_X, Smin_Y, Smin_X, Smax_Y, Smax_X )
            if LayerToPrint == self.CharacterBackup.PosZ:
                Y_PositionOfCharacterInWindow = Smin_Y
        if In_ColomnRelativeToCharacterPosition == 0:       # @-symbol needs to be printed
            self.YZ_View.addch( Y_PositionOfCharacterInWindow, int(self.YZ_View_Width/2), '@')
        self.debug("leaving printColomnIn_YZ_View")


    def printXZ_View(self):
        PositionOnPad = []                                                      # zeroest element will be y, first element, will be x
        PositionOnPad.append(self.CharacterBackup.PosY % self.HeightOfWorldPad)
        PositionOnPad.append(self.CharacterBackup.PosX % self.WidthOfWorldPad)
        self.XZ_View.erase()

        if self.CharacterBackup.PosX > int(self.XZ_View_Width/2 ):#- 1):               #is the Character far away from the edge?
                                                                                    # the minus 1 because we have the frame on the left side
            self.debug("XZ_View.getmaxyx()" + str( self.XZ_View.getmaxyx() ) )
            if (self.XZ_View_Height) >= self.MaxLevelOverZero:                      #if the view is higher then the amount of layers (z-levels)
                self.debug("XZ_View is high enough to handle all the layers")
                self.debug("self.XZ_View_Height >= self.MaxLevelOverZero")
                
                NumberOfPadsMustBePrinted = 1
                #first round
                Pmin_Y = int( PositionOnPad[0] )
                Pmin_X = int( PositionOnPad[1]-(self.XZ_View_Width/2) )
                Smin_Y = int( (int(self.XZ_View_Height-2) - int(self.MaxLevelOverZero)) / 2 ) + self.MaxLevelOverZero + 1   #don't know why '+1'
                Smin_X = 0
                Smax_Y = int( (int(self.XZ_View_Height-2) - int(self.MaxLevelOverZero)) / 2 ) + self.MaxLevelOverZero + 1   #same
                Smax_X = int( (self.XZ_View_Width) ) - 1        #well, it's a lot trail and error
                PadIndexToPrint = self.IndexOfCurrentWorldPad
                AmountOfPadsMustBePrinted = 1
                SideWhereToPlaceAnotherPad = ""
                
                
                if int(self.CharacterBackup.SightRadius) < int(self.XZ_View_Width/2):               #mostly the case, if the terminal is big, linke fully maximized on my thinkpad
                    self.debug("catching case: to near to the edge")
                    
                    #what if we are to close to the edge of a pad? need to catch that:
                    if ( Pmin_X + Smax_X ) >= self.WidthOfWorldPad:          #if we are near the right edge of a pad
                        #self.flashIt()
                        self.debug("near to the right edge")
                        Smax_X = self.WidthOfWorldPad - Pmin_X
                        AmountOfPadsMustBePrinted = 2                   #so we are doing the whole loop once again, to print the other pad
                        SideWhereToPlaceAnotherPad = "right"
                    elif Pmin_X < 0:
                        #self.flashIt()
                        self.debug("near to the left edge")
                        Smin_X = Pmin_X * (-1)
                        Pmin_X = 0
                        AmountOfPadsMustBePrinted = 2
                        SideWhereToPlaceAnotherPad = "left"
                    else:
                        self.debug("not near to any edge!")
                else:
                    self.debug("fissssshhhhyyy. error 0")
                    #flashIt()

                #actually printing: 
                PositionOfCharacterInPad = -1
                for Number in range(0, AmountOfPadsMustBePrinted):      #if we are only printing one pad, then 'AmountOfPadsMustBePrinted' is still 1, and we are doing the whole shit just once.
                                                                        #if we need to print two pads, then NumberOfPads Will be set to '2' and we are doing it again for the other pad
                    self.debug("Number: " + str(Number))

                    if Number == 1:                         #second round, if needed
                        #self.flashIt()
                        self.debug("entering Number 1")
                        if SideWhereToPlaceAnotherPad == "right":
                            PadIndexToPrint = self.getNumberOfPadOnSideOfCurrentPad("right")
                            #missing: parameters for the second pad to print...
                            #Pmin_Y stayes the same
                            Pmin_X = 0
                            Smin_Y = int( (int(self.XZ_View_Height-2) - int(self.MaxLevelOverZero)) / 2 ) + self.MaxLevelOverZero + 1   #same as in first round
                            Smin_X = self.WidthOfWorldPad - int( PositionOnPad[1]-(self.XZ_View_Width/2) )                              #where the first round stoped
                            Smax_Y = Smin_Y #int( (int(self.XZ_View_Height-2) - int(self.MaxLevelOverZero)) / 2 ) + self.MaxLevelOverZero + 2   #same as in first round
                            Smax_X = int( (self.XZ_View_Width) ) - 1                                                                    #as in first round
                        elif SideWhereToPlaceAnotherPad == "left":
                            #self.flashIt()
                            PadIndexToPrint = self.getNumberOfPadOnSideOfCurrentPad("left")
                            #Pmin_Y stayes the same
                            Smax_X = Smin_X - 1 #without that '-1' i get black stripes
                            Pmin_X = self.WidthOfWorldPad - Smin_X
                            Smin_Y = int( (int(self.XZ_View_Height-2) - int(self.MaxLevelOverZero)) / 2 ) + self.MaxLevelOverZero + 1    #same as in first round
                            Smin_X = 0
                            Smax_Y = int( (int(self.XZ_View_Height-2) - int(self.MaxLevelOverZero)) / 2 ) + self.MaxLevelOverZero + 1   #same as in first round
                        else:
                            self.debug("something fishy, going through the pads in xz-view")

                    self.debug("XZ_View.getmaxyx()" + str(self.XZ_View.getmaxyx()))
                    for Layer in range(0, self.MaxLevelOverZero):
                        Smin_Y = Smin_Y - 1
                        Smax_Y = Smax_Y - 1
                        
                        if Layer == self.CharacterBackup.PosZ:
                            #self.flashIt()
                            PositionOfCharacterInPad = Smin_Y
                        
                        #!!! do not delete. that can be handy at some time
                        #self.debug("")
                        #self.debug( "Pmin_Y: " + str( Pmin_Y ) )
                        #self.debug( "Pmin_X: " + str( Pmin_X ) )
                        #self.debug( "Smin_Y: " + str( Smin_Y ) )
                        #self.debug( "Smin_X: " + str( Smin_X ) )
                        #self.debug( "Smax_Y: " + str( Smax_Y ) )
                        #self.debug( "Smax_X: " + str( Smax_X ) )
                        
                        self.WorldPads_Meta[PadIndexToPrint][Layer][0].overlay( self.XZ_View, Pmin_Y, Pmin_X, Smin_Y, Smin_X, Smax_Y, Smax_X )
                
                #self.XZ_View.addch( int(self.XZ_View_Height/2) + ( int(self.MaxLevelOverZero/2) - self.CharacterBackup.PosZ ), int(self.XZ_View_Width/2), "@" )
                #self.XZ_View.addch( PositionOfCharacterInPad, int(self.XZ_View_Width/2), "@" )
                try:
                    self.XZ_View.addch( PositionOfCharacterInPad , int(self.XZ_View_Width/2), "@" )
                    self.debug("print '@'-symbol on position: Y: " + str(PositionOfCharacterInPad) + " ; X: " + str(PositionOfCharacterInPad))
                except:
                    self.debug("couldn't print '@'-symbol. Y: " + str(PositionOfCharacterInPad) + " ; X: " + str(int(self.XZ_View_Width/2)))

            else:       #what to do if the XZ_View_Window is not big enough ( in y- dimension) to handle all the Layers
                self.debug("XZ_View_Window is not big enough ( in y- dimension) to handle all the Layers")
                self.debug("getmaxyx: " + str(self.XZ_View.getmaxyx()))
                
                NumberOfPadsMustBePrinted = 1
                Pmin_Y = PositionOnPad[0]
                Pmin_X = int( PositionOnPad[1]-(self.XZ_View_Width/2) )
                Smin_Y = self.XZ_View_Height
                Smin_X = 0
                Smax_Y = self.XZ_View_Height
                Smax_X = self.XZ_View_Width - 1
                AmountOfPadsMustBePrinted = 1
                PadIndexToPrint = self.IndexOfCurrentWorldPad
                
                PositionOfCharacterInPad = -1
                #printing:
                for Layer in range(3, self.XZ_View_Height):
                    Smin_Y -= 1
                    Smax_Y -= 1
                    if Layer == self.CharacterBackup.PosZ:
                        PositionOfCharacterInPad = Smin_Y
                    
                    #!!! do not delete. that can be handy at some time
                    #self.debug("")
                    #self.debug( "Pmin_Y: " + str( Pmin_Y ) )
                    #self.debug( "Pmin_X: " + str( Pmin_X ) )
                    #self.debug( "Smin_Y: " + str( Smin_Y ) )
                    #self.debug( "Smin_X: " + str( Smin_X ) )
                    #self.debug( "Smax_Y: " + str( Smax_Y ) )
                    #self.debug( "Smax_X: " + str( Smax_X ) )
                    
                    self.WorldPads_Meta[PadIndexToPrint][Layer][0].overlay( self.XZ_View, Pmin_Y, Pmin_X, Smin_Y, Smin_X, Smax_Y, Smax_X )
                try:
                    self.XZ_View.addch( PositionOfCharacterInPad , int(self.XZ_View_Width/2), "@" )
                    self.debug("print '@'-symbol on position: Y: " + str(PositionOfCharacterInPad) + " ; X: " + str(PositionOfCharacterInPad))
                except:
                    self.debug("couldn't print '@'-symbol. Y: " + str(PositionOfCharacterInPad) + " ; X: " + str(int(self.XZ_View_Width/2)))
        else:
            self.debug("near to the edge")
            #if character is neer the left edge
            if self.XZ_View_Height >= self.MaxLevelOverZero:    #if the view is higher then the amount of layers (z-levels)
                
                PositionOfCharacterInPad_Y = -1
                PositionOfCharacterInPad_X = self.CharacterBackup.PosX
                
                PadIndexToPrint = self.IndexOfCurrentWorldPad
                
                Pmin_Y = int( PositionOnPad[0] )
                Pmin_X =  0 #int( PositionOnPad[1]-(self.XZ_View_Width/2) )
                Smin_Y = int( (int(self.XZ_View_Height-2) - int(self.MaxLevelOverZero)) / 2 ) + self.MaxLevelOverZero + 1   #don't know why '+1'
                Smin_X = 0
                Smax_Y = int( (int(self.XZ_View_Height-2) - int(self.MaxLevelOverZero)) / 2 ) + self.MaxLevelOverZero + 1   #same
                Smax_X = int( (self.XZ_View_Width) ) - 1        #well, it's a lot trail and error
                
                for Layer in range(0, self.MaxLevelOverZero):
                    Smin_Y = Smin_Y - 1
                    Smax_Y = Smax_Y - 1
                    
                    if Layer == self.CharacterBackup.PosZ:
                        #self.flashIt()
                        #self.debug("Layer == self.CharacterBackup.PosZ; Smin_Y: " + str(Smin_Y))
                        PositionOfCharacterInPad_Y = Smin_Y
                    
                    self.debug("")
                    self.debug( "Pmin_Y: " + str( Pmin_Y ) )
                    self.debug( "Pmin_X: " + str( Pmin_X ) )
                    self.debug( "Smin_Y: " + str( Smin_Y ) )
                    self.debug( "Smin_X: " + str( Smin_X ) )
                    self.debug( "Smax_Y: " + str( Smax_Y ) )
                    self.debug( "Smax_X: " + str( Smax_X ) )
                    
                    self.WorldPads_Meta[PadIndexToPrint][Layer][0].overlay( self.XZ_View, Pmin_Y, Pmin_X, Smin_Y, Smin_X, Smax_Y, Smax_X )
                try:
                    self.XZ_View.addch( PositionOfCharacterInPad_Y, PositionOfCharacterInPad_X, "@" )
                except:
                    self.debug("couldn't print '@'-symbol. Y: " + str(PositionOfCharacterInPad_Y) + " ; X: " + str(PositionOfCharacterInPad_X))
                    
            else:                                               #what to do if the XZ_View_Window is not big enough ( in y- dimension) to handle all the Layers
                self.debug("XZ_View_Window is not big enough ( in y- dimension) to handle all the Layers")
                
                
        self.XZ_View.addstr(0, 0, "XZ View:")
        self.XZ_View.refresh()
        #self.Thread_printXZ_View = -1

                



    def getNumberOfPadOnSideOfCurrentPad(self, In_Direction):
        Output = -1
        if self.Szenario == "A":
            if self.IndexOfCurrentWorldPad == 0:
                if In_Direction == "right":
                    Output = 1
                elif In_Direction == "left":
                    Output = -1                 #not possible
                elif In_Direction == "up":
                    Output = -1                 #not possible
                elif In_Direction == "down":
                    Output = 2
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 0")
            elif self.IndexOfCurrentWorldPad == 1:
                if In_Direction == "left":
                    Output = 0
                elif In_Direction == "down":
                    Output = 3
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 1")
            elif self.IndexOfCurrentWorldPad == 2:
                if In_Direction == "right":
                    Output = 3
                elif In_Direction == "up":
                    Output = 0
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 2")
            elif self.IndexOfCurrentWorldPad == 3:
                if In_Direction == "left":
                    Output = 2
                elif In_Direction == "up":
                    Output = 1
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 3")
        elif self.Szenario == "B":
            if self.IndexOfCurrentWorldPad == 0:
                if In_Direction == "right":
                    Output = 1
                elif In_Direction == "up":
                    Output = 2
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 4")
            elif self.IndexOfCurrentWorldPad == 1:
                if In_Direction == "left":
                    Output = 0
                elif In_Direction == "up":
                    Output = 3
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 5")
            elif self.IndexOfCurrentWorldPad == 2:
                if In_Direction == "right":
                    Output = 3
                elif In_Direction == "down":
                    Output = 0
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 6")
            elif self.IndexOfCurrentWorldPad == 3:
                if In_Direction == "left":
                    Output = 2
                elif In_Direction == "down":
                    Output = 1
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 7")
        elif self.Szenario == "C":
            if self.IndexOfCurrentWorldPad == 0:
                if In_Direction == "left":
                    Output = 1
                elif In_Direction == "up":
                    Output = 2
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 8")
            elif self.IndexOfCurrentWorldPad == 1:
                if In_Direction == "right":
                    Output = 0
                elif In_Direction == "up":
                    Output = 3
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 9")
            elif self.IndexOfCurrentWorldPad == 2:
                if In_Direction == "left":
                    Output = 3
                elif In_Direction == "up":
                    Output = 0
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 10")
            elif self.IndexOfCurrentWorldPad == 3:
                if In_Direction == "right":
                    Output = 2
                elif In_Direction == "down":
                    Output = 1
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 11")
        elif self.Szenario == "D":
            if self.IndexOfCurrentWorldPad == 0:
                if In_Direction == "left":
                    Output = 1
                elif In_Direction == "down":
                    Output = 2
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 12")
            elif self.IndexOfCurrentWorldPad == 1:
                if In_Direction == "right":
                    Output = 0
                elif In_Direction == "down":
                    Output = 3
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 13")
            elif self.IndexOfCurrentWorldPad == 2:
                if In_Direction == "left":
                    Output = 3
                elif In_Direction == "up":
                    Output = 0
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 14")
            elif self.IndexOfCurrentWorldPad == 3:
                if In_Direction == "right":
                    Output = 2
                elif In_Direction == "up":
                    Output = 1
                else:
                    self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 15")
            else:
                self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 16")
        else:
            self.debug("something fishy in getNumberOfPadOnSideOfCurrentPad(). error 17")
        return Output





    def drawSightRadius(self, In_SightRadius, In_PosY, In_PosX):                #it actually draws a frame/border
#        self.debug( "self.PositionOfTheLowerRightCornerInWorld_Y: " + str( self.PositionOfTheLowerRightCornerInWorld_Y ) + " ; self.self.PositionOfTheLowerRightCornerInWorld_X: " + str ( self.PositionOfTheLowerRightCornerInWorld_X ) )
#        self.debug("entering drawSightRadius() ")
        for Y in range(self.PositionOfTheUpperLeftCornerInWorld_Y, self.PositionOfTheUpperLeftCornerInWorld_Y + self.HeightOfWorldWindow+1):
            for X in range(self.PositionOfTheUpperLeftCornerInWorld_X, self.PositionOfTheUpperLeftCornerInWorld_X + self.WidthOfWorldWindow +1):
                if ( Y-int(In_PosY) )*( Y-int(In_PosY) ) + ( X-int(In_PosX))*(X-int(In_PosX) ) > ( int(In_SightRadius) ) * ( int(In_SightRadius) ) and ( Y-int(In_PosY) )*( Y-int(In_PosY) ) + ( X-int(In_PosX) )*( X-int(In_PosX) ) < ( int(In_SightRadius) + 2 ) * ( int(In_SightRadius) + 2 ):
                    self.SightFieldOverlay.addch( Y-int(self.PositionOfTheUpperLeftCornerInWorld_Y), X-int(self.PositionOfTheUpperLeftCornerInWorld_X), curses.ACS_CKBOARD, curses.color_pair(6) )
        self.SightFieldOverlay.overlay( self.World, 0, 0, 0, 0, self.HeightOfWorldWindow, self.WidthOfWorldWindow )


    def getRelativePositionOnScreenOfCharacter_Y(self):
        return int(self.CharacterBackup.PosY) - int(self.PositionOfTheUpperLeftCornerInWorld_Y)

    def getRelativePositionOnScreenOfCharacter_X(self):
        return int(self.CharacterBackup.PosX) - int(self.PositionOfTheUpperLeftCornerInWorld_X)



    def calculateOnWichQuarterOfTheCurrentPadTheCharacterIs(self, In_PosY, In_PosX):
        #self.debug("entering calculateOnWichQuarterOfTheCurrentPadTheCharacterIs() ")
        #means:we are only looking on ONE pad. and this funkction decides on which half in terms of verticallity and on which half in terms of horizontally the character sits..
        #
        #maybe i should make a backup of the varaibles on the beginning of this funktion, compare them after the funktion, and if they changes, i should erase the pad, which is affected
        #otherwise i get the cubes from a whole different area of the map on the pad...
        #
        #this is one pad
        #           left                right
        #    |-------------------|-------------------|                          ^
        #    |                   |                   |                          |
        #    |                   |                   |                          |
        #    |                   |                   |      upper               |
        #    |                   |                   |                          |
        #    |                   |                   |                          |
        #    |-------------------|-------------------|                          |self.HeightOfWorldPad
        #    |                   |                   |                          |
        #    |                   |                   |                          |
        #    |                   |                   |      lower               |
        #    |                   |                   |                          |
        #    |                   |                   |                          |
        #    |-------------------|-------------------|                          v
        #
        #    <--------------------------------------->
        #              self.WidhtOfWorldPad
        #
        #
        #Backup_CharacterIsOnLowerSideOfCurrentWorldPad = self.CharacterIsOnLowerSideOfCurrentWorldPad
        #Backup_CharacterIsOnUpperSideOfCurrentWorldPad = self.CharacterIsOnUpperSideOfCurrentWorldPad
        #Backup_CharacterIsOnRightSideOfCurrentWorldPad = self.CharacterIsOnRightSideOfCurrentWorldPad
        #Backup_CharacterIsOnLowerSideOfCurrentWorldPad = self.CharacterIsOnLowerSideOfCurrentWorldPad
        
        Temp_PosY = In_PosY % (self.HeightOfWorldPad)
        Temp_PosX = In_PosX % (self.WidthOfWorldPad)
        if Temp_PosY >= self.HeightOfWorldPad/2:                         #for example: Height is 4. -> Height/2 = 2 -> 0,1,2,3 . char has posY = 2 -> is on lower half
            self.CharacterIsOnLowerSideOfCurrentWorldPad = True
            self.CharacterIsOnUpperSideOfCurrentWorldPad = False
        else:
            self.CharacterIsOnLowerSideOfCurrentWorldPad = False
            self.CharacterIsOnUpperSideOfCurrentWorldPad = True
        if Temp_PosX >= self.WidthOfWorldPad/2:
            self.CharacterIsOnRightSideOfCurrentWorldPad = True
            self.CharacterIsOnLeftSideOfCurrentWorldPad = False
        else:
            self.CharacterIsOnRightSideOfCurrentWorldPad = False
            self.CharacterIsOnLeftSideOfCurrentWorldPad = True
            
#        if Backup_CharacterIsOnLowerSideOfCurrentWorldPad != self.CharacterIsOnLowerSideOfCurrentWorldPad or Backup_CharacterIsOnUpperSideOfCurrentWorldPad != self.CharacterIsOnUpperSideOfCurrentWorldPad or Backup_CharacterIsOnRightSideOfCurrentWorldPad != self.CharacterIsOnRightSideOfCurrentWorldPad or Backup_CharacterIsOnLowerSideOfCurrentWorldPad != self.CharacterIsOnLowerSideOfCurrentWorldPad:
#            self.flashIt()                                                                         #something has changed
#            for Index in range(0, 4):
#                #if Index is not self.IndexOfCurrentWorldPad:
#                if self.RecentlyReceivedCubesOnPad[Index] == False:
#                    self.clearPadsWithMetaIndex(Index)

        
        self.debug("leaving calculateOnWichQuarterOfTheCurrentPadTheCharacterIs() ")





    def clearPadsWithMetaIndex(self, In_PadsToClear):
        #self.flashIt()
        self.debug("entering clearPadsWithMetaIndex(). In_PadsToClear: " + str(In_PadsToClear))
        for Layer in range(0, self.MaxLevelOverZero):
            for IndexMapSet in range(0, 3):
                pass
                self.WorldPads_Meta[In_PadsToClear][Layer][IndexMapSet].erase()




    def calculateOnWichPadTheCharacterIsSittingRightNow(self, In_PosY, In_PosX):
        #self.debug("entering calculateOnWichPadTheCharacterIsSittingRightNow(): IndexOfCurrentWorldPad: " + str(self.IndexOfCurrentWorldPad))
        #Pads (per default):
        #
        #
        #
        #       |-----|-----|-----|-----|
        #       |     |     |     |     |                      
        #       |  3  |  2  |  3  |  3  |
        #       |     |     |     |     |
        #       |-----O-----|-----|-----|-------------  |
        #       |     |@    |     |     |     |         | we only see this..
        #       |  1  |  0  |  1  |  0  |  1  |         v
        #       |     |     |     |     |     |
        #       |-----|-----|-----|-----|-----|
        #       |     |     |     |     |     |
        #       |  3  |  2  |  3  |  2  |  3  |
        #       |     |     |     |     |     |
        #       |-----|-----|-----|-----|-----|
        #             |
        #             |
        #             |
        #
        #             ----> ...and this
        #
        CharacterIsOnPad_0_or_1 = False
        CharacterIsOnPad_2_or_3 = False
        CharacterIsOnPad = -1
        pass
        Temp_PosY = In_PosY % (2*self.HeightOfWorldPad)
        if Temp_PosY < self.HeightOfWorldPad:                   #character is on pad 0 or pad 1
            CharacterIsOnPad_0_or_1 = True
        else:                                                   #character is on pad 2 or pad 3
            CharacterIsOnPad_2_or_3 = True
        pass
        Temp_PosX = In_PosX % (2*self.WidthOfWorldPad)
        if Temp_PosX < self.WidthOfWorldPad:                    #character is on pad 0 or pad 2
            if CharacterIsOnPad_0_or_1 == True:
                self.IndexOfCurrentWorldPad = 0
            elif CharacterIsOnPad_2_or_3 == True:
                self.IndexOfCurrentWorldPad = 2
            else:
                self.debug("something went wrong in calculateOnWichPadTheCharacterIsSittingRightNow()")
                exit(0)
        else:                                                   #character is on pad 1 or pad 3
            if CharacterIsOnPad_0_or_1 == True:
                self.IndexOfCurrentWorldPad = 1
            elif CharacterIsOnPad_2_or_3 == True:
                self.IndexOfCurrentWorldPad = 3
            else:
                self.debug("something went wrong in calculateOnWichPadTheCharacterIsSittingRightNow()")
                exit(0)
        #self.debug("leaving calculateOnWichPadTheCharacterIsSittingRightNow(): IndexOfCurrentWorldPad: " + str(self.IndexOfCurrentWorldPad))


        

    def calcSzenario(self):
        self.debug("entering clacSzenario(). IndexOfCurrentWorldPad: " + str(self.IndexOfCurrentWorldPad))
        #this are the possible combinations
        #
        #       |-----|-----|       |-----|-----|       |-----|-----|       |-----|-----|
        #       |     |     |       |     |     |       |     |     |       |     |     |
        #       |  0  |  1  |       |  2  |  3  |       |  3  |  2  |       |  1  |  0  |
        #       |     |     |   ^   |     |     |       |     |     |   ^   |     |     |
        #       |-----|-----|   v   |-----|-----|  <->  |-----|-----|   v   |-----|-----|
        #       |     |     |       |     |     |       |     |     |       |     |     |
        #       |  2  |  3  |       |  0  |  1  |       |  1  |  0  |       |  3  |  2  |
        #       |     |     |       |     |     |       |     |     |       |     |     |
        #       |-----|-----|       |-----|-----|       |-----|-----|       |-----|-----|
        #
        #lets give them names:
        #
        #Szenario-    A          /        B          /        C          /        D
        #
        
        backup_Szenario = self.Szenario
        
        if self.CharacterIsOnRightSideOfCurrentWorldPad:        # Right   
            #self.debug("Right0")
            if self.CharacterIsOnLowerSideOfCurrentWorldPad:    # Lower
                #self.debug("Lower0")
                if self.IndexOfCurrentWorldPad == 0:            # 0
                    self.Szenario = "A"
                elif self.IndexOfCurrentWorldPad == 1:          # 1
                    self.Szenario = "D"
                elif self.IndexOfCurrentWorldPad == 2:          # 2
                    self.Szenario = "B"
                elif self.IndexOfCurrentWorldPad == 3:          # 3
                    self.Szenario = "C"
                    #self.debug("I")
                else:
                    self.debug("error 0")
            elif self.CharacterIsOnUpperSideOfCurrentWorldPad:   # Upper
                #self.debug("Upper0")
                if self.IndexOfCurrentWorldPad == 0:            # 0
                    self.Szenario = "B"
                elif self.IndexOfCurrentWorldPad == 1:          # 1
                    self.Szenario = "C"
                    #self.debug("II")
                elif self.IndexOfCurrentWorldPad == 2:          # 2
                    self.Szenario = "A"
                elif self.IndexOfCurrentWorldPad == 3:          # 3
                    self.Szenario = "D"
                else:
                    self.debug("error 1")
        elif self.CharacterIsOnLeftSideOfCurrentWorldPad:       # Left
            #self.debug("left0")
            if self.CharacterIsOnLowerSideOfCurrentWorldPad:    # Lower
                #self.debug("Lower1")
                if self.IndexOfCurrentWorldPad == 0:            # 0
                    self.Szenario = "D"
                elif self.IndexOfCurrentWorldPad == 1:          # 1
                    self.Szenario = "A"
                elif self.IndexOfCurrentWorldPad == 2:          # 2
                    self.Szenario = "C"
                    #self.debug("III")
                elif self.IndexOfCurrentWorldPad == 3:          # 3
                    self.Szenario = "B"
                else:
                    self.debug("error 2")
            elif self.CharacterIsOnUpperSideOfCurrentWorldPad:  # Upper
                #self.debug("Upper1")
                if self.IndexOfCurrentWorldPad == 0:            # 0
                    self.Szenario = "C"
                    #self.debug("IV")
                elif self.IndexOfCurrentWorldPad == 1:          # 1
                    self.Szenario = "B"
                elif self.IndexOfCurrentWorldPad == 2:          # 2
                    self.Szenario = "D"
                elif self.IndexOfCurrentWorldPad == 3:          # 3
                    self.Szenario = "A"
                else:
                    self.debug("error 3")
            else:
                self.debug("error 4")
        else:
            self.debug("error 5")
        
        #clearing pads if szenario has changed
        if backup_Szenario is not self.Szenario:   
            #self.flashIt()                                                                          #something has changed
            if (backup_Szenario == "A" and self.Szenario == "B") or (backup_Szenario == "B" and self.Szenario == "A") :   
                #
                #       |-----|-----|       |-----|-----|               |-----|-----|       |-----|-----|
                #       |     |     |       |     |     |               |     |     |       |     |     |
                #       |  0..|..1  |       |  2  |  3  |               |  2  |  3  |       |  0  |  1  |
                #       |  .  |  .  |   ^   |     |     |               |     |     |   ^   |     |     |
                #       |--.--|-----|   v   |-----|-----|       or      |-----|-----|   v   |-----|-----|
                #       |  .  |  .  |       |     |     |               |     |     |       |     |     |
                #       |  2..|..3  |       |  0  |  1  |               |  0  |  1  |       |  2  |  3  |
                #       |     |     |       |     |     |               |     |     |       |     |     |
                #       |-----|-----|       |-----|-----|               |-----|-----|       |-----|-----|
                #             A                   B                           B                   A
                #           backup              current                     backup              current
                #       (backup_Szenario)   (self.Szenario)             (backup_Szenario)   (self.Szenario)
                #
                if self.IndexOfCurrentWorldPad == 0 or self.IndexOfCurrentWorldPad == 1:            #going up/down on pad 0 or 1
                    self.clearPadsWithMetaIndex(2)
                    self.clearPadsWithMetaIndex(3)
                elif self.IndexOfCurrentWorldPad == 2 or self.IndexOfCurrentWorldPad == 3:          #going down/up on pad 2 or 3
                    self.clearPadsWithMetaIndex(0)
                    self.clearPadsWithMetaIndex(1)
                else:
                    self.debug("something fishy, tryed to clear pads. error 0")
            elif (backup_Szenario == "A" and self.Szenario == "C") or ( backup_Szenario == "C" and self.Szenario == "A" ):
                #       |-----|-----|       |-----|-----|                   |-----|-----|       |-----|-----|
                #       |     |     |       |     |     |                   |     |     |       |     |     |
                #       |  0..|..1  |       |  3  |  2  |                   |  3  |  2  |       |  0  |  1  |
                #       |  .  |  .  |   /   |     |     |                   |     |     |   /   |     |     |
                #       |--.--|-----|   \   |-----|-----|          or       |-----|-----|   \   |-----|-----|
                #       |  .  |  .  |       |     |     |                   |     |     |       |     |     |
                #       |  2..|..3  |       |  1  |  0  |                   |  1  |  0  |       |  2  |  3  |
                #       |     |     |       |     |     |                   |     |     |       |     |     |
                #       |-----|-----|       |-----|-----|                   |-----|-----|       |-----|-----|
                #             A                   C
                #           backup              current
                #       (backup_Szenario)   (self.Szenario)
                if self.IndexOfCurrentWorldPad == 0:                                                #going up-left/down-right on pad 0
                    self.clearPadsWithMetaIndex(1)
                    self.clearPadsWithMetaIndex(2)
                    self.clearPadsWithMetaIndex(3)
                elif self.IndexOfCurrentWorldPad == 1:                                                #going up-right/down-left on pad 1
                    self.clearPadsWithMetaIndex(0)
                    self.clearPadsWithMetaIndex(2)
                    self.clearPadsWithMetaIndex(3)
                elif self.IndexOfCurrentWorldPad == 2:                                                #going down-left/up-right on pad 2
                    self.clearPadsWithMetaIndex(0)
                    self.clearPadsWithMetaIndex(1)
                    self.clearPadsWithMetaIndex(3)
                elif self.IndexOfCurrentWorldPad == 3:                                                #going down-right/up-left on pad 3
                    self.clearPadsWithMetaIndex(0)
                    self.clearPadsWithMetaIndex(1)
                    self.clearPadsWithMetaIndex(2)
                else:
                    self.debug("something fishy, tryed to clear pads. error 1")
            elif (backup_Szenario == "A" and self.Szenario == "D") or (backup_Szenario == "D" and self.Szenario == "A"):
                #       |-----|-----|       |-----|-----|                   |-----|-----|       |-----|-----|
                #       |     |     |       |     |     |                   |     |     |       |     |     |
                #       |  0  |  1  |       |  1  |  0  |                   |  1  |  0  |       |  0  |  1  |
                #       |     |     |       |     |     |                   |     |     |       |     |     |
                #       |-----|-----|  <->  |-----|-----|          or       |-----|-----|  <->  |-----|-----|
                #       |     |     |       |     |     |                   |     |     |       |     |     |
                #       |  2  |  3  |       |  3  |  2  |                   |  3  |  2  |       |  2  |  3  |
                #       |     |     |       |     |     |                   |     |     |       |     |     |
                #       |-----|-----|       |-----|-----|                   |-----|-----|       |-----|-----|
                #             A                   D                               D                   A
                #           backup              current                         backup              current
                #       (backup_Szenario)   (self.Szenario)                 (backup_Szenario)   (self.Szenario)
                if self.IndexOfCurrentWorldPad == 0 or self.IndexOfCurrentWorldPad == 2:              #going left/right on pad 0 or 2
                    self.clearPadsWithMetaIndex(1)
                    self.clearPadsWithMetaIndex(3)
                elif self.IndexOfCurrentWorldPad == 1 or self.IndexOfCurrentWorldPad == 3:            #going right/left on pad 1 or 3
                    self.clearPadsWithMetaIndex(0)
                    self.clearPadsWithMetaIndex(2)
                else:
                    self.debug("something fishy, tryed to clear pads. error 2")
                    
            elif ( backup_Szenario == "B" and self.Szenario == "C" ) or ( backup_Szenario == "C" and self.Szenario == "B" ):
                #       |-----|-----|       |-----|-----|                   |-----|-----|       |-----|-----|
                #       |     |     |       |     |     |                   |     |     |       |     |     |
                #       |  2  |  3  |       |  3  |  2  |                   |  3  |  2  |       |  2  |  3  |
                #       |     |     |       |     |     |                   |     |     |       |     |     |
                #       |-----|-----|  <->  |-----|-----|          or       |-----|-----|  <->  |-----|-----|
                #       |     |     |       |     |     |                   |     |     |       |     |     |
                #       |  0  |  1  |       |  1  |  0  |                   |  1  |  0  |       |  0  |  1  |
                #       |     |     |       |     |     |                   |     |     |       |     |     |
                #       |-----|-----|       |-----|-----|                   |-----|-----|       |-----|-----|
                #             B                   C                               C                   B
                #           backup              current                         backup              current
                #       (backup_Szenario)   (self.Szenario)                 (backup_Szenario)   (self.Szenario)
                if self.IndexOfCurrentWorldPad == 2  or self.IndexOfCurrentWorldPad == 0 :              #going left/right on pad 
                    self.clearPadsWithMetaIndex(3)
                    self.clearPadsWithMetaIndex(1)
                elif self.IndexOfCurrentWorldPad == 1  or self.IndexOfCurrentWorldPad == 3 :            #going right/left on pad 
                    self.clearPadsWithMetaIndex(2)
                    self.clearPadsWithMetaIndex(0)
                else:
                    self.debug("something fishy, tryed to clear pads. error 3")
                    
            elif (backup_Szenario == "B" and self.Szenario == "D") or (backup_Szenario == "D" and self.Szenario == "B"):
                #       |-----|-----|       |-----|-----|                   |-----|-----|       |-----|-----|
                #       |     |     |       |     |     |                   |     |     |       |     |     |
                #       |  2  |  3  |       |  1  |  0  |                   |  1  |  0  |       |  2  |  3  |
                #       |     |     |   /   |     |     |                   |     |     |   /   |     |     |
                #       |-----|-----|   \   |-----|-----|          or       |-----|-----|   \   |-----|-----|
                #       |     |     |       |     |     |                   |     |     |       |     |     |
                #       |  0  |  1  |       |  3  |  2  |                   |  3  |  2  |       |  0  |  1  |
                #       |     |     |       |     |     |                   |     |     |       |     |     |
                #       |-----|-----|       |-----|-----|                   |-----|-----|       |-----|-----|
                #             B                   D                               D                   B   
                #           backup              current                         backup              current
                #       (backup_Szenario)   (self.Szenario)                 (backup_Szenario)   (self.Szenario)
                if self.IndexOfCurrentWorldPad == 0:                                                #going down-left/up-right on pad 0
                    self.clearPadsWithMetaIndex(2)
                    self.clearPadsWithMetaIndex(3)
                    self.clearPadsWithMetaIndex(1)
                elif self.IndexOfCurrentWorldPad == 1:                                              #going down-right/up-left on pad 1
                    self.clearPadsWithMetaIndex(0)
                    self.clearPadsWithMetaIndex(2)
                    self.clearPadsWithMetaIndex(3)
                elif self.IndexOfCurrentWorldPad == 2:                                              #going up-left/down-right on pad 2
                    self.clearPadsWithMetaIndex(0)
                    self.clearPadsWithMetaIndex(1)
                    self.clearPadsWithMetaIndex(3)
                elif self.IndexOfCurrentWorldPad == 3:                                              #going up-right/down-left on pad 3
                    self.clearPadsWithMetaIndex(0)
                    self.clearPadsWithMetaIndex(1)
                    self.clearPadsWithMetaIndex(2)
                else:
                    self.debug("something fishy, tryed to clear pads. error 4")
                
            elif (backup_Szenario == "C" and self.Szenario == "D") or (backup_Szenario == "D" and self.Szenario == "C"):
                #
                #       |-----|-----|       |-----|-----|               |-----|-----|       |-----|-----|
                #       |     |     |       |     |     |               |     |     |       |     |     |
                #       |  3  |  2  |       |  1  |  0  |               |  1  |  0  |       |  3  |  2  |
                #       |     |     |   ^   |     |     |               |     |     |   ^   |     |     |
                #       |-----|-----|   v   |-----|-----|       or      |-----|-----|   v   |-----|-----|
                #       |     |     |       |     |     |               |     |     |       |     |     |
                #       |  1  |  0  |       |  3  |  2  |               |  3  |  2  |       |  1  |  0  |
                #       |     |     |       |     |     |               |     |     |       |     |     |
                #       |-----|-----|       |-----|-----|               |-----|-----|       |-----|-----|
                #             C                   D                           D                   C
                #           backup              current                     backup              current
                #       (backup_Szenario)   (self.Szenario)             (backup_Szenario)   (self.Szenario)
                if self.IndexOfCurrentWorldPad == 3 or self.IndexOfCurrentWorldPad == 2:              #going up/down on pad 
                    self.clearPadsWithMetaIndex(1)
                    self.clearPadsWithMetaIndex(0)
                elif self.IndexOfCurrentWorldPad == 1 or self.IndexOfCurrentWorldPad == 0:            #going down/up on pad 
                    self.clearPadsWithMetaIndex(2)
                    self.clearPadsWithMetaIndex(3)
                else:
                    self.debug("something fishy, tryed to clear pads. error 5")
            else:
                self.debug("something reeeeeaaallyyyy fishy. error 6")            
        #self.debug("leaving clacSzenario(). IndexOfCurrentWorldPad: " + str(self.IndexOfCurrentWorldPad))




    #rewriting of updateWorldWindow() with a pad-driven engine
    #
    #well, i know, this funktion is the WORST example of spagetti code in history. i'm sorry. it took me 3 days so far (and i'm not finished yet) to write it 
    #i have no idea how to write it in a smarter way. if you have an idea, go for it! i'm looking forward to the pull request!
    def updateWorldWindow(self, In_Character):                      #called from sController::interpretReceivedMessage()
        
        self.calculateOnWichQuarterOfTheCurrentPadTheCharacterIs( int(In_Character.PosY), int(In_Character.PosX) )
        self.calculateOnWichPadTheCharacterIsSittingRightNow( int(In_Character.PosY), int(In_Character.PosX) )
        
        #what is happening here? -> detecting which absolute position in the server-side-world the upper-left corner of the world-window has..
        #i move the screen to the right section of the world with that...
        while int( self.getRelativePositionOnScreenOfCharacter_Y() )  >  self.Y_DistanceUpperLeftCornerOfMiddleSquare + self.HeightOfMiddleSquare:
            self.PositionOfTheUpperLeftCornerInWorld_Y = self.PositionOfTheUpperLeftCornerInWorld_Y + 1
        while int( self.getRelativePositionOnScreenOfCharacter_X() ) > self.X_DistanceUpperLeftCornerOfMiddleSquare + self.WidthOfMiddleSquare:
            self.PositionOfTheUpperLeftCornerInWorld_X = self.PositionOfTheUpperLeftCornerInWorld_X + 1
        
        while int( self.getRelativePositionOnScreenOfCharacter_Y() ) < self.Y_DistanceUpperLeftCornerOfMiddleSquare and self.PositionOfTheUpperLeftCornerInWorld_Y > 0:
            self.PositionOfTheUpperLeftCornerInWorld_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - 1
        while int( self.getRelativePositionOnScreenOfCharacter_X() ) < self.X_DistanceUpperLeftCornerOfMiddleSquare and self.PositionOfTheUpperLeftCornerInWorld_X > 0:
            self.PositionOfTheUpperLeftCornerInWorld_X = self.PositionOfTheUpperLeftCornerInWorld_X - 1
        #but why am i working on the CharacterBackup? i need to check, if i can replace that with the input-character
        
        self.UpperLeftCornerOfPad_Y[self.IndexOfCurrentWorldPad] = self.PositionOfTheUpperLeftCornerInWorld_Y
        self.UpperLeftCornerOfPad_X[self.IndexOfCurrentWorldPad] = self.PositionOfTheUpperLeftCornerInWorld_X
        
        #self.World.clear()
        #self.SightFieldOverlay.clear()
        self.World.erase()
        self.SightFieldOverlay.erase()
        
        if self.Z_LevelAttachedToCharacter:
            self.Z_Level = int(In_Character.PosZ)
        PositionOfLowerRightCornerOfDispayedSectionOfPad_Y = int(self.HeightOfWorldPad) - int(self.PositionOfTheUpperLeftCornerInWorld_Y)
        PositionOfLowerRightCornerOfDispayedSectionOfPad_X = int(self.WidthOfWorldPad) - int(self.PositionOfTheUpperLeftCornerInWorld_X)


        #lets find out, in which szenario we are
        self.calcSzenario()   #i put it in an extra function



        UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X = ( int( In_Character.PosY / self.HeightOfWorldPad ) * self.HeightOfWorldPad, int( In_Character.PosX / self.WidthOfWorldPad ) * self.WidthOfWorldPad )


        for PadIndex in range(0, 4):        #drawing one pad after another

            Pad_source_Y = -1
            Pad_source_X = -1
            World_dest_Y_up = -1
            World_dest_X_up = -1
            World_dest_Y_low = -1
            World_dest_X_low = -1
            PadIsVisibleOnScreen = False

            #this are the possible combinations
            #
            #       |-----|-----|       |-----|-----|       |-----|-----|       |-----|-----|
            #       |     |     |       |     |     |       |     |     |       |     |     |
            #       |  0  |  1  |       |  2  |  3  |       |  3  |  2  |       |  1  |  0  |
            #       |     |     |   ^   |     |     |       |     |     |   ^   |     |     |
            #       |-----|-----|   v   |-----|-----|  <->  |-----|-----|   v   |-----|-----|
            #       |     |     |       |     |     |       |     |     |       |     |     |
            #       |  2  |  3  |       |  0  |  1  |       |  1  |  0  |       |  3  |  2  |
            #       |     |     |       |     |     |       |     |     |       |     |     |
            #       |-----|-----|       |-----|-----|       |-----|-----|       |-----|-----|
            #
            #Szenario-    A          /        B          /        C          /        D
            #
            #
            #i know, the next junk of coe is not very elegant. but i really dont't know how to write it in a nicer way
            if self.Szenario == "A":
                #       |-----|-----|
                #       |     |     |
                #       |  0  |  1  |
                #       |     |     |
                #       |-----|-----|
                #       |     |     |
                #       |  2  |  3  |
                #       |     |     |
                #       |-----|-----|
                if self.IndexOfCurrentWorldPad == 0:        #character standing on pad 0
                    if PadIndex == 0:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 0, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = ( UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad ) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1 #was this the reason for seg-faults?
                        World_dest_X_low = ( UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad ) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 1:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 0, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + 2*self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 2:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 0, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + 2*self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 3:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 0, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)           # after days of writing that shit, i came to the idea, that this would be quicker
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                elif self.IndexOfCurrentWorldPad == 1:      #character standing on pad 1
                    if PadIndex == 0:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 1, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1 #this again seems to cause Segmentation fauls  
                        World_dest_X_low = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 1:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 1, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1     # minus one because of segmentation faults
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 2:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 1, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + 2*self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_low = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 3:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 1, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + 2*self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                elif self.IndexOfCurrentWorldPad == 2:      #character standing on pad 2 
                    if PadIndex == 0:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 2, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 1:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 2, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 2:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 2, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 3:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 2, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                elif self.IndexOfCurrentWorldPad == 3:      #character standing on pad 3
                    if PadIndex == 0:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 3, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 1:
                        self.debug("Szenario A, IndexOfCurrentWorldPad 3, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 2:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 3, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad - 1 #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 3:
                        #self.debug("Szenario A, IndexOfCurrentWorldPad 3, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad - 1 #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad

            elif self.Szenario == "B":
                #|-----|-----|
                #|     |     |
                #|  2  |  3  |
                #|     |     |
                #|-----|-----|
                #|     |     |
                #|  0  |  1  |
                #|     |     |
                #|-----|-----|
                if self.IndexOfCurrentWorldPad == 0:        #character standing on pad 0
                    if PadIndex == 0:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 0, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = ( UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad ) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1 #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = ( UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad ) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 1:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 0, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]   #0
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) # 0
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = ( UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad ) - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad ) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1 #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + 2*self.WidthOfWorldPad ) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 2:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 0, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad - 1
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 3:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 0, to print PadIndex = 3")
                        pass
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad - 1
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                elif self.IndexOfCurrentWorldPad == 1:        #character standing on pad 1
                    if PadIndex == 0:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 1, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0])
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X 
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1 #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = ( UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X )
                    elif PadIndex == 1:
                        #self.debug("\nSzenario B, IndexOfCurrentWorldPad 1, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1 #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 2:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 1, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad - 1 #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 3:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 1, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad - 1      #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                elif self.IndexOfCurrentWorldPad == 2:        #character standing on pad 2
                    if PadIndex == 0:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 2, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 1:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 2, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 2:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 2, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad - 1  #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 3:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 2, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad - 1      #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                elif self.IndexOfCurrentWorldPad == 3:        #character standing on pad 3
                    if PadIndex == 0:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 3, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 1:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 3, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 2:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 3, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad - 1 #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 3:
                        #self.debug("Szenario B, IndexOfCurrentWorldPad 3, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad - 1 #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad

            elif self.Szenario == "C":
                #|-----|-----|
                #|     |     |
                #|  3  |  2  |
                #|     |     |
                #|-----|-----|
                #|     |     |
                #|  1  |  0  |
                #|     |     |
                #|-----|-----|
                if self.IndexOfCurrentWorldPad == 0:        #character standing on pad 0
                    if PadIndex == 0:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 0, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]          
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1 #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 1:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 0, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad - 1  #this last minus one seems to be necessary to avoid black stripes
                    elif PadIndex == 2:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 0, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y - 1                   #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 3:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 0, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad - 1
                elif self.IndexOfCurrentWorldPad == 1:        #character standing on pad 1
                    if PadIndex == 0:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 1, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]  + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + 2*self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 1:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 1, to print PadIndex = 1")
                        Pad_source_Y =self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X - 1 #this last minus one seems to be necessary to avoid black stripes
                    elif PadIndex == 2:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 1, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad - 1 #without that '-1' i get a '!'-symbol on strange places ( a.k.a. lower right corner of pad)
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 3:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 1, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad - 1
                elif self.IndexOfCurrentWorldPad == 2:        #character standing on pad 2
                    if PadIndex == 0:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 2, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + 2*self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 1:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 2, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad - 1
                    elif PadIndex == 2:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 2, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 3:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 2, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad - 1
                elif self.IndexOfCurrentWorldPad == 3:        #character standing on pad 3
                    if PadIndex == 0:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 3, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 1:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 3, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad - 1
                    elif PadIndex == 2:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 3, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad - 1
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 3:
                        #self.debug("Szenario C, IndexOfCurrentWorldPad 3, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad - 1

            elif self.Szenario == "D":
                #|-----|-----|
                #|     |     |
                #|  1  |  0  |
                #|     |     |
                #|-----|-----|
                #|     |     |
                #|  3  |  2  |
                #|     |     |
                #|-----|-----|
                if self.IndexOfCurrentWorldPad == 0:        #character standing on pad 0
                    if PadIndex == 0:
                        #self.debug("UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]: " + str(UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]))
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1     #was this the reason for seg-faults?
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 1:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 0, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1 #seg faults...
                        World_dest_X_low = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X - 1   #it's strange, but i need it! (black stripes...)
                    elif PadIndex == 2:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 0, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)  
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 3:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 0, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad - 1
                elif self.IndexOfCurrentWorldPad == 1:        #character standing on pad 1
                    if PadIndex == 0:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 1, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0])
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1     #once more
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + 2*self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 1:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 1, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y - 1     #here it is seems to be necessary because of seg faults
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X - 1      #but here? -> yes. it is needed, otherwise i get black stripes... right. that was it..
                    elif PadIndex == 2:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 1, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 3:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 1, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] +self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad - 1
                elif self.IndexOfCurrentWorldPad == 2:        #character standing on pad 2
                    if PadIndex == 0:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 2, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 1:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 2, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad - 1
                    elif PadIndex == 2:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 2, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_up = UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.PositionOfTheUpperLeftCornerInWorld_X
                        World_dest_Y_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] + self.HeightOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_Y
                        World_dest_X_low = (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad) - self.PositionOfTheUpperLeftCornerInWorld_X
                    elif PadIndex == 3:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 2, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] - self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad - 1 # black stripes
                elif self.IndexOfCurrentWorldPad == 3:        #character standing on pad 3
                    if PadIndex == 0:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 3, to print PadIndex = 0")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 1:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 3, to print PadIndex = 1")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0] - self.HeightOfWorldPad)
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad - 1
                    elif PadIndex == 2:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 3, to print PadIndex = 2")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - (UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1] + self.WidthOfWorldPad)
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad
                    elif PadIndex == 3:
                        #self.debug("Szenario D, IndexOfCurrentWorldPad 3, to print PadIndex = 3")
                        Pad_source_Y = self.PositionOfTheUpperLeftCornerInWorld_Y - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[0]
                        Pad_source_X = self.PositionOfTheUpperLeftCornerInWorld_X - UpperLeftCornerOfPadCharacterIsCurrentlyStandingOn_Y_X[1]
                        World_dest_Y_up = Pad_source_Y * (-1)
                        World_dest_X_up = Pad_source_X * (-1)
                        World_dest_Y_low = World_dest_Y_up + self.HeightOfWorldPad
                        World_dest_X_low = World_dest_X_up + self.WidthOfWorldPad - 1
                else:
                    self.debug("fishy mc fish")
            else:
                self.debug("the real fishy mc fishinger")


        

            #time to catch  common cases
            if Pad_source_Y < 0:
                Pad_source_Y = 0
            if Pad_source_X < 0:
                Pad_source_X = 0
            if World_dest_Y_up < 0:
                World_dest_Y_up = 0
            if World_dest_X_up < 0:
                World_dest_X_up = 0
            if World_dest_Y_low > self.HeightOfWorldWindow:
                World_dest_Y_low = self.HeightOfWorldWindow
            if World_dest_X_low > self.WidthOfWorldWindow:
                World_dest_X_low = self.WidthOfWorldWindow
            if ((World_dest_Y_low - World_dest_Y_up) >= 0) and ((World_dest_X_low - World_dest_X_up) >= 0):
                PadIsVisibleOnScreen = True

            #self.debug( "PadIsVisibleOnScreen: " + str(PadIsVisibleOnScreen) )
            if PadIsVisibleOnScreen:
                #self.debug("")
                #self.debug( "overlaying pads now" )
                #self.debug( "Pad_source_Y = " + str(Pad_source_Y) )
                #self.debug( "Pad_source_X = " + str(Pad_source_X) )
                #self.debug( "World_dest_Y_up = " + str(World_dest_Y_up) )
                #self.debug( "World_dest_X_up = " + str(World_dest_X_up) )
                #self.debug( "World_dest_Y_low = " + str(World_dest_Y_low) )
                #self.debug( "World_dest_X_low = " + str(World_dest_X_low) )
                #self.debug( "PadIndex: " + str(PadIndex) )
                
                for Layer in range( 0, int(self.Z_Level-1)):        #all layers way below character (SecondarySymbols)
                    self.WorldPads_Meta[PadIndex][Layer][1].overlay( self.World, int(Pad_source_Y), int(Pad_source_X), int(World_dest_Y_up), int(World_dest_X_up), int(World_dest_Y_low), int(World_dest_X_low) ) 
                self.WorldPads_Meta[PadIndex][self.Z_Level-1][2].overlay( self.World, int(Pad_source_Y), int(Pad_source_X), int(World_dest_Y_up), int(World_dest_X_up), int(World_dest_Y_low), int(World_dest_X_low) )      #all symbols on level below character (tertiarySymbols)
                self.WorldPads_Meta[PadIndex][self.Z_Level][0].overlay( self.World, int(Pad_source_Y), int(Pad_source_X), int(World_dest_Y_up), int(World_dest_X_up), int(World_dest_Y_low), int(World_dest_X_low) )        ##all symbols on the same level as character (primarySymbols)

        #sightfield-overlay:
        if self.Z_Level == int(In_Character.PosZ):
            self.placeStringInSightFieldOverlay( int(In_Character.PosY) - int(self.PositionOfTheUpperLeftCornerInWorld_Y), int(In_Character.PosX) - int(self.PositionOfTheUpperLeftCornerInWorld_X), "@", 0 )
            self.Z_LevelAttachedToCharacter = True
        else:
            self.placeStringInSightFieldOverlay( int(In_Character.PosY) - int(self.PositionOfTheUpperLeftCornerInWorld_Y), int(In_Character.PosX) - int(self.PositionOfTheUpperLeftCornerInWorld_X), "+", 0 )
        self.printSightFieldOverlay( int(In_Character.SightRadius), int(In_Character.PosY), int(In_Character.PosX) )
        self.World.refresh()


    def printSightFieldOverlay(self, In_Radius, In_PosY, In_PosX):
        if (self.AbsolutePositionInTarget_PosY == -1 and self.AbsolutePositionInTarget_PosX == -1) == False:
            self.markPositionWithX_absolute( self.AbsolutePositionInTarget_PosY, self.AbsolutePositionInTarget_PosX )
        self.drawSightRadius( In_Radius, In_PosY, In_PosX )
        self.World.refresh()



    def showHeartbleed(self):
        self.Heartbleed.erase()
        try:
            self.Heartbleed.addstr(0, 0, "*", curses.color_pair(3))
        except curses.error:                    #dirty. but... http://ubuntuforums.org/showthread.php?t=1306504
            pass
        self.Heartbleed.refresh()

    def turnOnBlueHeartbleed(self):
        self.Heartbleed.erase()
        #self.Heartbleed.addstr(0, 0, "*", curses.color_pair(4))
        try:
            self.Heartbleed.addstr(0, 0, "*", curses.color_pair(4))
        except curses.error:                    #dirty. but... http://ubuntuforums.org/showthread.php?t=1306504
            pass
        self.Heartbleed.refresh()

    def beep(self):
        os.system("bash -c \"speaker-test -t sine -f 440 -l 1 -p 2 >/dev/null\"")

    def debug(self, In):
        ToWriteMessage = "[" + str(time.time()) + "]: " + str(In) + "\n"
        with open("log/debug.log", 'a') as out:
            out.write( ToWriteMessage )

    def placeCharInWorld(self, In_PosY, In_PosX, In_Char, In_ColorSet):
        pass
        try:
            #self.debug("passed 'try'")
            self.World.addch(int(In_PosY), int(In_PosX), In_Char, curses.color_pair(In_ColorSet))
        except curses.error:                    #dirty. but... http://ubuntuforums.org/showthread.php?t=1306504
            pass

    def placeStringInWorld(self, In_PosY, In_PosX, In_Char, In_ColorSet):
        pass
        try:
            #self.debug("passed 'try'")
            self.World.addstr(int(In_PosY), int(In_PosX), In_Char, curses.color_pair(In_ColorSet))
        except curses.error:                    #dirty. but... http://ubuntuforums.org/showthread.php?t=1306504
            pass

    def placeStringInSightFieldOverlay(self, In_PosY, In_PosX, In_Char, In_ColorSet):
        pass
        try:
            self.SightFieldOverlay.addstr(int(In_PosY), int(In_PosX), In_Char, curses.color_pair(In_ColorSet))
        except curses.error:                    #dirty. but... http://ubuntuforums.org/showthread.php?t=1306504
            pass




#    def placeCharBoldInWorld(self, In_PosY, In_PosX, In_Char, In_ColorSet):
#        pass
#        try:
#            self.World.addch(int(In_PosY), int(In_PosX), In_Char, curses.color_pair(In_ColorSet) | curses.A_BOLD)
#        except curses.error:                    #dirty. but... http://ubuntuforums.org/showthread.php?t=1306504
#            pass

    def placeCharIn_XZ_View(self, In_PosY, In_PosX, In_Char, In_ColorSet):
        try:
            #self.debug("passed 'try'")
            self.XZ_View.addch(int(In_PosY), int(In_PosX), In_Char, curses.color_pair(In_ColorSet))
        except curses.error:                    #dirty. but... http://ubuntuforums.org/showthread.php?t=1306504
            pass


    def placeCharIn_YZ_View(self, In_PosY, In_PosX, In_Char, In_ColorSet):
        try:
            #self.debug("passed 'try'")
            self.YZ_View.addch(int(In_PosY), int(In_PosX), In_Char, curses.color_pair(In_ColorSet))
        except curses.error:                    #dirty. but... http://ubuntuforums.org/showthread.php?t=1306504
            pass



    def zoomUp(self,  In_Character):
        if self.Z_Level < (self.MaxLevelOverZero - 1):
            self.Z_Level = int(self.Z_Level) + 1
            self.Z_LevelAttachedToCharacter = False
            self.updateWorldWindow( In_Character )

    def zoomDown(self, In_Character):
        if self.Z_Level > 1:
            self.Z_Level = int(self.Z_Level) - 1
            self.Z_LevelAttachedToCharacter = False
            self.updateWorldWindow( In_Character )

    def resetZoom(self, In_Character):
        self.Z_Level = int(In_Character.PosZ)
        self.Z_LevelAttachedToCharacter = True
        self.updateWorldWindow( In_Character )

      




    def rebuildWindow(self):
        curses.endwin()
        if curses.isendwin():
            pass
            print("rebuildWindow(): rebuilding View")            #that can be deleted later
            self.debug("rebuildWindow(): rebuilding View")


    def flashIt_thread(self):
        self.CurrentlyFlashing = True
        curses.flash()
        self.CurrentlyFlashing = False

    def flashIt(self):
        if self.CurrentlyFlashing == False:
            Thread_WaitForClearServerMsg = Thread( target=self.flashIt_thread, args=() )
            Thread_WaitForClearServerMsg.start()

    def printOtherCharactersName(self, In_CharacterName, In_PosY, In_PosX, In_SightRadiusOfOwnCharacter):
#        self.debug( "printOtherCharactersName(), input parameter: In_CharacterName: " + str(In_CharacterName) + " ;In_PosY: " + str(In_PosY) + " ;In_PosX: " + str(In_PosX) + " ;In_SightRadiusOfOwnCharacter: " +  str(In_SightRadiusOfOwnCharacter) )
#        self.debug( "self.NumberOfOtherCharacterToPlace:" + str(self.NumberOfOtherCharacterToPlace) )
        if self.IsInUpperSideOfScreen == False:
            ToPlaceString = "^ " + In_CharacterName
            self.placeStringInSightFieldOverlay( self.PosYInWorldWindow+int(In_SightRadiusOfOwnCharacter)+1+self.NumberOfOtherCharacterToPlace, int(In_PosX), ToPlaceString, 0 )
        else:
            ToPlaceString = "v " + In_CharacterName
            self.placeStringInSightFieldOverlay( self.PosYInWorldWindow-(int(In_SightRadiusOfOwnCharacter)+1+self.NumberOfOtherCharacterToPlace), int(In_PosX), ToPlaceString, 0 )
        pass
        if self.IsInLeftSideOfScreen == True:
            ToPlaceString = "< " + In_CharacterName
            self.placeStringInSightFieldOverlay( int(In_PosY), self.PosXInWorldWindow + int(In_SightRadiusOfOwnCharacter) + 2, ToPlaceString, 0 )
        else:
            ToPlaceString = In_CharacterName + " >"
            self.placeStringInSightFieldOverlay( int(In_PosY), self.PosXInWorldWindow - (int(In_SightRadiusOfOwnCharacter) + len(ToPlaceString) + 2), ToPlaceString, 0 )





    def markPositionWithX_absolute( self, In_PosY, In_PosX ):
        #self.debug("entering markPositionWithX")
        self.placeCharInWorld( In_PosY-int(self.PositionOfTheUpperLeftCornerInWorld_Y), In_PosX-int(self.PositionOfTheUpperLeftCornerInWorld_X), "X", 0 )
        #self.World.refresh()



    def displayMessageIn_Server_MsgContent( self, In_MessageContent ):
        self.Server_MsgContent.erase()
        self.Server_MsgContent.addstr( 0, 0, str(In_MessageContent) )
        self.Server_MsgContent.refresh()
        Thread_WaitForClearServerMsg = Thread( target=self.waitForClearServerMsg, args=() )
        Thread_WaitForClearServerMsg.start()
        ToWriteMessage = "[" + str( time.time() ) + "]: " + str(In_MessageContent) + "\n"
        with open("log/serverMsg.log", 'a') as out:
            out.write( ToWriteMessage )

    def waitForClearServerMsg(self):
        time.sleep(10)
        self.Server_MsgContent.erase()
        self.Server_MsgContent.refresh()

    def writeSightFieldInWorldPads(self, In_SightField, In_CharacterPosY, In_CharacterPosX):
        self.debug("entering writeSightFieldInWorldPad(). In_CharacterPosY = " + str( In_CharacterPosY) + " ; In_CharacterPosX: " + str(In_CharacterPosX))
        for Key in In_SightField:
            for Temp_Cube in In_SightField[Key]:
                Temp_CubeType = Temp_Cube.KindOfCube
                ColorSet = 1
                #PrimarColorSet = 3                #standard colour set
                #SecundarColorSet = 2
                #TertiaryColorSet = 7
                PrimarySymbol = "X"                     #for all the stuff which is on the same level as the character (or better: the current view-level because of zoom-stuff)
                SecondarySymbol = curses.ACS_BULLET     #little dots to represent everything below you. the color should be the same as the primary-symbol
                TertiarySymbol = curses.ACS_BULLET      #
                #self.debug("\tTemp_CubeType: " + str(Temp_CubeType) )
                if Temp_CubeType == "solid":
                    PrimarySymbol = "S"
                    TertiarySymbol = curses.ACS_CKBOARD
                    #TertiarySymbol = "S"
                elif Temp_CubeType == "grass":
                    PrimarySymbol = ","
                    ColorSet = 2
                elif Temp_CubeType == "sand":
                    PrimarySymbol = ":"
                    ColorSet = 3
                elif Temp_CubeType == "trunk":
                    PrimarySymbol = "O"
                elif Temp_CubeType == "water":
                    PrimarySymbol = "~"
                    ColorSet = 4
                elif Temp_CubeType == "leafsAndTwigs":
                    PrimarySymbol = "b"
                    ColorSet = 2
                elif Temp_CubeType == "branch":
                    PrimarySymbol = "|"
                elif Temp_CubeType == "characterCube":
                    SecondarySymbol = "+"
                    TertiarySymbol = "+"
                    PrimarySymbol = "@"
                    pass
                    #missing:stuff to display name of other character. the old stuff is not working with the pad-based-design
                    #
                    #self.NumberOfOtherCharacterToPlace = self.NumberOfOtherCharacterToPlace + 1
                    #self.printOtherCharactersName(Temp_Cube.CharacterName, int(Temp_Cube.PosY)-int(self.PositionOfTheUpperLeftCornerInWorld_Y), int(Temp_Cube.PosX)-int(self.PositionOfTheUpperLeftCornerInWorld_X), self.CharacterBackup.SightRadius )
                elif Temp_CubeType == "gravel":
                    PrimarySymbol = "*"
                    #SecondarySymbol = curses.ACS_BULLET
                    ColorSet = 3
                elif Temp_CubeType == "none":   #in most cases 'none'-cubes won't be submitted, because that would be to much... but in some cases (after a character moved) it is improtant
                    PrimarySymbol = " "
                    SecondarySymbol = " "
                    TertiarySymbol = " "
                else:
                    PrimarySymbol = "?"        #not found?
                    self.debug("\tsymbol not found: view.updateWorldWindow() low: " + Temp_CubeType)
                pass
                MetaPadIndexOfThisCube = self.getOnWichPadThisCubeBelongs( int(Temp_Cube.PosY), int(Temp_Cube.PosX) )
                Relative_Y = int(Temp_Cube.PosY) % self.HeightOfWorldPad
                Relative_X = int(Temp_Cube.PosX) % self.WidthOfWorldPad
                #self.RecentlyReceivedCubesOnPad[MetaPadIndexOfThisCube] = True
                try:
                    self.WorldPads_Meta[MetaPadIndexOfThisCube][int(Temp_Cube.PosZ)][0].addch(( Relative_Y ), int( Relative_X ), PrimarySymbol, curses.color_pair( ColorSet )  )
                    self.WorldPads_Meta[MetaPadIndexOfThisCube][int(Temp_Cube.PosZ)][1].addch(( Relative_Y ), int( Relative_X ), SecondarySymbol, curses.color_pair( ColorSet ) )
                    self.WorldPads_Meta[MetaPadIndexOfThisCube][int(Temp_Cube.PosZ)][2].addch(( Relative_Y ), int( Relative_X ), TertiarySymbol, curses.color_pair( ColorSet ) )
                except curses.error:
                    self.debug("catching error in writeSightFieldInWorldPads()")
                pass
        pass
        #gc.collect()        # https://stackoverflow.com/questions/1316767/how-can-i-explicitly-free-memory-in-python
        #well, thats not really helping



    def getOnWichPadThisCubeBelongs(self, In_PosY, In_PosX):
        #self.debug("entering getOnWichPadThisCubeBelongs() with In_PosY: " + str(In_PosY) + "; In_PosX: " + str(In_PosX) )
        Output = -1
        #
        #       |-----|-----|
        #       |     |     |
        #       |  0  |  1  |
        #       |     |     |
        #       |-----|-----|
        #       |     |     |
        #       |  2  |  3  |
        #       |     |     |
        #       |-----|-----|
        #
        BelongsOnPad_0_or_1 = False
        Temp_PosY = In_PosY % (2*self.HeightOfWorldPad)
        Temp_PosX = In_PosX % (2*self.WidthOfWorldPad)

        if Temp_PosY < self.HeightOfWorldPad:
            #self.debug("Temp_PosY < self.HeightOfWorldPad")
            BelongsOnPad_0_or_1 = True
        else:
            pass
            #self.debug("Temp_PosY >= self.HeightOfWorldPad")
        if Temp_PosX < self.WidthOfWorldPad:
            #self.debug("Temp_PosX < self.WidthOfWorldPad")
            if BelongsOnPad_0_or_1:
                Output = 0
            else:
                Output = 2
        else:
            #self.debug("Temp_PosX >= self.WidthOfWorldPad")
            if BelongsOnPad_0_or_1:
                Output = 1
            else:
                Output = 3
        return Output
                


    def updatePing(self, In_Ping):
        #clearing the window first (because of the border, clear() is not working and i'm to lazy to make a extra Ping-Window without the border
        if self.InChatMode == False:    #important to interrupt ping-refreshing when in chat mode, because it would move the curser
            BlankString = ""
            for Index in range(0, self.PING_SIZE):
                BlankString += (" ")
            self.Ping_Frame.addstr( 1, 1, BlankString)
            
            Temp_Ping = -1
            if int(float(In_Ping)) == -1:       #if server is no down
                Temp_Ping = " -1"
            else:
                Temp_Ping = round( float(In_Ping), int(self.PING_SIZE-2) )
            self.Ping_Frame.addstr( 1, 1, str(Temp_Ping) )
            self.Ping_Frame.refresh()


    def takePositionInTarget( self, In_PosY, In_PosX ):
        self.AbsolutePositionInTarget_PosY = In_PosY
        self.AbsolutePositionInTarget_PosX = In_PosX
        self.markPositionWithX_absolute( In_PosY, In_PosX )
        self.World.refresh()
        
        
        
        
